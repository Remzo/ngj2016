#version 330 core

layout (location=0) out vec4 out_color;

in vec3 v2f_dirUV;

uniform samplerCube tex0;

void main()
{
    //out_color = vec4(1,0,1,1);
	out_color = texture(tex0, v2f_dirUV);
	//out_color = vec4(out_color.w);
}