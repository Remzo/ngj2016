#version 330 core

layout(location=0) in vec3 in_position;
layout(location=1) in vec2 in_uv;

out vec3 v2f_dirUV;

// Model, View, Projection matrix.
uniform mat4 MVP;

void main()
{
    gl_Position = MVP * vec4(in_position, 1);
    // Just pass the uv through directly.
    v2f_dirUV = in_position;
}