#version 330 core

layout(location=0) in vec3 in_position;
layout(location=1) in vec2 in_uv;
layout(location=2) in vec3 in_normals;

out vec2 v2f_uv;
out vec3 v2f_normals;

// Model, View, Projection matrix.
uniform mat4 MVP;

void main()
{
    gl_Position = MVP * vec4(in_position, 1);
    // Just pass the uv through directly.
    v2f_uv = in_uv;
	v2f_normals = in_normals;
}