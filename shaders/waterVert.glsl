#version 330 core

layout(location=0) in vec4 in_position;
layout(location=1) in vec4 in_normal;
layout(location=2) in vec4 in_color;

out vec4 v2f_color;
out vec4 v2f_normal;
out vec4 v2f_camDir;

out vec3 worldPos;

// Model, View, Projection matrix.
uniform mat4 MVP;
uniform mat4 Model;
uniform vec4 color;
uniform vec3 camDir;
uniform float uTime;

float hash( float n ) { return fract(sin(n)*753.5453123); }
float hash_noise( in vec3 x )
{
    vec3 p = floor(x);
    vec3 f = fract(x);
    f = f*f*(3.0-2.0*f);
	
    float n = p.x + p.y*157.0 + 113.0*p.z;
    return mix(mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
                   mix( hash(n+157.0), hash(n+158.0),f.x),f.y),
               mix(mix( hash(n+113.0), hash(n+114.0),f.x),
                   mix( hash(n+270.0), hash(n+271.0),f.x),f.y),f.z) * 4.0 - 2.0;
}

float fbm_3d(vec3 coords, int numberOfOcts, float freq){
        float fValue = 0.0;
        float weight = .5;
        for(int i = 0; i < numberOfOcts; i++){
                fValue += hash_noise(freq * coords) * weight;
                weight *= 0.5;
                freq *= 2.0;
        }
        return fValue;
}

void main()
{
    v2f_normal = Model * in_normal; // keep in world space
	worldPos = in_position.xyz;
	worldPos += v2f_normal.xyz * fbm_3d( vec3(worldPos + vec3(uTime*0.001, uTime * 0.002, uTime * 0.004)), 4, 100) * 0.02;
	
	vec4 pos4 = vec4(worldPos, 1);
	
	gl_Position = MVP * pos4;
    // Just pass the color through directly.
    v2f_color = color*in_color;
	v2f_camDir = vec4(camDir,1);
}