#version 330 core

layout (location=0) out vec4 out_color;

in vec2 v2f_uv;
in vec3 v2f_normals;

uniform sampler2D tex0; //diffuse
uniform sampler2D tex1; //normal map

void main()
{
	vec3 lightDir = vec3(0,0,1);
    //out_color = vec4(1,0,1,1);
	out_color = texture(tex0, v2f_uv); //TODO: implement
	//out_color = vec4(out_color.w);
}