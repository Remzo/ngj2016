#version 430

// Compute Shader SSB Data Structure and Buffer Definition

layout(binding = 0, r32f) writeonly uniform image3D destTex;
layout(binding = 3, r32f) writeonly uniform image3D collTex;

struct Brush {
  vec3 pos;
  float r;
  ivec4 opInfo;
};

// std140 is pretty important here, it is the only way to guarantee the data
//   structure is aligned as described above and that the stride between
//     elements in verts[] is 0.
layout(std140, binding = 1) buffer BrushBuffer { 
	Brush brushes[]; 
} data;

struct Particle {
  vec3 pos;
  float r;
  vec3 vel;
  float lifetime;
};

layout(std140, binding = 2) buffer ParticleBuffer { 
	Particle particles[];
} pData;


layout(local_size_x = 8, local_size_y = 8, local_size_z = 8) in;

float opU(float d1, float d2) { return min(d1, d2); }

float opS(float d1, float d2) { return max(-d1, d2); }

float opI(float d1, float d2) { return max(d1, d2); }

float opUSoft(float d1, float d2, int hardness) {
	float k = 1.0f/hardness;
	float h = clamp( 0.5+0.5*(d2-d1)/k, 0.0, 1.0 );
    return mix( d2, d1, h ) - k*h*(1.0-h);
}

void main() {
  ivec3 storePos = ivec3(gl_GlobalInvocationID.xyz);
  vec3 imagePos = vec3(storePos) / imageSize(destTex);

  float minDist = distance(imagePos, data.brushes[0].pos) - data.brushes[0].r;
  for (int i = 1; i < data.brushes.length(); i++) {
	Brush b = data.brushes[i];
    float dist = distance(imagePos, b.pos) - b.r;
	
	switch(b.opInfo.x) {
		case 0: minDist = opU(dist, minDist); break;
		case 1: minDist = opUSoft(dist, minDist, b.opInfo.y); break;
		case 2: minDist = opI(dist, minDist); break;
		case 3: minDist = opS(dist, minDist); break;
	}
  }
  imageStore(collTex, storePos, vec4(minDist));
	
  for (int i = 0; i < pData.particles.length(); i++) {
	Particle p = pData.particles[i];
    float dist = distance(imagePos, p.pos) - p.r;
	minDist = opUSoft(dist, minDist, 20);
  }

  imageStore(destTex, storePos, vec4(minDist));
}