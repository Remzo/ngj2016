#version 330 core

layout (location=0) out vec4 out_color;

in vec4 v2f_color;
in vec4 v2f_normal;
in vec4 v2f_camDir;
in vec3 worldPos;

uniform float uTime;

void main()
{

	//out_color = v2f_normal;
	float interp = clamp(sin(worldPos.y * 20), 0, 1);
	out_color = mix(vec4(21.0/255, 53.0/255, 61.0/255, 1.0) * 2.5, vec4(158.0/255, 228.0/255, 241.0/255, 1.0), interp * 1.3);
}