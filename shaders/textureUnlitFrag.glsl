#version 330 core

layout (location=0) out vec4 out_color;

in vec2 v2f_uv;
in vec4 v2f_color;

uniform sampler2D tex0; //diffuse

void main()
{
    //out_color = vec4(1,0,1,1);
	out_color = texture(tex0, v2f_uv) * v2f_color;
	if(out_color.a <= 0.1)
	{
		discard;
	}
	//out_color = vec4(out_color.w);
}