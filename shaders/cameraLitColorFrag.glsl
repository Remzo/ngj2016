#version 330 core

layout (location=0) out vec4 out_color;

in vec4 v2f_color;
in vec4 v2f_normal;
in vec4 v2f_camDir;

void main()
{

	//out_color = v2f_normal;
	out_color = v2f_color*(1.0f-clamp((dot(v2f_camDir,v2f_normal)),0.0f,0.7f));
	out_color.a = v2f_color.a;
	
}