#version 330 core

layout (location=0) out vec4 out_color;

in vec2 v2f_uv;

uniform sampler2D tex0; //diffuse

void main()
{
	out_color = texture(tex0, v2f_uv);
	//out_color.a = 1.0;
	//out_color = vec4(out_color.w);
}