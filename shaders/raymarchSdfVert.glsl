#version 430
  
layout (location = 0) in vec3 position;

smooth out vec3 worldSpacePosition;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
	worldSpacePosition = (model * vec4(position, 1.0)).xyz;
	gl_Position =   proj * view * model * vec4(position, 1.0);
}