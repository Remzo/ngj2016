#version 330 core

layout (location=0) out vec4 out_color;

in vec2 v2f_uv;

uniform sampler2D tex0; //diffuse

uniform float blurSize;
uniform vec2 dir; // (1.0,0.0) or (0.0,1.0)

void main()
{
	const int tap = 7;	// always odd, 17 max
	const float pascal[144] = 	float[144](
									1,	1,	1,	1,		1,		1,		1,		1,		1,
									1,	2,	1,	1,		1,		1,		1,		1,		1,
									1,	3,	3,	1,		1,		1,		1,		1,		1,
									1,	4,	6,	4,		1,		1,		1,		1,		1,
									1,	5,	10,	10,		5,		1,		1,		1,		1,
									1,	6,	15,	20,		15,		6,		1,		1,		1,
									1,	7,	21,	35,		35,		21,		7,		1,		1,
									1,	8,	28,	56,		70,		56,		28,		8,		1,
									1,	9,	36,	84,		126,	126,	84,		36,		9,
									1,	10,	45,	120,	210,	252,	210,	120,	45,
									1,	11,	55,	165,	330,	462,	462,	330,	165,
									1,	12,	66,	220,	495,	792,	924,	792,	495,
									1,	13,	78,	286,	715,	1287,	1716,	1716,	1287,
									1,	14,	91,	364,	1001,	2002,	3003,	3432,	3003,
									1,	15,	105,455,	1365,	3003,	5005,	6435,	6435,
									1,	16, 120,560,	1820,	4368,	8008,	11440,	12870
								); //coefficients up to n = 16, 9

								
	   vec4 colorSum = vec4(0.0f);
	 
	   float multiplier;
	   float halfTap = float((tap-1.0f))/2.0f;
	   float divisor = 1.0f/pow(2.0f,tap-1.0f);
	   
	   int index = 0;
	   for(int i = 0; i < halfTap; i++)
	   {
			index = 9*(tap-2)+i;
			multiplier = float(pascal[index])*divisor;
			colorSum += texture(tex0, vec2(v2f_uv.x - dir.x*(halfTap-i)*blurSize, v2f_uv.y - dir.y*(halfTap-i)*blurSize)) * multiplier;
			colorSum += texture(tex0, vec2(v2f_uv.x + dir.x*(halfTap-i)*blurSize, v2f_uv.y + dir.y*(halfTap-i)*blurSize)) * multiplier;
		}
		//middle pixel
		index = 9*(tap-2)+int(halfTap);
		multiplier = float(pascal[index])*divisor;
		colorSum += texture(tex0, v2f_uv) * multiplier;
		//out_color = texture(tex0, v2f_uv);

		out_color = colorSum;
	
}