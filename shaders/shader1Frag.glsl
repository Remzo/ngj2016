#version 330 core

layout (location=0) out vec4 out_color;

in vec4 v2f_color;

void main()
{
    //out_color = vec4(1,1,1,1);
	out_color = v2f_color;
}