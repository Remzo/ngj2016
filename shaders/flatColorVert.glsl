#version 330 core

layout(location=0) in vec4 in_position;
layout(location=1) in vec4 in_normal;
layout(location=2) in vec4 in_color;

out vec4 v2f_color;
out vec4 v2f_normal;
out vec4 v2f_camDir;

// Model, View, Projection matrix.
uniform mat4 MVP;
uniform mat4 Model;
uniform vec4 color;
uniform vec3 camDir;

void main()
{
    gl_Position = MVP * in_position;
    // Just pass the color through directly.
    v2f_color = color*in_color;
	v2f_normal = Model * in_normal; // keep in world space
	v2f_camDir = vec4(camDir,1);
}