#include "pch.h"

#include "TitleState.h"
#include <functional>
#include <random>
#include "StateManager.h"

TitleState::TitleState(float halfHeight, float halfWidth, StateManager* stateManager) : State(stateManager)
{
}

TitleState::~TitleState()
{	
}

void TitleState::Tick(float a_DT)
{
	this->HandleInput();

	//float stickDelta  = Input.GetStickClockwiseDelta(0);
	//glm::vec2 stickState = Input.GetStickState(0);
	//Log.WriteDebug("RightStickState: %.2f, %.2f", stickState.x, stickState.y);
	//Log.WriteDebug("RightStickDelta: %.2f", stickDelta);
	//Log.WriteDebug("RighStickNeutral: %s", (Input.IsStickNeutral(0)) ? "YES" : "NO");
}

void TitleState::UpdateAccumulators(float a_DT)
{

}

void TitleState::UpdateTitle(float a_DT)
{
	

}

void TitleState::HandleInput()
{
	if (Input.GetKeyDown(SDL_SCANCODE_SPACE))
	{
		m_StateManager->SetState(StateManager::States::PLAY);
	}
}

void TitleState::Render()
{	
}
