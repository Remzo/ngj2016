#pragma once

#include "Model.h"

class Collider2D : public Model // but really it's a 2D polygon/shape
{
public:
	Collider2D();
	~Collider2D();

	void SetPoints(std::vector<glm::vec3> points, float scale = 1.0f);

	void RescaleCollider(float scale);
	std::vector<glm::vec3> GetCollisionPoints();

private:
	std::vector<glm::vec3> m_Points;
};
