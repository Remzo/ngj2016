#pragma once

#include "sphere.h"

class Axe;
class Sprite;
class Ship;
class Octopus;

class Player : public Sphere
{
public:
	enum PlayerStates
	{
		DEAD = -1,
		ALIVE,
		ATTACK,
		CHARGING,
		STUNNED

	};

	Player(float radius = 1.0f);
	~Player();

	void Tick(float a_DT);
	void HandleInput();
	void Render();

	void Stun(float stunDuration = 1.0f);

	void SetControllerID(int controllerID);

	void SetState(PlayerStates newState); 
	void SetShip(Ship* ship);
  Octopus* m_Octopus;

private:
	//constant tweakables
	//movement
	const float c_Acceleration = 640.0f;
	const float c_Friction = 20.0f;
	//attack
	const float c_AttackDuration = 0.5f;
	const float c_ChargeDuration = 2.0f;
	const float c_ChargingFriction = 50.0f;
	//stunned
	const float c_StunDuration = 1.0f;
	const float c_SpriteScale = 10.0f;
	//axe
	const float c_AxeOffsetMagnitude = 10.0f;

	int m_ControllerID = -1;

	glm::vec3 m_Velocity;
	glm::vec3 m_InputDir;

	glm::vec3 m_AxeOffset;

	PlayerStates m_CurrentState = ALIVE;

	bool b_Attacking = false;
	bool b_Charging = false;
	bool b_Stunned = false;
	bool b_Moving = false;

	//timers
	float m_AttackTimer = 0.0f;
	float m_ChargeTimer = 0.0f;
	float m_StunTimer = 0.0f;
	float m_PulseTimer = 0.0f;

	Axe* m_Bucket;

	bool b_ValidControllerConnected = false;


	void UpdateTimers(float a_DT);
	void UpdateTargetAxePosition();

	void StartCharging();
	void CancelCharging();
	void ChargingComplete();

	void PulseAxe();

	void Attack();

	Sprite* m_Sprite;
	Sprite* m_StunSprite;

	Ship* m_Ship;
};
