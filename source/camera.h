#pragma once

class Camera
{
public:
	Camera();
	~Camera();

	enum ProjectionType
	{
		UNINITIALISED = 0,
		PERSPECTIVE,
		OTHROGRAPHIC
	};

	void Tick(const float& a_deltaTime);

	void SetPosition(const glm::vec3& pos);
	glm::vec3 GetPosition() const;

	void SetMovementSpeed(const float speed);
	float GetMovementSpeed() const;
	void SetRotationSpeed(const float speed);
	float GetRotationSpeed() const;
	void FreezeCamera(bool freeze);

	void RotateEuler(const float angle, const glm::vec3& rotAxis, const bool local = true);

	void SetRotation(const glm::quat& rot);
	glm::quat GetRotation() const;
	void SetRotationEuler(const glm::vec3& anglesInDeg);
	glm::vec3 GetRotationEuler() const;
	glm::vec3 ForwardVec() const;
	ProjectionType GetProjectionType() const {
		return m_ProjType;
	};

	void Reset();

	// Translate the camera by some amount. If local is TRUE (default) then the translation should
	// be applied in the local-space of the camera. If local is FALSE, then the translation is 
	// applied in world-space.
	void Translate(const glm::vec3& translation, bool local = true);

	void SetPerspective(float fovDeg,int scrWidth, int scrHeight, float zNear, float zFar);
	void SetOrthographic(const float left, const float right, const float bottom, const float top, const float zNear, const float zFar);
	glm::mat4 GetProjectionMatrix();
	glm::mat4 GetViewMatrix();

private:

	ProjectionType m_ProjType = UNINITIALISED;

	float m_SpeedIncreaseFactor = 5.0f;

	float m_MovementSpeed = 10.0f;
	float m_RotationSpeed = 90.0f;
	void HandleInput(const float& a_deltaTime);

	void UpdateViewMatrix();

	bool m_DirtyViewMatrix = true;

	glm::vec3 m_Position;
	glm::quat m_Rotation;

	glm::mat4 m_ProjectionMatrix;
	glm::mat4 m_ViewMatrix;

	bool b_FreezeCamera = false;
};

extern Camera& MainCamera;
