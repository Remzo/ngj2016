#pragma once

#include "State.h"
#include <unordered_map>

class StateManager;
class Boat;
class Quad;
class Skybox;
class PlayState : public State
{
public:
	PlayState(float halfHeight, float halfWidth, StateManager* stateManager);
	~PlayState();

private:
	void Tick(float a_DT);
	void PhysicsTick(float a_DT);
	void Render();

	void HandleInput(float a_DT);

	Boat* m_Boat;
	Quad* m_Water;
	Skybox* m_Skybox;
};
