#include "pch.h"

#include "texture.h"

Texture::Texture(GLuint textureID, int width, int height)
{
	m_ID = textureID;
	m_Height = height;
	m_Width = width;
}

Texture::Texture(const char* texPath, int width, int height)
{
	//TODO: create texture manager that hashes texture strings and returns the texture ID if the texture has already been loaded instead of loading it again. unload all textures on destroy.
	this->m_ID = SOIL_load_OGL_texture(texPath, SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, SOIL_FLAG_MULTIPLY_ALPHA);

	if (this->m_ID == NULL) 
	{
		Log.WriteFatal("Image not loaded: %s", texPath);
		assert(0);
	}

	this->m_Height = height;
	this->m_Width = width;
}

Texture::~Texture()
{
	if (m_ID != NULL)
	{
		glDeleteTextures(1, &m_ID);
	}
}
