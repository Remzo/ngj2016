#pragma once

class Collider;
class CollisionHandler
{
public:

	enum CollisionTypes
	{
		STATIC = 0,
		DYNAMIC
	};

	struct CollisionInfo
	{
		glm::vec3 collisionPoint;
		glm::vec3 collisionNormal;
		float	  penetration;
	};

	CollisionHandler();
	~CollisionHandler();

	void Tick(float a_DT);
	void RegisterCollider(Collider* col);

	void TestCollisions();
	void ResolveCollisions();
	bool TestCollision(Collider* obj1, Collider* obj2, CollisionInfo& colInfo);
	void ClearRegisteredColliders();

private:
	bool CheckOverlapOnColliderFaceAxes(Collider* colFaces, Collider* col2) const;
	float CalcMinLengthInterval(Collider* col, glm::vec3 axis) const;
	void GetMinMaxProjection(Collider* col, float* min, float* max, glm::vec3 projAxis);
	bool TestAxis(Collider* obj1, Collider* obj2, glm::vec3 axisNormal, float& penetration);
	bool SatTest(Collider* obj1, Collider* obj2, CollisionInfo& colInfo);

	std::vector<Collider*> m_StaticColliders;
	std::vector<Collider*> m_DynamicColliders;

	std::vector<std::pair<std::pair<Collider*, Collider*>, CollisionInfo>> m_CollisionsToResolve;
};
extern CollisionHandler& g_CollisionManager;