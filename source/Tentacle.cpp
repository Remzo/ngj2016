#include "pch.h"

#include "Tentacle.h"
#include "Model.h"
#include "Collider.h"
#include "Line.h"
#include "Cannonball.h"
#include "Player.h"
#include "TentacleTip.h"

#define DEBUG_LINE_OFFSET glm::vec3(0.0f, 1.0f, 0.0f)
#define DEBUG_KEYBOARD_CONTROLS 0

static GLfloat verticesCube[] = {
  1.0f,  1.0f,  -1.0f, // Top Right
  1.0f,  -1.0f, -1.0f, // Bottom Right
  -1.0f, -1.0f, -1.0f, // Bottom Left
  -1.0f, 1.0f,  -1.0f, // Top Left

  1.0f,  1.0f,  1.0f, // Top Right
  1.0f,  -1.0f, 1.0f, // Bottom Right
  -1.0f, -1.0f, 1.0f, // Bottom Left
  -1.0f, 1.0f,  1.0f, // Top Left
};

static GLuint indicesCube[] = {
  0, 1, 3, 1, 2, 3, // Front
  7, 5, 4, 7, 6, 5, // Back

  4, 0, 7, 0, 3, 7, // Top
  1, 5, 2, 5, 6, 2, // Bottom

  4, 5, 0, 5, 1, 0, // Right
  3, 2, 7, 2, 6, 7, // Left
};


Tentacle::Tentacle(const std::vector<glm::vec3>& points)
{
  m_Points = points;
  m_StartPoints = m_Points;
  m_Velocities.resize(m_Points.size());
  for (auto& vel : m_Velocities) {
    vel = { 0, 0, 0 };
  }

  m_Forces.resize(m_Points.size());
  m_TargetLengths.resize(m_Points.size() - 1);

  for (auto& length : m_TargetLengths) {
    length = 0.5f;
  }

  {
    glGenVertexArrays(1, &VAOCube);
    glGenBuffers(1, &VBOCube);
    glGenBuffers(1, &EBOCube);

    glBindVertexArray(VAOCube);

    glBindBuffer(GL_ARRAY_BUFFER, VBOCube);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verticesCube), verticesCube,
      GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOCube);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicesCube), indicesCube,
      GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat),
      (GLvoid *)0);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

  {
    glGenBuffers(1, &BrushesBO);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, BrushesBO);
    glBufferData(GL_SHADER_STORAGE_BUFFER, brushes.size() * sizeof(Brush), brushes.data(), GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, BrushesBO);
  }
}

Tentacle::~Tentacle()
{
}

const float MIN_RADIUS = 0.05 * 15;
const float MAX_RADIUS = 0.15 * 25;

using namespace glm;

float fSphere(glm::vec3 p, float r) {
  return length(p) - r;
}

// Distance to line segment between <a> and <b>, used for fCapsule() version 2below
float fLineSegment(vec3 p, vec3 a, vec3 b) {
  vec3 ab = b - a;
  float t = clamp(dot(p - a, ab) / dot(ab, ab), 0.0f, 1.0f);
  return length((ab*t + a) - p);
}

// Plane with normal n (n is normalized) at some distance from the origin
float fPlane(vec3 p, vec3 n, float distanceFromOrigin) {
  return dot(p, n) + distanceFromOrigin;
}

// Capsule version 2: between two end points <a> and <b> with radius r 
float fCapsule(vec3 p, vec3 a, vec3 b, float r) {
  return fLineSegment(p, a, b) - r;
}

float opU(float d1, float d2) { return min(d1, d2); }

float opS(float d1, float d2) { return max(-d1, d2); }

float opI(float d1, float d2) { return max(d1, d2); }

float opUSoft(float d1, float d2, int hardness) {
  float k = 1.0f / hardness;
  float h = clamp(0.5 + 0.5*(d2 - d1) / k, 0.0, 1.0);
  return mix(d2, d1, h) - k*h*(1.0 - h);
}

float Tentacle::GetDistance(glm::vec3 samplePos) {
  float minDist = 100000;
  for (int i = 0; i < brushes.size(); i++) {
    Brush b = brushes[i];
    float dist = 0;


    switch (b.OpInfo.z) {
      case 0: dist = fSphere(b.Position - samplePos, b.Radius); break;
      case 1: vec3 end = { b.AdditionalArg.x, b.AdditionalArg.y, b.AdditionalArg.z}; dist = fCapsule(samplePos, b.Position, end, b.Radius); break;
    }

    switch (b.OpInfo.x) {
      case 0: minDist = opU(dist, minDist); break;
      case 1: minDist = opUSoft(dist, minDist, b.OpInfo.y); break;
      case 2: minDist = opI(dist, minDist); break;
      case 3: minDist = opS(dist, minDist); break;
    }
  }

  return minDist;
}

void Tentacle::ResetPoints()
{
	m_Points = m_StartPoints;
	//glm::vec3 dirToStart;
	//for (int i = 0; i < m_Points.size(); i++)
	//{
	//	dirToStart = m_StartPoints[i] - m_Points[i];
	//	m_Velocities[i] += dirToStart;
	//}

}

void Tentacle::Render()
{
  glm::vec3 min = m_Points[0];
  glm::vec3 max = m_Points[0];

  brushes.clear();
  for (int i = 0; i < m_Points.size() - 1; i++) {
    glm::vec3& start = m_Points[i];
    glm::vec3& end = m_Points[i + 1];

    min = glm::min(start, min);
    max = glm::max(start, max);

    glm::vec3 dir = glm::normalize(end - start);

    float interp = float(i) / m_Points.size();
    interp *= interp;
    float radius = (1.0f - interp) * (MAX_RADIUS - MIN_RADIUS) + MIN_RADIUS;

    brushes.push_back({ start, radius, glm::vec4(end, 0), glm::ivec4(BrushOp::SoftUnion, 1, PrimType::Capsule, 0) });
  }

  if (m_InHole) {
    brushes.push_back({ 
      m_Points[m_Points.size() - 1] + glm::vec3(0, 0.2, 0), 
      MIN_RADIUS * 2, 
      glm::vec4(m_Points[m_Points.size() - 1] - glm::vec3(0, 0.2, 0), 0), 
      glm::ivec4(BrushOp::SoftUnion, 1, PrimType::Capsule, 0) });
  }



  min = glm::min(m_Points[m_Points.size() - 1], min);
  max = glm::max(m_Points[m_Points.size() - 1], max);

  min -= glm::vec3(MAX_RADIUS);
  max += glm::vec3(MAX_RADIUS);

  glm::vec3 center = (min + max) * 0.5f;
  glm::vec3 extends = max - min;

  this->SetPosition(center);
  this->SetScale(extends * 0.5f);

  // RAYMARCH
  auto shader = g_ShaderLoader.GetShader(ShaderType::RAYMARCH_SDF);
  glUseProgram(shader);

  // DISTANCE BRUSHES
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, BrushesBO);
  glBufferData(GL_SHADER_STORAGE_BUFFER, brushes.size() * sizeof(Brush), brushes.data(), GL_DYNAMIC_DRAW);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, BrushesBO);

  Camera* cam = &MainCamera;

  //Set MVP
  glm::mat4 modelMatrix = this->GetModelMatrix();
  glm::mat4 mvp = cam->GetProjectionMatrix() * cam->GetViewMatrix() * modelMatrix;
  GLint uniformModel = glGetUniformLocation(shader, "model");
  GLint uniformView = glGetUniformLocation(shader, "view");
  GLint uniformProj = glGetUniformLocation(shader, "proj");


  glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(this->GetModelMatrix()));
  glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(MainCamera.GetViewMatrix()));
  glUniformMatrix4fv(uniformProj, 1, GL_FALSE, glm::value_ptr(MainCamera.GetProjectionMatrix()));
  
  glUniform3f(glGetUniformLocation(shader, "eyePos"), 
    cam->GetPosition().x, cam->GetPosition().y, cam->GetPosition().z);

  auto tipPos = m_Points[m_Points.size() - 1];
  glUniform3f(glGetUniformLocation(shader, "tipPos"),
    tipPos.x, tipPos.y, tipPos.z);

  auto tipColor = glm::vec4(0.0);

  switch (tipState) {
    case TipState::DEFAULT: break;
    case TipState::LEFT: tipColor = glm::vec4(0.3f, 0.4f, 0.9f, 1.0f); break;
    case TipState::RIGHT: tipColor = glm::vec4(0.21f, 0.85f, 0.4f, 1.0f);  break;
    case TipState::POKE: tipColor = glm::vec4(0.9f, 0.2f, 0.2f, 1.0f);  break;
  }

  glUniform4f(glGetUniformLocation(shader, "tipColor"),
    tipColor.x, tipColor.y, tipColor.z, tipColor.w);


  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glBindVertexArray(VAOCube);
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

  /*for (int i = 0; i < m_Points.size() - 1; i++) {
    glm::vec3& start = m_Points[i];
    glm::vec3& end = m_Points[i + 1];
    Line line{ start , end };
    line.SetColor(glm::vec4(1, 0, 0, 1));
    line.Render();
  }*/
}

glm::vec3 safeNormalize(glm::vec3 vec) {
  if (glm::length(vec) == 0) return{ 0, 0, 0 };
  return glm::normalize(vec);
}

void Tentacle::Update(float a_DT)
{
  float targetDistance = 0.5;
  float stiffness = 5;

  for (auto& force : m_Forces) {
    force = { 0, 0, 0 };
  }

  float length = 0;

  for (int i = 0; i < m_Points.size() - 1; i++) {
    glm::vec3& start = m_Points[i];
    glm::vec3& end = m_Points[i + 1];

    glm::vec3 dir = end - start;

    float distance = glm::length(dir);
    length += distance;

    dir = glm::normalize(dir);
    float error = distance - m_TargetLengths[i];

    m_TargetLengths[i] += error * a_DT * 0.25;
    m_TargetLengths[i] -= 0.1 * a_DT;
    m_TargetLengths[i] = glm::clamp(m_TargetLengths[i], 0.5f, 0.75f);
    float stiffnessLerp = float(i) / m_Points.size();
    stiffnessLerp *= stiffnessLerp;

    float stiffness = 800.0f + (1300.0f - 800.0f) * stiffnessLerp;
    float weight = 5.0f + (30.0f - 5.0f) * stiffnessLerp;


    if (error > 0) {
      m_Forces[i] += (stiffness * error * dir - 100.0f * m_Velocities[i]) / weight;
      m_Forces[i + 1] += (stiffness  * error * -dir - 100.0f * m_Velocities[i + 1]) / weight;
    } else {
      m_Forces[i] += (100.0f * error * dir - 120.0f * m_Velocities[i]) / weight;
      m_Forces[i + 1] += (100.0f * error * -dir - 120.0f * m_Velocities[i + 1]) / weight;
    }

    glm::vec3 perpDir = glm::vec3(-dir.z, dir.y, dir.x);
    m_Forces[i] += -80.0f * glm::dot(m_Velocities[i], perpDir) * perpDir;
  }

  m_Forces[m_Points.size() - 1] *= 0.0f;
  m_TargetLengths[m_Points.size() - 2] = 0.5;

  auto playerPos = player->GetPosition();

  for (int i = 1; i < m_Points.size(); i++) {
    auto playerDir = playerPos - m_Points[i];
    playerDir.y = 0;
    auto playerDist = glm::length(playerDir);

    if (playerDist < 15.0) {
      m_Forces[i] += -100.0f * glm::normalize(playerDir) * (15.0f - playerDist);
    }

    m_Velocities[i] += m_Forces[i] * a_DT;

    if(glm::length(m_Velocities[i]) < 0.9f) continue;

    auto prevPos = m_Points[i];
    auto futurePos = m_Points[i] + m_Velocities[i] * a_DT + m_Forces[i] * a_DT * a_DT;

    if (futurePos.z < 25.0f || futurePos.z > 75.0f || futurePos.x < -70.0f || futurePos.x > 74) {
      m_Velocities[i] = glm::vec3(0);
    } else {
      m_Points[i] = futurePos;
    }
  }

  /*if (length < 5) {
    for (int i = 0; i < m_Points.size() - 2; i++) {
      glm::vec3& start = m_Points[i];
      glm::vec3& end = m_Points[i + 1];

      glm::vec3 dir = end - start;

      float distance = glm::length(dir);

      if (distance > 1.0) {
        m_TargetLengths[i] = distance * 0.15;
        m_TargetLengths.insert(m_TargetLengths.begin() + i + 1, distance * 0.1);
        m_Points.insert(m_Points.begin() + i + 1, start + dir * 0.5f);
        m_Forces.insert(m_Forces.begin() + i + 1, glm::vec3(0, 0, 0));
        m_Velocities.insert(m_Velocities.begin() + i + 1, m_Velocities[i + 1]);
      }
    }
  }*/
}

void Tentacle::SetInHole(bool inHole) {
  m_InHole = inHole;
}

void Tentacle::AddVelocitiesToPoint(glm::vec3 point)
{
	glm::vec3 pointToStart;
	const float retractFactor = 5.0f;

	for (int i = 0; i < m_Velocities.size(); i++)
	{
		pointToStart = point - m_Points[i];
		m_Velocities[i] += pointToStart * retractFactor;
	}
	//this->MoveEndpoint(m_Points[m_Points.size()-1]-point, 1.0f);
}

void Tentacle::MoveEndpoint(const glm::vec3& dist, float a_DT) {
  //m_Points[m_Points.size() - 1] += dist * a_DT;
  m_Velocities[m_Points.size() - 1] = dist;
  m_Velocities[m_Points.size() - 2] += dist * 0.2f;
}