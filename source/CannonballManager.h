#pragma once

class Cannonball;
class Boat;
class CannonballManager
{
public:
	CannonballManager();
	~CannonballManager();

	void UpdateCannonballs(float a_DT);
	void Render();

	Cannonball* CreateCannonball(unsigned int ownerID, glm::vec3 pos, glm::vec3 vel); //returns pointer to the new cannonball

private:
	struct CannonballInfo
	{
		Cannonball* m_Ball;
		float m_Lifetime;
	};
	std::vector<CannonballInfo> m_Cannonballs;
};

extern CannonballManager& g_CannonballManager;