#include "Object.h"
#include "Model.h"
#include "Sphere.h"
#include <memory>
#include "Tentacle.h"

class Tentacle;
class Player;

class TentacleTip: public Object 
{
	int holeIndex;
public:
	TentacleTip(int index);
  void SetPlayer(Player * player);
	Sphere* modeltest;
  std::unique_ptr<Tentacle> tentacle;

	glm::vec3 velocity;
	const float c_Acceleration = 720.0f;
	const float c_Friction = 20.0f;
	
	
	void Render();
  float GetDistance(glm::vec3 pos);
  void Freeze();
  //void Poke();
	void UpdateVelocity(glm::vec3 _inputdir, float _dt);
	void SetColor(glm::vec4 _color);
  void PhysicsTick(float _dt);
  void SetHole(int _holeindex);
  void FreeHole();
  int GetHoleIndex();

  void Stab();

	~TentacleTip();

	bool b_Stabbing = false;
	bool b_StabCooldown = false;

	glm::vec3 m_StartPos;
private:
  Tentacle::TipState prevState;
  float m_StabTimer = 0.0f;
	const float c_StabTime = 0.6f;
	const float c_StabCooldown = 1.0f;
	const float c_StabAcceleration = 16000.f;
	const float c_StabFriction = 0.5f;



};