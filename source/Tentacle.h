#pragma once

#include "GameObject.h"

class Line;
class Model;
class Player;

class Tentacle : public Object
{
private:
  bool m_InHole;
public:

  enum class TipState {
    DEFAULT,
    LEFT,
    RIGHT,
    POKE
  };

  TipState tipState = TipState::DEFAULT;

  Tentacle(const std::vector<glm::vec3>& points);
	~Tentacle();
  
	void Update(float a_DT);
  void SetInHole(bool inHole);
	void Render();
  void MoveEndpoint(const glm::vec3& dist, float a_DT);

  void AddVelocitiesToPoint(glm::vec3 point);

  float GetDistance(glm::vec3 samplePos);

  void ResetPoints();

  Player* player;

private:
  enum BrushOp : int32_t {
    Union = 0,
    SoftUnion = 1,
    Intersect = 2,
    Subtract = 3
  };

  enum PrimType : int32_t {
    Sphere = 0,
    Capsule = 1,
  };

  struct Brush {
    glm::vec3 Position;
    float Radius;
    glm::vec4 AdditionalArg;
    glm::ivec4 OpInfo;
  };

  std::vector<glm::vec3> m_Points;
  std::vector<glm::vec3> m_StartPoints;
  std::vector<glm::vec3> m_Velocities;
  std::vector<glm::vec3> m_Forces;
  std::vector<float> m_TargetLengths;

  GLuint VAOCube, VBOCube, EBOCube, BrushesBO;

  std::vector<Brush> brushes = {
    { glm::vec3(0.8f, 0.5f, 0.8f), 0.2f, glm::vec4(0, 0, 0, 0), glm::ivec4(BrushOp::Union, 0, PrimType::Sphere, 0) },  };
};
