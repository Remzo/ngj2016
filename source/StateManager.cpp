#include "pch.h"

#include "StateManager.h"
#include "State.h"
#include "TitleState.h"
#include "PlayState.h"
#include "KeyPressPrototypeState.h"
#include "LaviniaState.h"
#include "BucketPrototypeState.h"
#include "CollisionDebugState.h"
#include "Texture.h"

StateManager::StateManager() : m_HalfGameHeight(GlobalInfo.ScreenHeight / 2.f), m_HalfGameWidth(GlobalInfo.ScreenWidth/2.f)
{
	this->m_State = new BucketPrototypeState(this->m_HalfGameHeight, this->m_HalfGameWidth, this);
	this->m_CurState = CHARACTER_MOVE;
}

StateManager::~StateManager()
{
	delete this->m_State;
}

void StateManager::PhysicsTick(float a_DT)
{
	if (this->m_State != 0)
	{
		this->m_State->PhysicsTick(a_DT);
	}
}

void StateManager::Tick(float a_DT)
{
	this->HandleInput();

	if (this->m_State != 0)
	{
		this->m_State->Tick(a_DT);
	}
}

void StateManager::HandleInput()
{
	switch (this->m_CurState)
	{
	case States::MENU:
		break;
	case States::PLAY:
		break;
	default:
		break;
	};
}

void StateManager::Render()
{
	if (this->m_State != 0)
	{
		this->m_State->Render();
	}
}

void StateManager::SetState(States state)
{
	if (this->m_State != 0)
	{
		delete this->m_State;
	}

	g_CollisionManager.ClearRegisteredColliders();

	switch (state)
	{
	case States::MENU:
		this->m_State = new TitleState(this->m_HalfGameHeight, this->m_HalfGameWidth, this);
		break;
	case States::PLAY:
		this->m_State = new PlayState(this->m_HalfGameHeight, this->m_HalfGameWidth, this);
		break;
	case States::DEBUG:
		this->m_State = new CollisionDebugState(this->m_HalfGameHeight, this->m_HalfGameWidth, this);
    break;
  case States::KEY_PRESS_PROTOTYPE:
    this->m_State = new KeyPressPrototypeState(this->m_HalfGameHeight, this->m_HalfGameWidth, this);
    break;
	case States::LAVINIASTATE:
		this->m_State = new LaviniaState(this->m_HalfGameHeight, this->m_HalfGameWidth, this);
		break;
	case States::CHARACTER_MOVE:
		this->m_State = new BucketPrototypeState(this->m_HalfGameHeight, this->m_HalfGameWidth, this);
		break;
	default:
		assert(0);
  };

	//Reset input keys
	Input.ResetKeys();

	this->m_CurState = state;
}

