#include "pch.h"

#include "texture.h"
#include "OctopusModel.h"

OctopusModel::OctopusModel() : Model(g_ModelLoader.GetSubMeshOfModel(ModelLoader::OCTOPUS, "octo"), ShaderType::TEXTURED_UNLIT)
{
	this->SetScale(0.5f);
	this->m_Texture = new Texture("textures/octopus_tex.png", 256, 256);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Texture->GetTextureID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

}


OctopusModel::~OctopusModel()
{
}
