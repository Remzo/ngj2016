#include "pch.h"

#include "Sprite.h"
#include "texture.h"
#include "Mesh.h"

Sprite::Sprite(const char* spritesheet, int spriteSheetWidth, int spriteSheetHeight, unsigned int numAnims, unsigned int* framesPerAnim) : Quad()
{
	this->m_Texture  = new Texture(spritesheet, spriteSheetWidth, spriteSheetHeight);

	this->m_CurAnim = 0;
	this->m_CurFrame = 0;

	//get frame width, height from spritesheet

	this->m_NumAnims = numAnims;
	this->m_FramesPerAnim = new unsigned int[numAnims];

	unsigned int maxFramesInAnim = 0;
	for (unsigned int i = 0; i < m_NumAnims; i++)
	{
		this->m_FramesPerAnim[i] = framesPerAnim[i];
		if (framesPerAnim[i] > maxFramesInAnim)
		{
			maxFramesInAnim = framesPerAnim[i];
		}
	}

	this->m_FrameHeight = spriteSheetHeight / (float) numAnims;

	this->m_FrameWidth = spriteSheetWidth / (float) maxFramesInAnim;

	this->SetShaderType(ShaderType::TEXTURED_UNLIT);

	this->b_Animating = false;
}
Sprite::Sprite(Texture* _tex, unsigned int numAnims, unsigned int* framesPerAnim)
{
	this->m_Texture = _tex;

	this->m_CurAnim = 0;
	this->m_CurFrame = 0;

	//get frame width, height from spritesheet

	this->m_NumAnims = numAnims;
	this->m_FramesPerAnim = new unsigned int[numAnims];

	unsigned int maxFramesInAnim = 0;
	for (unsigned int i = 0; i < m_NumAnims; i++)
	{
		this->m_FramesPerAnim[i] = framesPerAnim[i];
		if (framesPerAnim[i] > maxFramesInAnim)
		{
			maxFramesInAnim = framesPerAnim[i];
		}
	}

	this->m_FrameHeight = _tex->GetTextureHeight() / (float) numAnims;

	this->m_FrameWidth = _tex->GetTextureWidth() / (float) maxFramesInAnim;

	this->SetShaderType(ShaderType::TEXTURED_UNLIT);

	this->b_Animating = false;
}

Sprite::~Sprite()
{
	if (m_Texture) delete m_Texture; m_Texture = 0;
	delete[] m_FramesPerAnim;
}

void Sprite::SetFrame(unsigned int frameNum)
{
	this->m_CurFrame = frameNum;
	b_UVsNeedUpdating = true;
}

void Sprite::StartAnim()
{
	b_Animating = true;
}

void Sprite::StopAnim()
{
	b_Animating = false;
}

void Sprite::SetAnim(unsigned int animNum, float secondsPerFrame)
{
	this->m_FrameTimer = 0.0f;
	this->m_CurFrame = 0;
	this->m_CurAnim = animNum;
	this->m_SecondsPerFrame = secondsPerFrame;
	b_UVsNeedUpdating = true;
}

void Sprite::Tick(float a_DT)
{
	if (b_Animating)
	{
		this->m_FrameTimer += a_DT;

		if (this->m_FrameTimer > this->m_SecondsPerFrame)
		{
			this->m_FrameTimer -= this->m_SecondsPerFrame;
			this->m_CurFrame++;
			this->m_CurFrame %= this->m_FramesPerAnim[this->m_CurAnim];
			b_UVsNeedUpdating = true;
		}
	}
}

void Sprite::Render()
{
	if (b_UVsNeedUpdating)
	{
		UpdateVerticesUVs();
	}
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Texture->GetTextureID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	Quad::Render();
	glBindTexture(GL_TEXTURE_2D, 0);

}

void Sprite::UpdateVerticesUVs()
{
	float x = 0.5f;

	int textureWidth = m_Texture->GetTextureWidth();
	int textureHeight = m_Texture->GetTextureHeight();

	float frameWidthUV = m_FrameWidth / textureWidth;
	float frameHeightUV = m_FrameHeight / textureHeight;

	float curFrameUVwidth = frameWidthUV*m_CurFrame;
	float curFrameUVheight = frameHeightUV*m_CurAnim;

	m_Vertices.clear();


	//topLeft
	m_Vertices.push_back(VertexComplete(glm::vec3(-x, x, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(curFrameUVwidth + (int)b_ReverseV * (frameWidthUV), curFrameUVheight), glm::vec4(1.0f)));
	//top right
	m_Vertices.push_back(VertexComplete(glm::vec3(x, x, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(curFrameUVwidth + frameWidthUV * (int)!b_ReverseV, curFrameUVheight), glm::vec4(1.0f)));
	//bottom right
	m_Vertices.push_back(VertexComplete(glm::vec3(x, -x, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(curFrameUVwidth + frameWidthUV * (int)!b_ReverseV, curFrameUVheight + frameHeightUV), glm::vec4(1.0f)));
	//bottom left
	m_Vertices.push_back(VertexComplete(glm::vec3(-x, -x, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(curFrameUVwidth + (int)b_ReverseV * (frameWidthUV), curFrameUVheight + frameHeightUV), glm::vec4(1.0f)));


	this->SetMesh(new Mesh(m_Vertices, m_Indices));

}