#pragma once

#include "GameObject.h"

class PhysicsObject : public virtual GameObject
{
public:
	struct Force
	{
		Force(glm::vec3 forceIN, glm::vec3 posIN){
			force = forceIN;
			position = posIN;
		};

		glm::vec3 force;
		glm::vec3 position;
	};
	struct State
	{
	};
	PhysicsObject();
	~PhysicsObject();

	float GetMass() const { return m_Mass; }
	void SetMass(float mass) { m_Mass = mass; }
	virtual void Tick(float a_DT);
	void AddForce(glm::vec3 force, glm::vec3 position = glm::vec3(0));
	void AddTorque(glm::vec3 torque);
	virtual glm::mat3 GetInertialTensor() const;

	virtual void SetVelocity(glm::vec3 vel);

	//returns force applied
	glm::vec3 AddAcceleration(glm::vec3 accel);

	glm::vec3 GetAngularMomentum() const;

private:
	void RecalculateState();
	void RecalculateRotState();
	void ApplyForces(float a_DT);
	void ApplyTorque(float a_DT);

	std::vector<Force> m_ForcesToApply;
	std::vector<glm::vec3> m_TorqueToApply;

	float m_Mass = 1.0f;
	glm::vec3 m_Momentum =glm::vec3(0);

	glm::quat m_Spin;
	glm::mat3 m_IntertialTensor = glm::mat3(1);
	glm::vec3 m_AngularVelocity = glm::vec3(0);
	glm::vec3 m_AngularMomentum = glm::vec3(0);

};
