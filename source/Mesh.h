#pragma once
#include "Object.h"

class Mesh
{
	friend class Model;
public:
	Mesh();
	Mesh(const Mesh& mesh);
	Mesh(std::vector<VertexComplete> vertexData, std::vector<unsigned int> indices = std::vector<unsigned int>());
	~Mesh();

private:

	std::vector<unsigned int> GenerateIndicesFromVertexDataSize(unsigned int vertexDataSize);

	//these two nums could perhaps be opt
	int m_NumIndices;
	int m_NumVerts;
	std::vector<VertexComplete> m_VertexData;
	std::vector<unsigned int> m_Indices;
};
