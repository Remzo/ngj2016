#include "pch.h"

#include "KeyPressPrototypeState.h"
#include <functional>
#include <random>
#include "StateManager.h"
#include "Tentacle.h"
#include <iostream>

GLfloat verticesCube[] = {
  1.0f,  1.0f,  -1.0f, // Top Right
  1.0f,  -1.0f, -1.0f, // Bottom Right
  -1.0f, -1.0f, -1.0f, // Bottom Left
  -1.0f, 1.0f,  -1.0f, // Top Left

  1.0f,  1.0f,  1.0f, // Top Right
  1.0f,  -1.0f, 1.0f, // Bottom Right
  -1.0f, -1.0f, 1.0f, // Bottom Left
  -1.0f, 1.0f,  1.0f, // Top Left
};

GLuint indicesCube[] = {
  0, 1, 3, 1, 2, 3, // Front
  7, 5, 4, 7, 6, 5, // Back

  4, 0, 7, 0, 3, 7, // Top
  1, 5, 2, 5, 6, 2, // Bottom

  4, 5, 0, 5, 1, 0, // Right
  3, 2, 7, 2, 6, 7, // Left
};

GLuint generate3DTexture(GLenum texture, GLint minMagFilter) {
  GLuint result;
  glActiveTexture(texture);
  glGenTextures(1, &result);
  glBindTexture(GL_TEXTURE_3D, result);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, minMagFilter);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, minMagFilter);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_MIRRORED_REPEAT);
  glTexParameterfv(GL_TEXTURE_3D, GL_TEXTURE_BORDER_COLOR, glm::value_ptr(glm::vec4(0)));
  return result;
}

KeyPressPrototypeState::KeyPressPrototypeState(float halfHeight, float halfWidth, StateManager* stateManager) : State(stateManager)
{
  glEnable(GL_TEXTURE_3D);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glPointSize(5);

  std::cout << "OpenGL context version: " << glGetString(GL_VERSION) << std::endl;
  std::cout << "OpenGL vendor:          " << glGetString(GL_VENDOR) << std::endl;
  std::cout << "OpenGL renderer:        " << glGetString(GL_RENDERER) << std::endl;
  std::cout << "GLSL version:           " << glGetString(GL_SHADING_LANGUAGE_VERSION)
    << std::endl;

  std::vector<glm::vec3> points;

  for (int i = 0; i < 10; i++) {
    points.push_back({ 0, i*0.125, 0 });
  }

  testTentacle = std::make_unique<Tentacle>(points);

  MainCamera.FreezeCamera(true);
}

KeyPressPrototypeState::~KeyPressPrototypeState()
{	
}

void KeyPressPrototypeState::PhysicsTick(float a_DT)
{
  testTentacle->Update(a_DT);
}

void KeyPressPrototypeState::Tick(float a_DT)
{
	this->HandleInput();
  glm::vec3 dir{ 0,0,0 };

  if (Input.GetKey(SDL_SCANCODE_W)) {
    dir.y += 1;
  }

  if (Input.GetKey(SDL_SCANCODE_S)) {
    dir.y -= 1;
  }

  if (Input.GetKey(SDL_SCANCODE_A)) {
    dir.x -= 1;
  }

  if (Input.GetKey(SDL_SCANCODE_D)) {
    dir.x += 1;
  }

  if (glm::length(dir) != 0) {
    dir = glm::normalize(dir);
  }

  testTentacle->MoveEndpoint(dir * 3.0f, a_DT);
}

void KeyPressPrototypeState::HandleInput()
{

}

void KeyPressPrototypeState::Render()
{	
  testTentacle->Render();
}
