#include "pch.h"

Globals& GlobalInfo = Globals();

#define CHILL_BLUE glm::vec4(0.3f, 0.4f, 0.9f, 1.0f)
#define CHILL_RED glm::vec4(0.9f, 0.2f, 0.2f, 1.0f)
#define CHILL_GREEN glm::vec4(0.21f, 0.85f, 0.4f, 1.0f)

glm::vec4 Globals::s_PlayerColors[8] = { CHILL_BLUE, CHILL_RED, CHILL_GREEN, glm::vec4(1.0f, 1.0f, 0.4f, 1.0f), glm::vec4(0.6f, 0.4f, 0.8f, 1.0f), glm::vec4(0.9f, 0.4f, 0.18f, 1.0f), glm::vec4(0.21f, 0.9f, 0.85f, 1.0f), glm::vec4(1.0f, 0.4f, 0.9f, 1.0f) };

