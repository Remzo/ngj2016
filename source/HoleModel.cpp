#include "pch.h"

#include "HoleModel.h"
#include "texture.h"

HoleModel::HoleModel() : Model(g_ModelLoader.GetSubMeshOfModel(ModelLoader::HOLE, "Union"), ShaderType::TEXTURED_UNLIT)
{
	/*this->SetScale(0.5f);*/
	this->m_Texture = new Texture("textures/brokenhole.png", 64, 64);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Texture->GetTextureID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	float randVal = rand();
	randVal /= (float)RAND_MAX;

	this->SetRotationEuler(glm::vec3(0.0f, randVal * 360.0f, 0.0f));
}

HoleModel::~HoleModel()
{
	delete m_Texture;
}