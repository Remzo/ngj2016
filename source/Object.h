#pragma once

class Collider;
class Object
{
public:
	Object();
	~Object();

	virtual void SetPosition(const glm::vec3& pos);
	virtual void Translate(const glm::vec3& translation, bool local = true);

	virtual glm::vec3 GetPosition(bool local = false) const;

	virtual void SetRotation(const glm::quat& rot);
	virtual glm::quat GetRotation(bool local = true) const;
	virtual void SetRotationEuler(const glm::vec3& anglesInDeg);
	virtual void RotateEuler(const float angle, const glm::vec3& rotAxis, const bool local = true);
	glm::vec3 GetRotationEuler() const;
	glm::vec3 ForwardVec()  const;
	glm::vec3 UpVec()  const;
	glm::vec3 RightVec()  const;

	Collider* GetCollider() const { return m_Collider; }

	virtual void SetScale(float scale);
	virtual void SetScale(glm::vec3 scale);
	glm::vec3 GetScale() { return m_Scale; }

	bool IsModelMatrixDirty() const;
	void SetModelMatrix(glm::mat4 mat);
	glm::mat4 GetModelMatrix();

	void SetParent(Object* obj);

	void SetDirtyModelMatrix();

protected:

	glm::vec3 m_Position;
	glm::quat m_Rotation;
	glm::vec3 m_Scale;
	Collider* m_Collider;

private:
	std::vector<Object*> m_Children;
	Object* m_Parent;

	bool b_DirtyMatrix;
	glm::mat4 m_ModelMatrix; //heirarchry/scene graph
	glm::mat4 m_LocalModelMatrix;
	void UpdateModelMatrix();
};