#include "pch.h"

#include <time.h>
#include "window.h"
#include "game.h"

#define MAX_DELTA_TIME 0.1f

bool InitSDL( Window& a_window ) {
	if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) { 
		Log.WriteFatal( "%s", SDL_GetError() );
		return false;
	}
	else {
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 ); // Without these 2 lines, SDL will create a GL 2.x context 
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 ); 
		SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 ); 

		SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG );
    
		SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 1 );
		SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 1 ); //or 16

		a_window.m_pSDLwindow = SDL_CreateWindow( "OpenGL Game Template", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, a_window.m_windowWidth, a_window.m_windowHeight, SDL_WINDOW_OPENGL/*|SDL_WINDOW_RESIZABLE*/ );
		if ( a_window.m_pSDLwindow == NULL ) { 
			Log.WriteFatal( "%s", SDL_GetError() );
			return false;
		}

		a_window.m_glContext = SDL_GL_CreateContext( a_window.m_pSDLwindow );
	}

	return true;
}

bool InitOpenGL() {
	// Initialising openGL using glew
	glewExperimental = GL_TRUE;
	if( glewInit() != GLEW_OK ) {
		Log.WriteFatal( "Error in glew init!" );
		return false;
	}

	// Check for 3.3 support.
	if ( !GLEW_VERSION_3_3 ) {
		Log.WriteFatal( "OpenGL 3.3 required version support not present." );
		return false;
	}

	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClearDepth( 1.0f );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_CULL_FACE );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	//SDL_ShowCursor(0);
	return true;
}

void WindowResized( Window& a_window ) {
	
}

void CheckEvent( const SDL_Event& a_event, bool& a_quit, Window& a_window, Game& a_game ) {
	switch ( a_event.type ) {
	case SDL_QUIT:
		a_quit = true;
		break;
	case SDL_KEYDOWN:
		Input.SetHold( a_event.key.keysym.scancode, true );
		break;
	case SDL_KEYUP:
		Input.SetHold( a_event.key.keysym.scancode, false );
		break;
	case SDL_MOUSEMOTION:
		Input.SetMouseXY( a_event.motion.x, a_event.motion.y );
		break;
	case SDL_MOUSEBUTTONDOWN:
		Input.SetMouseHold( a_event.button.button, true );
		break;
	case SDL_MOUSEBUTTONUP:
		Input.SetMouseHold( a_event.button.button, false );
		break;
	case SDL_WINDOWEVENT:
		switch( a_event.window.event ) {
		case SDL_WINDOWEVENT_RESIZED:
			a_window.m_windowWidth = a_event.window.data1;
			a_window.m_windowHeight = a_event.window.data2;
			WindowResized( a_window );
			a_game.OnWindowResize();
			break;
		}
		break;
	}
}

int main() {
	GlobalInfo.ScreenWidth = 1280;
	GlobalInfo.ScreenHeight = 800;

	Window window(GlobalInfo.ScreenWidth, GlobalInfo.ScreenHeight); //hardcoded resolution, do not change plz (or if you do just keep the aspect ratio)				
	if( !InitSDL( window ) ) { return 1; }		
	if( !InitOpenGL() ) { return 1; }

	srand((unsigned int)time(0));

	bool quit = false;										
	Game& game = Game( quit, &window );						
	game.Init();											

	const float divThousand = 1/1000.0f;

	const float PHYSICS_TIMESTEP = 1.0f/60.0f;
	float physicsTime = 0.0f;
	float timeToSimulate = 0.0f;

   float previousTime = SDL_GetTicks()*divThousand;
   float currentTime = SDL_GetTicks()*divThousand;
   float deltaUpdateTime = 0;

	SDL_Event event;
	Log.WriteInfo( "Started." );
	while( !quit ) {
		currentTime = SDL_GetTicks()*divThousand;
		deltaUpdateTime = glm::min(currentTime - previousTime, MAX_DELTA_TIME);
        previousTime = currentTime;
		physicsTime = 0.0f;

		while ( SDL_PollEvent( &event ) ) {
			CheckEvent( event, quit, window, game );
		}
		if( Input.GetKeyDown( SDL_SCANCODE_ESCAPE ) ) {
			game.Quit();
		}

		Input.Tick(deltaUpdateTime);
		game.Tick( deltaUpdateTime );
		while( physicsTime+PHYSICS_TIMESTEP < deltaUpdateTime ) 
		{
			game.PhysicsTick( PHYSICS_TIMESTEP );
			physicsTime += PHYSICS_TIMESTEP;
		}
		game.PhysicsTick(deltaUpdateTime - physicsTime);

		Input.ResetHoldChange();
		Input.ResetXInputs();

		game.Render();																			

		SDL_GL_SwapWindow( window.m_pSDLwindow );
	}
	game.Clean();
	Log.WriteInfo( "Exited." );
	
	SDL_Quit();
	return 0;
}