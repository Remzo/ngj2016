#include "pch.h"

#include "PlayState.h"
#include <algorithm>
#include <random>
#include <ctime>
#include "StateManager.h"
#include "Model.h"
#include "Quad.h"
#include "Boat.h"
#include "skybox.h"
#include "Collider.h"
#include "Line.h"

#include "Cannonball.h"

auto engine2 = std::default_random_engine((unsigned int)time(0));
bool echoToggle = false;

glm::vec3 boatVel(10.0f, 0.0f, 0.0f);

Boat* boats[8];

PlayState::PlayState(float halfHeight, float halfWidth, StateManager* stateManager) : State(stateManager)
{	
	//init MainCamera
	MainCamera.Reset();
	//MainCamera.SetOrthographic(-GlobalInfo.m_HalfGameWidth, GlobalInfo.m_HalfGameWidth, -GlobalInfo.m_HalfGameHeight, GlobalInfo.m_HalfGameHeight, 0.01f, 1000.0f);
	MainCamera.SetPosition(glm::vec3(0, 30, 0));
	MainCamera.SetRotationEuler(glm::vec3(-90.0f, 0.0f, 0.0f));
	
	m_Boat = new Boat(0, 0);
	g_CollisionManager.RegisterCollider(m_Boat->GetCollider());
	
	m_Water = new Quad();
	m_Water->RotateEuler(-90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	m_Water->SetScale(100.0f);
	m_Water->SetColor(glm::vec4(0.0f, 0.0f, 1.0f, 0.3f));
	m_Water->SetShaderType(ShaderType::FLAT_CAMERA_LIT);

	//init skybox
	this->m_Skybox = new Skybox(g_ShaderLoader.GetShader(ShaderType::SKYBOX), "textures/cubemap/greenNeb_left.png", "textures/cubemap/greenNeb_right.png", "textures/cubemap/greenNeb_top.png", "textures/cubemap/greenNeb_bottom.png", "textures/cubemap/greenNeb_front.png", "textures/cubemap/greenNeb_back.png");


	float sector = (glm::pi<float>()*2.0f) / 8;
	for (unsigned int i = 0; i < 8; i++)
	{
		boats[i] = new Boat(i);

		boats[i]->SetPosition(glm::vec3(-glm::cos(sector*i), 0.0f, glm::sin(sector*i))*5.0f);
		boats[i]->SetRotationEuler(glm::vec3(0.0f, sector*i*(180.0f/glm::pi<float>()), 0.0f));
	}
}

PlayState::~PlayState()
{
	delete m_Boat;
	delete m_Water;
	for (int i = 0; i < 8; i++)
	{
		delete boats[i];
	}
	delete m_Skybox;
}

void PlayState::Render()
{
	//clear
	glm::vec3 clearColor = glm::vec3(1.0f, 0.0f, 1.0f);
	glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	//skybox
	m_Skybox->Draw();

	//water
	m_Water->Render();

	//boats
	m_Boat->Render();
	for (int i = 0; i < 8; i++)
	{
		boats[i]->Render();
	}
}

void PlayState::PhysicsTick(float a_DT)
{
	m_Boat->UpdatePhysics(a_DT);
}

void PlayState::Tick(float a_DT)
{
	HandleInput(a_DT);

	for (int i = 0; i < 8; i++)
	{
		boats[i]->RotateEuler(-5.0f*a_DT, glm::vec3(0.0f, 1.0f, 0.0f));
		boats[i]->Translate(glm::vec3(0.0f, 0.0f, -1.0f) * a_DT);
	}

	m_Boat->Update(a_DT);
}

void PlayState::HandleInput(float a_DT)
{
	m_Boat->HandleInput(SDL_SCANCODE_I, SDL_SCANCODE_L, SDL_SCANCODE_J, SDL_SCANCODE_K);

	if (Input.GetKeyDown(SDL_SCANCODE_P))
	{
		g_DebugManager.b_PausePhysics = !g_DebugManager.b_PausePhysics;
	}
}
