#pragma once

#include "Quad.h"

class Texture;
class Sprite : public Quad
{
public:
	Sprite(const char* spritesheet, int spriteSheetWidth, int spriteSheetHeight, unsigned int numAnims, unsigned int* framesPerAnim);
	Sprite(Texture* _tex, unsigned int numAnims = 0, unsigned int* framesPerAnim = NULL);
	~Sprite();

	void Tick(float a_DT);

	void SetFrame(unsigned int frameNum);
	void Render();

	void SetAnim(unsigned int animNum, float secondsPerFrame);
	void StartAnim();
	void StopAnim();

	void SetDirtyUVs() { b_UVsNeedUpdating = true; }

	bool b_ReverseV = false;
private:

	float m_FrameTimer;
	float m_SecondsPerFrame;

	bool b_Animating;
	bool b_UVsNeedUpdating = false;
	void UpdateVerticesUVs();

	unsigned int m_NumAnims;
	unsigned int m_CurAnim;

	unsigned int* m_FramesPerAnim;
	unsigned int m_CurNumFramesInAnim;
	unsigned int m_CurFrame;

	float m_FrameHeight;
	float m_FrameWidth;
};
