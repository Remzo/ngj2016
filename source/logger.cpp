#include "pch.h"

#include "logger.h"

//#define WIN32_LEAN_AND_MEAN		// http://stackoverflow.com/questions/11040133/what-does-defining-win32-lean-and-mean-exclude-exactly
//#include "SDL/SDL.h"
#include <iomanip>
#include <sstream>
#include <iostream>
//#include <windows.h>

Logger& Log = Logger();

HANDLE hConsole;

Logger::Logger() : m_filename( "log.txt" ) 
{
	m_file = std::ofstream( m_filename ); // Clearing the file
	m_file << "";

	hConsole = GetStdHandle( STD_OUTPUT_HANDLE );

	//Write( LogType::INFO, "Started." );
}

Logger::~Logger() {
	CloseHandle( hConsole );
}

void Logger::WriteInternal( const LogType::Type& a_type, const char a_message[512] ) {
	m_file = std::ofstream( m_filename, std::ios::app );
	std::ostream* outputStream = &m_file;

	char prefix[10];
	sprintf_s( prefix, "%7s| ", LogType::TypeStrings[a_type] );
	const std::string timeString = Logger::ComputeTimeString();
	for( unsigned int i = 0; i < 2; i++ ) {
		(*outputStream) << timeString.c_str();
		(*outputStream) << prefix << a_message;
		(*outputStream) << '\n';
		
		if( m_file.is_open() ) {
			prefix[0] = '\0';
			m_file.close();
			outputStream = &std::cout;

			switch( a_type ) {
			case LogType::INFO:
				SetConsoleTextAttribute( hConsole, FOREGROUND_BLUE | FOREGROUND_GREEN );
				//(*outputStream) << "Info   \t| ";
				break;
			case LogType::DEBUG:
				SetConsoleTextAttribute( hConsole, FOREGROUND_GREEN );
				//(*outputStream) << "Debug \t| ";
				break;

			case LogType::WARNING:
				SetConsoleTextAttribute( hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY );
				//(*outputStream) << "Warning \t| ";
				break;

			case LogType::FATAL:
				SetConsoleTextAttribute( hConsole, FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_RED );
				//(*outputStream) << "Error \t| ";
				break;
			
			}
		}
	}
}

void Logger::Write( const LogType::Type& a_type, const char* a_pMessage, ... ) {
	// Use the formatting to get the correct log
	va_list arguments;
	char message[512];

	va_start( arguments, a_pMessage );
	vsprintf_s( message, a_pMessage, arguments );
	va_end( arguments );

	WriteInternal( a_type, message );
}

void Logger::WriteInfo( const char* a_pMessage, ... ) {
	// Use the formatting to get the correct log
	va_list arguments;
	char message[512];

	va_start( arguments, a_pMessage );
	vsprintf_s( message, a_pMessage, arguments );
	va_end( arguments );

	WriteInternal( LogType::INFO, message );
}
void Logger::WriteDebug( const char* a_pMessage, ... ) {
#ifdef _DEBUG
	// Use the formatting to get the correct log
	va_list arguments;
	char message[512];

	va_start( arguments, a_pMessage );
	vsprintf_s( message, a_pMessage, arguments );
	va_end( arguments );

	WriteInternal( LogType::DEBUG, message );
#endif
}
void Logger::WriteWarning( const char* a_pMessage, ... ) {
	// Use the formatting to get the correct log
	va_list arguments;
	char message[1024];

	va_start( arguments, a_pMessage );
	vsprintf_s( message, a_pMessage, arguments );
	va_end( arguments );

	WriteInternal( LogType::WARNING, message );
}

void Logger::WriteFatal( const char* a_pMessage, ... ) {
	// Use the formatting to get the correct log
	va_list arguments;
	char message[512];

	va_start( arguments, a_pMessage );
	vsprintf_s( message, a_pMessage, arguments );
	va_end( arguments );

	WriteInternal( LogType::FATAL, message );
}

const std::string Logger::ComputeTimeString() {
	const float timeElapsed = (float)SDL_GetTicks();
	const float totalHours = ((timeElapsed/1000.0f)/60.0f)/60.0f;
	const float minutes = ((totalHours-((int)totalHours))*60.0f);
	const float seconds = ((minutes-((int)minutes))*60.0f);
	const int milliseconds = (int)timeElapsed % 1000;

	std::stringstream milliSecondsString;
	milliSecondsString << std::setfill( '0' ) << std::setw(3) << milliseconds;
	std::stringstream secondsString;
	secondsString << std::setfill( '0' ) << std::setw(2) << (int)seconds;
	std::stringstream minutesString;
	minutesString << std::setfill( '0' ) << std::setw(2) << (int)minutes;
	//std::stringstream hoursString;
	//hoursString << std::setfill( '0' ) << std::setw(2) << (int)totalHours;
	
	std::string time = "" + minutesString.str() + ":" + secondsString.str() + ":" + milliSecondsString.str() + " | ";

	return time;
}