#include "pch.h"

#include "Boat.h"
#include "Model.h"
#include "Collider.h"
#include "Line.h"
#include "Cannonball.h"

#define DEBUG_LINE_OFFSET glm::vec3(0.0f, 1.0f, 0.0f)
#define DEBUG_KEYBOARD_CONTROLS 0

Boat::Boat(unsigned int colorID, int controllerID) : c_MaxHelmAngle(360.0f)
{
	m_ControllingPlayerID = controllerID;

	//get seperate meshes for boat
	std::vector<std::pair<std::string, const Mesh*>> meshes = g_ModelLoader.GetModelAsSeperateMeshes(ModelLoader::BOAT);

	Mesh* boat = g_ModelLoader.GetSubMeshOfModel(ModelLoader::BOAT, "Boat");
	Mesh* sail = g_ModelLoader.GetSubMeshOfModel(ModelLoader::BOAT, "Flag");

	this->m_HullModel = new Model(boat);
	this->m_SailModel = new Model(sail);

	this->m_SailModel->SetColor(Globals::s_PlayerColors[colorID]);
	this->m_HullModel->SetColor(glm::vec4(glm::vec3(0.5098039216f, 0.4117647059f, 0.3254901961f)*0.5f, 1.0f));

	this->m_SailModel->SetShaderType(ShaderType::FLAT_CAMERA_LIT);
	this->m_HullModel->SetShaderType(ShaderType::FLAT_CAMERA_LIT);

	//init collider
	this->InitCollider();
	this->SetParent(this->m_Collider);

	this->InitDebugHelm();

	this->m_SailModel->SetParent(this);
	this->m_HullModel->SetParent(this);
	//this->m_Collider->SetMass(10.0f);

}

void Boat::InitCollider()
{
	//refactor this into makeCube() somewhere
	const float xScale = 1.0f;
	const float yScale = 1.0f;
	const float zScale = 1.0f;

	const float halfX = 0.5f;
	const float halfY = 0.5f;
	const float halfZ = 0.5f;

	this->m_Collider = new Collider(glm::vec3(0.85, 0.5, 1.3f), CollisionHandler::CollisionTypes::DYNAMIC);
	m_Collider->SetMass(200.0f);
}

Boat::~Boat()
{
	delete m_HullModel;
	delete m_SailModel;
}

void Boat::Render()
{
	m_HullModel->Render();
	m_SailModel->Render();


	//if debug render velocity vector and collider
#ifdef _DEBUG
	m_Collider->Render();

	const glm::vec3 lineOffset = m_Collider->GetPosition() + glm::vec3(0.0f, 1.0f, 0.0f);
	Line* velLine = new Line(lineOffset, lineOffset + this->m_Collider->GetVelocity());
	velLine->SetColor(glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
	velLine->Render();
	delete velLine;

	DrawDebugRudder();
	DrawDebugShootDir();
#endif
}

void Boat::Update(float a_DT)
{
	UpdateRudder(a_DT);
	UpdateAim(a_DT);
}

void Boat::UpdatePhysics(float a_DT)
{
	CalculateNewForces(a_DT);
	m_Collider->Tick(a_DT);
}

void Boat::UpdateAim(float a_DT)
{
	const float c_AimDelta = 1.0f;

	if (DEBUG_KEYBOARD_CONTROLS)
	{
		float aimDir = (float)m_Inputs.b_AimRight - (float)m_Inputs.b_AimLeft;
		m_ShootVal = glm::clamp(m_ShootVal + c_AimDelta*aimDir*a_DT, -1.0f, 1.0f);
	}
	else
	{
		m_ShootVal += c_AimDelta * m_Inputs.m_AimDelta* a_DT;
		m_ShootVal = glm::clamp(m_ShootVal, -1.0f, 1.0f);
	}
}

void Boat::CalculateNewForces(float a_DT)
{
	CalculateBuoyancy(a_DT);
	//drag
	const float c_HorizontalDragFactor = 5.0f;
	const float c_MaxSpeed = 5.0f;
	const float staticFrictionFactor = 2.0f;

	//drag based on boat's rotation relative to movement direction and speed
	glm::vec3 curVel = m_Collider->GetVelocity();
	glm::vec3 rightVel = m_Collider->RightVec();
	float rightFactor = glm::dot(rightVel, curVel);
	const float c_maxRightSpeed = 5.0f;

	//horizontal drag
	float horizSpeedFactor = glm::clamp(rightFactor / c_maxRightSpeed, -1.0f, 1.0f);
	this->m_Collider->AddAcceleration(rightVel*-horizSpeedFactor*c_HorizontalDragFactor * a_DT);

	//longitudinal drag
	float speedFactor = glm::clamp(glm::dot(m_Collider->GetVelocity(), m_Collider->ForwardVec()) / c_MaxSpeed, -1.0f, 1.0f);
	this->m_Collider->AddAcceleration(-this->ForwardVec()*a_DT*speedFactor);

	//forward	
	if (m_Inputs.b_Forward)
	{
		m_Collider->AddAcceleration(this->ForwardVec()*a_DT*m_ThrustValue);
		//when at max forward velocity, force of longitudinal drag and force of "forwardness" should cancel
	}

	//rudder torque
	float const maxRudderTorque = 70.0f;

	if (this->m_Collider->GetVelocity() != glm::vec3(0))
	{
		glm::vec3 rudderVec = this->GetAbsoluteVectorFromVal(m_RudderVal, c_RudderMaxAngle, 180.0f);
		float dotResultRudderVel = glm::dot(glm::normalize(rudderVec), this->m_Collider->RightVec());

		glm::vec3 perpendicularToVelocity = glm::normalize(glm::cross(this->m_Collider->GetVelocity(), this->m_Collider->UpVec()));
		m_Collider->AddTorque(-dotResultRudderVel*glm::vec3(0.0f, maxRudderTorque, 0.0f)*speedFactor*a_DT);
	}

	//gravity
	m_Collider->AddAcceleration(glm::vec3(0.0f, -9.81f, 0.0f)*a_DT);

	//damping
	if (b_Submerged)
	{
		const float c = 5.0f; //damping coefficient;
		float dampingForce = -c*this->m_Collider->GetVelocity().y;
		this->m_Collider->AddForce(dampingForce*glm::vec3(0.0f, 1.0f, 0.0f));
	}

	//resistive torque
	const float resistiveTorqueFactor = 0.5f;
	m_Collider->AddTorque(-m_Collider->GetAngularMomentum() * resistiveTorqueFactor * a_DT);

	//self-righting force
	const float selfRightingFactor = 80.0f;
	glm::vec3 crossResult = glm::cross(m_Collider->UpVec(), glm::vec3(0.0f,1.0f,0.0f));
	//Log.WriteDebug("CrossResult: %f, %f, %f", crossResult.x, crossResult.y, crossResult.z);
	//pitch
	m_Collider->AddTorque(glm::vec3(crossResult.x, 0.0f, crossResult.z) *a_DT*selfRightingFactor);
}

void Boat::CalculateBuoyancy(float a_DT)
{
	const glm::vec3 buoyancyOffset = glm::vec3(0.0f, -0.5f, 0.0f);
	std::vector<glm::vec3> m_Points = this->m_Collider->GetCollisionPoints(); //world space

	//loop points, apply hooke's law to points below water level
	const float k = 100.0f;
	b_Submerged = false;

	glm::vec3 point = m_Collider->GetPosition() + buoyancyOffset;

	if (point.y < 0.0f)
	{
		b_Submerged = true;
		float x = 0.0f - point.y;
		float forceMag = k*x;
		this->m_Collider->AddForce(glm::vec3(0.0f, 1.0f, 0.0f)*forceMag);
	}

	//for (unsigned int i = 0; i < m_Points.size(); i++)
	//{
	//	if (m_Points[i].y < 0.0f) //replace 0.0f with water height at point
	//	{
	//		float x = 0.0f - m_Points[i].y;
	//		float forceMagnitude = k*x;

	//		this->m_Collider->AddForce(glm::vec3(0.0f, 1.0f, 0.0f)*forceMagnitude/3.0f, m_Points[i]); //replace up vector with water normal at point?
	//	}
	//}
}

void Boat::HandleInput(SDL_Scancode forward, SDL_Scancode right, SDL_Scancode left, SDL_Scancode shoot)
{
	this->m_Inputs.b_Forward = Input.GetKey(forward);
	this->m_ThrustValue = 1.0f*m_Inputs.b_Forward;
	this->m_Inputs.b_Left = Input.GetKey(left);
	this->m_Inputs.b_Right = Input.GetKey(right);
	this->m_Inputs.b_Shoot = Input.GetKey(shoot);

	//controller
	if (!DEBUG_KEYBOARD_CONTROLS)
	{
		this->m_Inputs.b_Forward = Input.GetButton(m_ControllingPlayerID, InputManager::GamepadButtons::BUTTON_A);
		this->m_ThrustValue = Input.GetTriggerValue(m_ControllingPlayerID, true);
		this->m_Inputs.b_Forward = (m_ThrustValue != 0.0f);

		this->m_Inputs.m_HelmDelta = Input.GetStickClockwiseDelta(m_ControllingPlayerID, false);

		this->m_Inputs.b_Shoot = Input.GetButtonDown(m_ControllingPlayerID, InputManager::GamepadButtons::RIGHT_THUMB);
		//this->m_Inputs.b_AimLeft = Input.GetButton(m_ControllingPlayerID, InputManager::GamepadButtons::LEFT_SHOULDER);
		//this->m_Inputs.b_AimRight = Input.GetButton(m_ControllingPlayerID, InputManager::GamepadButtons::RIGHT_SHOULDER);

		this->m_Inputs.m_AimDelta = Input.GetStickClockwiseDelta(m_ControllingPlayerID, true);

		
	}

	//debug stop movement
	if (Input.GetKeyDown(SDL_SCANCODE_Z))
	{
		glm::vec3 forceToStopBoat = this->m_Collider->GetMass()*-m_Collider->GetVelocity();
		this->m_Collider->AddForce(forceToStopBoat);
	}

	if (Input.GetKeyDown(SDL_SCANCODE_U))
	{
		m_Collider->AddTorque(20.0f * m_Collider->RightVec());
	}
	if (Input.GetKeyDown(SDL_SCANCODE_O))
	{
		m_Collider->AddTorque(20.0f * m_Collider->ForwardVec());
	}

}

void Boat::UpdateRudder(float a_DT)
{
	const float c_RudderDelta = 1.85f;
	const float c_RudderReturnRate = 0.6f;

	bool rudderFree = true;

	if (DEBUG_KEYBOARD_CONTROLS)
	{
		if (m_Inputs.b_Left)
		{
			this->m_RudderVal += c_RudderDelta*a_DT;
			rudderFree = false;
		}

		if (m_Inputs.b_Right)
		{
			this->m_RudderVal -= c_RudderDelta*a_DT;
			rudderFree = false;
		}
	}
	else
	{
		//controller
		rudderFree = Input.IsStickNeutral(m_ControllingPlayerID, false);
		this->m_RudderVal -= c_RudderDelta * m_Inputs.m_HelmDelta* a_DT;
	}

	if (rudderFree)
	{
		const float threshold = 0.01f;
		if (abs(this->m_RudderVal) > threshold)
		{
			//rudder tends to 0.0f when not actively moved
			float dir = (this->m_RudderVal < 0.0f) ? 1.0f : -1.0f;
			this->m_RudderVal += dir*c_RudderDelta*c_RudderReturnRate*a_DT;
		}
	}

	this->m_RudderVal = glm::clamp(this->m_RudderVal, -1.0f, 1.0f);
}

glm::vec3 Boat::GetAbsoluteVectorFromVal(float normalisedValue, float maxAngleDeviationDeg, float angleOffsetDeg) const
{
	float angle = maxAngleDeviationDeg*normalisedValue;
	angle += angleOffsetDeg;

	if (angle >= 360.0f)
	{
		angle -= 360.0f;
	}

	if (angle < 0.0f)
	{
		angle += 360.0f;
	}

	angle = glm::radians(angle);

	glm::vec4 outVec = glm::vec4(glm::sin(angle), 0.0f, -cos(angle), 0.0f);
	outVec = m_Collider->GetRotation(false) * outVec;

	return glm::vec3(outVec);
}

void Boat::InitDebugHelm()
{
	const unsigned int numHelmPoints = 8;
	float deltaAngleRad = (360.0f) / numHelmPoints;

	float angle = 0.0f;

	for (int i = 0; i < numHelmPoints; i++)
	{
		m_DHelmAngles.push_back(angle);
		angle += deltaAngleRad;
	}
}

void Boat::DrawDebugRudder()
{
	const glm::vec3 lineOffset = m_Collider->GetPosition() + glm::vec3(0.0f, 1.0f, 0.0f);

	//rudderline
	Line* rudderLine = new Line(lineOffset, lineOffset+ this->GetAbsoluteVectorFromVal(m_RudderVal, c_RudderMaxAngle, 180.0f));
	rudderLine->SetColor(glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));
	rudderLine->Render();

	delete rudderLine;

	//helm
	const float helmOuterRadius = 1.1f;
	const float helmInnerRadius = 1.0f;
	const float addRadius = 0.5f;
	Line* helmLine;
	for (unsigned int i = 0; i < m_DHelmAngles.size(); i++)
	{
		//center to point
		glm::vec3 center = this->m_Collider->GetPosition() + DEBUG_LINE_OFFSET;
		glm::vec3 point = center + (this->GetAbsoluteVectorFromVal(m_DHelmAngles[i]/c_MaxHelmAngle, c_MaxHelmAngle, -m_RudderVal*c_MaxHelmAngle));

		//rotational offset vector
		glm::vec3 centerToPoint = glm::normalize(point - center);

		helmLine = new Line(center+centerToPoint*helmInnerRadius, point+centerToPoint*(helmOuterRadius+((i%2==0)?addRadius:0.0f))); // draw line from innerRadius to outerRadius with additional length if index is even
		helmLine->SetColor((i == 0) ? glm::vec4( 0xff/0xff,  0xff/0xff, 0.0f, 1.0f) : glm::vec4(1.0)); // yellow if top vertical line
		helmLine->Render();

		delete helmLine;
	}


}

void Boat::DrawDebugShootDir()
{
	const glm::vec3 lineOffset = m_Collider->GetPosition() + m_Collider->ForwardVec() + DEBUG_LINE_OFFSET;

	Line* shootAimLine = new Line(lineOffset, lineOffset + this->GetAbsoluteVectorFromVal(m_ShootVal, c_ShootMaxAngle));
	shootAimLine->SetColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	shootAimLine->Render();

	delete shootAimLine;
}

