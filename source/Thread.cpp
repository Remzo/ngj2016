#include "pch.h"

#include "Thread.h"

Thread::Thread()
{
	this->shutdown = false;
	m_Handle = CreateThread(NULL, 0, ThreadProc, (LPVOID) this, 0, NULL);
}

Thread::~Thread()
{
	//WaitForFinish();
	//WaitForSingleObject(m_Handle, 200);
	TerminateThread(m_Handle, 0);
	//CloseHandle(m_Handle);
}

void Thread::Kill()
{
	this->shutdown = true;
}

void Thread::BackgroundTask()
{
	while (!shutdown)
	{
		this->ThreadFunction();
	}

	delete this;
}

DWORD WINAPI Thread::ThreadProc(__in LPVOID lpParam)
{
	Thread* threadInstance = (Thread*)lpParam;
	threadInstance->BackgroundTask();
	return 0;
}

HANDLE Thread::GetHandle()
{
	return this->m_Handle;
}

void Thread::WaitForFinish()
{
	WaitForSingleObject(m_Handle, INFINITE);
}