#pragma once

#include <Xinput.h>

class XInputController
{
public:
	XInputController(int playerID);
	~XInputController();

	XINPUT_STATE GetState();
	bool IsConnected();
	void Vibrate(int leftVal = 0, int rightVal = 0);
private:
	XINPUT_STATE m_ControllerState;
	int m_ControllerID;
};
