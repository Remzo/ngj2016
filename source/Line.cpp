#include "pch.h"

#include "Line.h"
#include "Mesh.h"

Line::Line(VertexComplete p1, VertexComplete p2)
{
	SetGLRenderMode(GL_LINES);

	m_Vertices.push_back(p1);
	m_Vertices.push_back(p2);

	m_Indices.push_back(0);
	m_Indices.push_back(1);

	SetShaderType(ShaderType::FLAT_UNLIT);
	SetMesh(new Mesh(m_Vertices, m_Indices));
}

Line::Line(glm::vec3 p1, glm::vec3 p2, glm::vec3 normal)
{
	SetGLRenderMode(GL_LINES);

	m_Vertices.push_back(VertexComplete(p1, normal, glm::vec2(0.0f), glm::vec4(1.0f)));
	m_Vertices.push_back(VertexComplete(p2, normal, glm::vec2(0.0f), glm::vec4(1.0f)));

	m_Indices.push_back(0);
	m_Indices.push_back(1);

	SetShaderType(ShaderType::FLAT_UNLIT);
	SetMesh(new Mesh(m_Vertices, m_Indices));
}

Line::~Line()
{
}

void Line::Init()
{

}

void Line::SetLineWidth(float width)
{
	this->m_Thickness = width;
}
