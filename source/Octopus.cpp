#include "pch.h"

#include "OctopusModel.h"
#include "Octopus.h"
#include "Ship.h"
#include "Tentacle.h"
#include "SoundSystem.h"

#define POINTTHRESHOLDMIN 5
extern SoundSystem & f;
Octopus::Octopus()
{
	m_Model = new OctopusModel();
	m_Model->SetScale(1.0f);
	m_Model->SetPosition({ 0.0f, -1.0f, 0.0f });

	tipL = 0;
	tipR = 1;
	m_InputDirL = m_InputDirR = glm::vec3(0);
	tips[tipL].SetColor(glm::vec4(0.6f, 0, 0, 1));
  tips[tipL].tentacle->tipState = Tentacle::TipState::LEFT;
  tips[tipR].SetColor(glm::vec4(0.8f, 0, 0.8f, 1));
  tips[tipR].tentacle->tipState = Tentacle::TipState::RIGHT;

	m_EyeModel = new Model(g_ModelLoader.GetSubMeshOfModel(ModelLoader::EYE, "Cylinder"));
	m_EyeModel->SetColor(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	m_EyeModelOffset = glm::vec3(0.0f, 20.0f, 20.0f);
	m_EyeModel->SetPosition(m_EyeModelOffset);
	m_EyeModel->RotateEuler(45.0f, glm::vec3(1.0f, 0.0f, 0.0f));

}

void Octopus::SetPlayer(Player* player) {
  this->player = player;
  for (auto& t : tips) {
    t.SetPlayer(player);
  }
}

Octopus::~Octopus()
{
	if (m_Model) delete m_Model; m_Model = 0;
	delete m_EyeModel;
}

float Octopus::GetDistance(glm::vec3 pos, TentacleTip** closestTip) {
  float minDist = 10000;
  float tempMin;
  for (int i = 0; i < 8; i++) {
	  float dist = tips[i].GetDistance(pos);

	  if (dist < minDist)
	  {
		  if (closestTip != NULL)
		  {
			  *closestTip = &tips[i];
		  }
	  }
	  minDist = glm::min(dist, minDist);

  }
  return minDist;
}

glm::vec3 Octopus::GetDir(glm::vec3 pos) {
  float dist = GetDistance(pos);
  float distDx = GetDistance(pos + glm::vec3(0.01, 0, 0)) - dist;
  float distDy = GetDistance(pos + glm::vec3(0, 0.01, 0)) - dist;
  float distDz = GetDistance(pos + glm::vec3(0, 0, 0.01)) - dist;

  return glm::normalize(glm::vec3(distDx, distDy, distDz));
}

void Octopus::SetControllerID(int controllerID)
{
	m_ControllerID = controllerID;

	b_ValidControllerConnected = Input.ConnectedControllers()[m_ControllerID];
	if (b_ValidControllerConnected)
	{
		Log.WriteDebug("ValidController for player octopus: %i", m_ControllerID);
	}
	else
	{
		Log.WriteDebug("InvalidController for player octopus: %i", m_ControllerID);
	}
}

void Octopus::Tick(float a_DT)
{

	HandleInput();
	//reduce vel
	tips[tipL].UpdateVelocity(m_InputDirL, a_DT);
	tips[tipR].UpdateVelocity(m_InputDirR, a_DT);
	CheckHole(tipL);
	CheckHole(tipR);
	

}
void Octopus::CheckHole(int _tipIndex)
{
	int hi = tips[_tipIndex].GetHoleIndex();
	if (hi >= 0) //if tip covers a hole
	{
		glm::vec3 p = tips[_tipIndex].GetPosition();
		float dist = glm::length(shipref->holes[hi]->GetPosition() - p);
		if (dist > POINTTHRESHOLDMIN)
		{
			shipref->holes[hi]->isOpen = true;
			tips[_tipIndex].FreeHole();
			Log.WriteDebug("freed hole %i from tip %i\n", hi, _tipIndex);
		}
	}
}

void Octopus::PhysicsTick(float a_DT) 
{
  for (int i = 0; i < 8; i++) {
    tips[i].PhysicsTick(a_DT);
  }
}
void Octopus::SetShipRef(Ship* _s)
{
	shipref = _s;
}

void Octopus::UpdateEyePosition(glm::vec3 playerPos)
{
	glm::vec3 newPos = m_EyeModelOffset + glm::normalize(playerPos - m_Model->GetPosition()) * 2.0f;
	this->m_EyeModel->SetPosition(newPos);
}

void Octopus::HandleInput()
{
	m_InputDirL = m_InputDirR = glm::vec3(0.0f);

	if (Input.GetKeyDown(SDL_SCANCODE_D))
	{
		tips[tipL].tentacle->ResetPoints();
	}

	if (!b_ValidControllerConnected) //keyboard fallback
	{
		//Movement
		//keyboard
		if (Input.GetKey(SDL_SCANCODE_J))
		{
			m_InputDirL += glm::vec3(-1.0f, 0.0f, 0.0f);
		}

		if (Input.GetKey(SDL_SCANCODE_L))
		{
			m_InputDirL += glm::vec3(1.0f, 0.0f, 0.0f);
		}

    if (Input.GetKey(SDL_SCANCODE_I)) {
      m_InputDirL += glm::vec3(0.0f, 0.0f, -1.0f);
    }

		if (Input.GetKey(SDL_SCANCODE_K))
		{
			m_InputDirL += glm::vec3(0.0f, 0.0f, 1.0f);
		}

    if (glm::length(m_InputDirL) != 0) {
      m_InputDirL = glm::normalize(m_InputDirL) * 5.0f;
    }

		//Aiming
	}else
	{
		//Select Tentacles

    int prevTipL = tipL;
    int prevTipR = tipR;

		bool trl = Input.GetButtonDown(m_ControllerID, InputManager::LEFT_SHOULDER);
		if (trl)
		{
			if ((tipL + 1) % 8 != tipR)
			{
				tips[tipL].SetColor(glm::vec4(1, 0, 0, 1));
				tipL = (tipL + 1) % 8;
				tips[tipL].SetColor(glm::vec4(0.6f, 0, 0, 1));
			}
			else
			{
				tips[tipL].SetColor(glm::vec4(1, 0, 0, 1));
				tipL = (tipL + 2) % 8;
				tips[tipL].SetColor(glm::vec4(0.6f, 0, 0, 1));
			}

		}
		else
		{
			bool trr = Input.GetButtonDown(m_ControllerID, InputManager::RIGHT_SHOULDER);
			if (trr)
			{
				if ((tipR + 1) % 8 != tipL)
				{
					tips[tipR].SetColor(glm::vec4(1, 0, 0, 1));
					tipR = (tipR + 1) % 8;
					tips[tipR].SetColor(glm::vec4(0.8f, 0, 0.8f, 1));
				}
				else
				{
					tips[tipR].SetColor(glm::vec4(1, 0, 0, 1));
					tipR = (tipR + 2) % 8;
					tips[tipR].SetColor(glm::vec4(0.8f, 0, 0.8f, 1));
				}

			}


		}
		
    if (tipL != prevTipL) {
      tips[prevTipL].Freeze();
      tips[prevTipL].tentacle->tipState = Tentacle::TipState::DEFAULT;
      tips[tipL].tentacle->tipState = Tentacle::TipState::LEFT;
    }

    if (tipR != prevTipR) {
      tips[prevTipR].Freeze();
      tips[prevTipR].tentacle->tipState = Tentacle::TipState::DEFAULT;
      tips[tipR].tentacle->tipState = Tentacle::TipState::RIGHT;
    }

		//Movement
		glm::vec2 leftStick = Input.GetStickState(m_ControllerID, false);
		glm::vec2 rightStick = Input.GetStickState(m_ControllerID, true);

		m_InputDirL = glm::vec3(leftStick.x, 0, -leftStick.y);
		m_InputDirR = glm::vec3(rightStick.x, 0, -rightStick.y);

		//poking

		CheckPoke(InputManager::LEFT_THUMB, tipL);
		CheckPoke(InputManager::RIGHT_THUMB, tipR);
		
		//stab
		float lTriggerValue = Input.GetTriggerValue(m_ControllerID);
		if (!b_LeftStabPressed && lTriggerValue >= 0.9f)
		{
			//left
			tips[tipL].Stab();
			b_LeftStabPressed = true;
		}
		else if (lTriggerValue <= 0.1f)
		{
			b_LeftStabPressed = false;
		}

		float rTriggerValue = Input.GetTriggerValue(m_ControllerID, true);
		if (!b_RightStabPressed && rTriggerValue >= 0.9f)
		{
			//right
			tips[tipR].Stab();
			b_RightStabPressed = true;
		}
		else if (rTriggerValue <= 0.1f)
		{
			b_RightStabPressed = false;
		}
	}
}

void::Octopus::CheckPoke(InputManager::GamepadButtons _but, int _tipidx)
{
	bool poke = Input.GetButtonDown(m_ControllerID, _but);
	if (poke)
	{
		float mymin = 2 * POINTTHRESHOLDMIN;
		int chosenhole = -1;
		for (int i = 0; i < shipref->holes.size(); i++)
		{
			glm::vec3 p = tips[_tipidx].GetPosition();
			float dist = glm::length(shipref->holes[i]->GetPosition() - p);
			if (dist < POINTTHRESHOLDMIN  && mymin> dist && shipref->holes[i]->isOpen)
			{
				mymin = dist;
				chosenhole = i;
			}
		}
		if (chosenhole >= 0)
		{
			shipref->holes[chosenhole]->isOpen = false;
			Log.WriteDebug("plugged hole %i  with tip \n", chosenhole, _tipidx);
			g_SoundSystem.PlaySound(g_SoundSystem.soundOctoCoverHole);
			tips[_tipidx].SetHole(chosenhole);
		}

	}
}
void Octopus::Render()
{
	m_Model->Render();
	m_EyeModel->Render();
	for (int i = 0; i < 8; i++)
		tips[i].Render();
}