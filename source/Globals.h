#pragma once

class Globals
{
public:
	Globals(){};
	~Globals(){};

	static glm::vec4 s_PlayerColors[8];

	unsigned int ScreenHeight;
	unsigned int ScreenWidth;
	float m_HalfGameHeight;
	float m_HalfGameWidth;

};

extern Globals& GlobalInfo;