#pragma once

#include "Model.h"

class GameObject : public virtual Model
{
public:
	GameObject();
	virtual ~GameObject();

	virtual void Tick(float a_DT);
	virtual void SetVelocity(glm::vec3 vel) { m_Velocity = vel; }
	glm::vec3 GetVelocity() { return m_Velocity; }

private:


protected:
	glm::vec3 m_Velocity;
	glm::vec3 m_Acceleration;

};
