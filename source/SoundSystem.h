#pragma once

#include "FMOD/Windows/inc/fmod.hpp"
class SoundObject
{
	FMOD::Sound      *sound1, *sound2;

};
//enum ---path ---index play sound by index
class SoundSystem
{
public:
	SoundSystem();
	FMOD::System     *system;
	FMOD::Sound      *soundbg, *soundbgIntro, *soundOctoAttack,*soundOctoCoverHole, *soundVikingMakeHole, *soundVikingCharge, * soundvikingAttack;
	FMOD::Channel    *channelbg, *channeleffects, *channelGameover;
	FMOD_RESULT       result;
	int maxchannels;
	void Init(); //set listener po
	void Terminate();
	//void LoadSound(const char* _path);//add sound(path name)
	void LoadSounds();
	void Update();
	void PlayBgSound();
	void StopBgSound();
	bool IsPlaying(FMOD::Channel* c);
	bool triggeredMainBgLoop, startedbg;
	void SoundSystem::PlaySound(FMOD::Sound* s);


	//unload sounds
	//playsound(name)
	//channel bg
	//channel effects

};

extern SoundSystem& g_SoundSystem;