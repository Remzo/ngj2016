#include "pch.h"

#include "window.h"

Window::Window( const unsigned int& a_windowWidth, const unsigned int& a_windowHeight ) : 
	m_windowWidth( a_windowWidth ), 
	m_windowHeight( a_windowHeight ) {

}

Window::~Window() {
	SDL_DestroyWindow( m_pSDLwindow );
	SDL_GL_DeleteContext( m_glContext );
}