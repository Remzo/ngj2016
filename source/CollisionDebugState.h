#pragma once

#include "State.h"

class StateManager;
class Collider;
class CollisionDebugState : public State
{
public:
	CollisionDebugState(float halfHeight, float halfWidth, StateManager* stateManager);
	~CollisionDebugState();

	void Tick(float a_DT);
	void Render();

private:
	void HandleInput(float a_DT);

	Collider* m_Collider1;
	Collider* m_Collider2;
};
