#pragma once

#include "TentacleTip.h"
class Ship;
class OctopusModel;
class Player;


class Octopus {
  //move tentacles slower when further from octopus head
public:
  Octopus();
  void SetPlayer(Player * player);
  ~Octopus();

  Player* player;

  float GetDistance(glm::vec3 pos, TentacleTip** outTip = NULL);

  glm::vec3 GetDir(glm::vec3 pos);

  void Tick(float a_DT);
  void PhysicsTick(float a_DT);
  void HandleInput();
  void Render();

  void SetControllerID(int controllerID);
  void SetShipRef(Ship* _s);
  void CheckHole(int _tipIndex);
  void CheckPoke(InputManager::GamepadButtons _but, int _tipidx);

  OctopusModel* m_Model;
  TentacleTip tips[8] = {
    { 0 },
    { 1 },
    { 2 },
    { 3 },
    { 4 },
    { 5 },
    { 6 },
    { 7 },
  };

	int  tipR, tipL; //index id left; index id right;
	glm::vec3 m_InputDirL,  m_InputDirR;
	const float c_Acceleration = 320.0f;
	const float c_Friction = 20.0f;

	void UpdateEyePosition(glm::vec3 playerPos);
private:
	bool b_LeftStabPressed = false;
	bool b_RightStabPressed = false;
	
	bool b_ValidControllerConnected = false;
	int m_ControllerID = -1;
	Ship* shipref;
	Model* m_EyeModel;

	glm::vec3 m_EyeModelOffset = glm::vec3(0.0f);
};
