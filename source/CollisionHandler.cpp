#include "pch.h"

#include "Collider.h"
#include "CollisionHandler.h"

CollisionHandler& g_CollisionManager = CollisionHandler();

CollisionHandler::CollisionHandler()
{
}

CollisionHandler::~CollisionHandler()
{
	this->ClearRegisteredColliders();
}

void CollisionHandler::RegisterCollider(Collider* col)
{
	switch (col->GetColliderType())
	{
	case STATIC:
		this->m_StaticColliders.push_back(col);
		break;
	case DYNAMIC:
		this->m_DynamicColliders.push_back(col);
		break;
	default:
		assert(0);
		break;
	}
}

void CollisionHandler::ClearRegisteredColliders()
{
	m_DynamicColliders.clear();
	m_StaticColliders.clear();
}

void CollisionHandler::TestCollisions()
{
	//check all registered dynamic colliders against the static colliders and every other dynamic collider, optimise with spatial partitioning
	CollisionInfo colInfo;
	std::pair<std::pair<Collider*, Collider*>, CollisionInfo> collisionToResolve;

	//static-dynamic
	for (unsigned int i = 0; i < m_StaticColliders.size(); i++)
	{
		for (unsigned int j = 0; j < m_DynamicColliders.size(); j++)
		{
			bool collisionResult = this->TestCollision(m_DynamicColliders[j], m_StaticColliders[i], colInfo);
			if (collisionResult)
			{
				collisionToResolve.first = std::pair<Collider*, Collider*>(m_DynamicColliders[j], m_StaticColliders[i]);
				collisionToResolve.second = colInfo;

				m_CollisionsToResolve.push_back(collisionToResolve);
			}
		}
	}

	//dynamic-dynamic
	for (unsigned int i = 0; i < m_DynamicColliders.size(); i++)
	{
		m_DynamicColliders[i]->SetColor(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
		for (unsigned int j = i+1; j < m_DynamicColliders.size(); j++)
		{
			unsigned int index = j;

			//test collision
			bool collisionResult = this->TestCollision(m_DynamicColliders[index], m_DynamicColliders[i], colInfo);
			if (collisionResult)
			{
				collisionToResolve.first = std::pair<Collider*, Collider*>(m_DynamicColliders[index], m_DynamicColliders[i]);
				collisionToResolve.second = colInfo;

				m_CollisionsToResolve.push_back(collisionToResolve);
			}

		}
	}
}

void CollisionHandler::ResolveCollisions()
{
	//resolve collisions then clear resolve lists
	for (auto i : m_CollisionsToResolve)
	{
		i.first.first->SetColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
		i.first.second->SetColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	}

	m_CollisionsToResolve.clear();
}

//returns false if no collision
bool CollisionHandler::TestCollision(Collider* obj1, Collider* obj2, CollisionInfo& colInfo)
{
	bool result = this->SatTest(obj1, obj2, colInfo);
	return result;
}

//returns true if separating
bool CollisionHandler::TestAxis(Collider* obj1, Collider* obj2, glm::vec3 axisNormal, float& penetration)
{
	float obj1Min, obj1Max, obj2Min, obj2Max;
	obj1Min = obj2Min = FLT_MAX;
	obj1Max = obj2Max = FLT_MIN;

	float objectSeparation = glm::abs( glm::dot((obj2->GetPosition() - obj1->GetPosition()), axisNormal));

	this->GetMinMaxProjection(obj1, &obj1Min, &obj1Max, axisNormal);
	this->GetMinMaxProjection(obj2, &obj2Min, &obj2Max, axisNormal);

	if (obj2Min > obj1Max || obj2Max < obj1Min)
	{
		//separating axis
		return true;
	}

	penetration = glm::min((obj1Max - obj2Min), (obj2Max - obj1Min));
	penetration = glm::min(obj2Max,obj1Max)- glm::max(obj1Min,obj2Min);
	return false;
}

float CollisionHandler::CalcMinLengthInterval(Collider* col, glm::vec3 axis) const
{
	//http://www.geometrictools.com/Documentation/DynamicCollisionDetection.pdf
	//assumptions: axis in col space, extents in same order as axis

	float outVal = 0.0f;
	glm::vec3 halfExtents = col->c_Dimensions / 2.0f;

	//calc for the three pinciple axes
	//right
	outVal += abs(halfExtents.x * glm::dot(axis, col->RightVec()));
	//up
	outVal += abs(halfExtents.y * glm::dot(axis, col->UpVec()));
	//forward
	outVal += abs(halfExtents.z * glm::dot(axis, col->ForwardVec()));

	return abs(outVal);
}

void CollisionHandler::Tick(float a_DT)
{
#ifdef PROFILE_COLLISION
	LARGE_INTEGER startTime;
	QueryPerformanceCounter(&startTime);
#endif

	TestCollisions();

#ifdef PROFILe_COLLISION
	LARGE_INTEGER endTime;
	QueryPerformanceCounter(&endTime);

	__int64 elapsedTime = endTime.QuadPart - startTime.QuadPart;
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);

	float freqInS = float(freq.QuadPart) / 1000.0f;
	float timeInS = elapsedTime / freqInS;


	Log.WriteInfo("Collision Check Duraion: %.2f", timeInS);
#endif


	ResolveCollisions();
}

void CollisionHandler::GetMinMaxProjection(Collider* col, float* min, float* max, glm::vec3 projAxis)
{
	float objMin = FLT_MAX;
	float objMax = -FLT_MAX;

	std::vector<glm::vec3> colliderPoints = col->GetCollisionPoints();

	for (unsigned int i = 0; i < colliderPoints.size(); i++)
	{
		//glm::vec3 vertWorldPos = (col->GetModelMatrix() * glm::vec4(objVerts[i].m_Pos, 1)).xyz; // *objectScale

		glm::vec3 vertWorldPos = colliderPoints[i];
		//project onto axis
		float dotResult = glm::dot(vertWorldPos, projAxis);

		if (dotResult > objMax)
		{
			objMax = dotResult;
		}

		if (dotResult < objMin)
		{
			objMin = dotResult;
		}
	}

	*min = objMin;
	*max = objMax;
}

bool CollisionHandler::CheckOverlapOnColliderFaceAxes(Collider* colFaces, Collider* col2) const
{
	std::vector<glm::vec3> colFaceNormals = colFaces->GetFaceNormals();
	glm::vec3 testAxis;

	for (unsigned int i = 0; i < colFaceNormals.size(); i++)
	{
		testAxis = colFaces->GetRotation(false) * colFaceNormals[i];

		float minLengthInterval1 = CalcMinLengthInterval(col2, testAxis);
		float minLengthInterval2 = CalcMinLengthInterval(colFaces, testAxis);

		glm::vec3 d = (col2->GetPosition() - colFaces->GetPosition());
		float separation = abs(glm::dot(d, testAxis));

		if (separation > minLengthInterval1 + minLengthInterval2)
		{
			return false; // no overlap
		}
	}
	
	return true; //all overlap
}

bool CollisionHandler::SatTest(Collider* obj1, Collider* obj2, CollisionInfo& colInfo)
{

	std::vector<glm::vec3> obj2FaceNormals = obj2->GetFaceNormals();
	bool axisResult = false;
	float penetration = -FLT_MAX;
	glm::vec3 testAxis = glm::vec3(0, 0, 0);
	glm::vec3 penetrationNormal = glm::vec3(0, 0, 0);
	float testPenetration = 0.0f;

	//REFACTOR
	//test obj1 faces
	if (CheckOverlapOnColliderFaceAxes(obj1, obj2) == false)
	{
		return false; //overlap found
	}

	//obj2 faces
	if (CheckOverlapOnColliderFaceAxes(obj2, obj1) == false)
	{
		return false; //overlap found
	}

	//oh my lord we also need to test all edge pairs' cross products?! THAT'S O(n^2)! optimise with minkowski difference and gauss map later as per gregorious dirk's presentation
	//TODO: edge-edge-collision

	colInfo.collisionNormal = penetrationNormal;
	colInfo.penetration = penetration;
	return true;

}