#include "pch.h"

#include "PhysicsObject.h"

//resource: http://gafferongames.com/game-physics/physics-in-3d/

PhysicsObject::PhysicsObject()
{

}

PhysicsObject::~PhysicsObject()
{

}

void PhysicsObject::Tick(float a_DT)
{
	ApplyForces(a_DT);
	ApplyTorque(a_DT);

	this->SetPosition(this->GetPosition() + this->m_Velocity*a_DT);

	
	glm::quat newRotation = this->GetRotation() + m_Spin * a_DT;
	newRotation = glm::normalize(newRotation); // needs to be normalised to stop drifting errors
	this->SetRotation(newRotation);
}

void PhysicsObject::AddForce(glm::vec3 force, glm::vec3 position)
{
	m_ForcesToApply.push_back(Force(force, position));
	
	this->AddTorque(glm::cross(force, (-position)));
}

void PhysicsObject::AddTorque(glm::vec3 torque)
{
	m_TorqueToApply.push_back(torque);
}

void PhysicsObject::RecalculateState()
{
	this->m_Velocity = m_Momentum / m_Mass;
}

glm::vec3 PhysicsObject::GetAngularMomentum() const
{
	return m_AngularMomentum;
}

void PhysicsObject::SetVelocity(glm::vec3 vel)
{
	m_Velocity = vel;

	//recalc momentum
	m_Momentum = m_Velocity / m_Mass;

}

glm::mat3 PhysicsObject::GetInertialTensor() const
{
	//temp inertial tensor of cube with side length 1, replace with m_InertialTensor
	float tensor = 1.0f / 6.0f * powf(1.0f, 2.0f)*  m_Mass;
	glm::mat3 outTensor = glm::mat3(1)*tensor;

	return outTensor;

}

void PhysicsObject::RecalculateRotState()
{
	//calc angular velocity

	//temp inertial tensor of cube with side length 1, replace with m_InertialTensor
	//float inTensor = 1.0f / 6.0f * powf(1.0f, 2.0f)*  m_Mass;

	m_AngularVelocity = m_AngularMomentum * glm::inverse(this->GetInertialTensor());
	
	glm::quat q(0.0f, m_AngularVelocity.x, m_AngularVelocity.y, m_AngularVelocity.z);

	m_Spin = 0.5f * q * this->GetRotation();
}

void PhysicsObject::ApplyForces(float a_DT)
{
	for (auto i : m_ForcesToApply)
	{
		//update momentum for every force
		this->m_Momentum += i.force;
	}

	m_ForcesToApply.clear();

	this->RecalculateState();
}

void PhysicsObject::ApplyTorque(float a_DT)
{
	for (auto i : m_TorqueToApply)
	{
		this->m_AngularMomentum += i;
	}

	m_TorqueToApply.clear();

	this->RecalculateRotState();
}

glm::vec3 PhysicsObject::AddAcceleration(glm::vec3 accel)
{
	glm::vec3 force = this->GetMass() * accel;

	this->AddForce(force);
	return force;
}