#include "pch.h"

#include "BucketPrototypeState.h"
#include <algorithm>
#include <random>
#include <ctime>
#include "StateManager.h"
#include "Model.h"
#include "Quad.h"
#include "skybox.h"
#include "Player.h"
#include "texture.h"
#include "ParticleSystem.h"
#include "Octopus.h"
#include "Ship.h"
#include "SoundSystem.h"

BucketPrototypeState::BucketPrototypeState(float halfHeight, float halfWidth, StateManager* stateManager) : State(stateManager)
{
	//init MainCamera
	MainCamera.Reset();
	//MainCamera.SetOrthographic(-GlobalInfo.m_HalfGameWidth, GlobalInfo.m_HalfGameWidth, -GlobalInfo.m_HalfGameHeight, GlobalInfo.m_HalfGameHeight, 0.01f, 1000.0f);
	MainCamera.SetPosition(glm::vec3(1.17f, 82.85f, 113.0f));
	glm::quat quaternion;
	quaternion.x = -0.4058f;
	quaternion.y = quaternion.z = 0.0f;
	quaternion.w = 0.914f;
	MainCamera.SetRotation(quaternion);

	//init player sphere
	m_Player = new Player(1.0f);
	m_Player->SetColor(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
	m_Player->SetScale(2.0f);

	m_PS = new ParticleSystem(new Texture("textures/debug/img_test.png", 256, 256), 100);
	m_PS->SetScale(10.0f);

	MainCamera.FreezeCamera(true);

	//init skybox
	this->m_Skybox = new Skybox(g_ShaderLoader.GetShader(ShaderType::SKYBOX), "textures/cubemap/greenNeb_left.png", "textures/cubemap/greenNeb_right.png", "textures/cubemap/greenNeb_top.png", "textures/cubemap/greenNeb_bottom.png", "textures/cubemap/greenNeb_front.png", "textures/cubemap/greenNeb_back.png");

	Input.Initialize();

	m_Player->SetControllerID(0);
	m_Player->SetPosition(glm::vec3(0.0f, 2.5f, 45.0f));

	//octopus
	m_Octopus = new Octopus();
	m_Octopus->SetControllerID(1);
  m_Octopus->SetPlayer(m_Player);
  m_Player->m_Octopus = m_Octopus;


	//ship
	m_Ship = new Ship();
	m_Ship->SetPosition({ 0.0f, -1.0f, 70.0f });
	m_Octopus->SetShipRef(m_Ship);
	m_Player->SetShip(m_Ship);

	m_Water = new Model(g_ModelLoader.GetSubMeshOfModel(ModelLoader::WATER, "Plane"), ShaderType::WATER);
	m_Water->SetScale(300);
	m_Water->SetPosition({ 0, -20, 0 });

	g_SoundSystem.Init();
	g_SoundSystem.LoadSounds();

	
}

BucketPrototypeState::~BucketPrototypeState()
{
	delete m_Skybox;
	delete m_Player;
	delete m_PS;
	delete m_Octopus;
	delete m_Ship;
}

void BucketPrototypeState::Render()
{
	//clear
	glm::vec3 clearColor = glm::vec3(1.0f, 0.0f, 1.0f);
	glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	//skybox
	m_Skybox->Draw();

	//player
	m_Player->Render();
	m_PS->Render();

	m_Octopus->Render();

	m_Ship->Render();

	m_Water->Render();
	
}

void BucketPrototypeState::PhysicsTick(float a_DT)
{
	m_Octopus->PhysicsTick(a_DT);
}

void BucketPrototypeState::Tick(float a_DT)
{
	HandleInput(a_DT);

	m_Player->Tick(a_DT);

	m_Octopus->UpdateEyePosition(m_Player->GetPosition());

	m_PS->Tick(a_DT);
	m_Octopus->Tick(a_DT);
	m_Ship->Tick(a_DT);

	g_SoundSystem.PlayBgSound();
	g_SoundSystem.Update();

	m_Water->Update(a_DT);
}

void BucketPrototypeState::HandleInput(float a_DT)
{
	m_Player->HandleInput();
	if (Input.GetKeyDown(SDL_SCANCODE_P))
	{
		g_DebugManager.b_PausePhysics = !g_DebugManager.b_PausePhysics;
	}

	if (Input.GetKeyDown(SDL_SCANCODE_J))
	{
		m_PS->SetPosition(m_Player->GetPosition());
		m_PS->Emit(1);
	}
}
