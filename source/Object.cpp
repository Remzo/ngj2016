#include "pch.h"

#include "Object.h"
#include "Collider.h"

Object::Object()
{
	this->m_Position = glm::vec3(0);
	this->m_Rotation = glm::quat();
	this->m_ModelMatrix = glm::mat4(1);
	this->m_LocalModelMatrix = glm::mat4(1);
	this->b_DirtyMatrix = false;
	this->m_Parent = NULL;
	this->m_Children = std::vector<Object*>();
	this->m_Scale = glm::vec3(1.0f);
	this->m_Collider = NULL;
}

Object::~Object()
{
	if (this->m_Collider != NULL)
	{
		delete this->m_Collider;
		this->m_Collider = NULL;
	}
}

void Object::SetParent(Object* obj)
{
	if (obj == NULL)
	{
		//clear parent
		if (this->m_Parent != NULL)
		{
			//remove child from parent list
			for (auto i = this->m_Parent->m_Children.begin(); i != this->m_Parent->m_Children.end(); ++i)
			{
				if (*i == this)
				{
					this->m_Parent->m_Children.erase(i);
					break;
				}
			}

			this->m_Parent = NULL;
			this->SetDirtyModelMatrix();
		}
		return;
	}

	//place this object in the parent's m_Children vector
	obj->m_Children.push_back(this);

	//NOTE: once an object is parented it's position is now relative to it's parent
	if (this->m_Parent == 0)
	{
		glm::vec3 newPos = this->GetPosition() - obj->GetPosition();
		//this->SetPosition(newPos);
	}

	//new scale - needed to maintain absoulte scale when parent's scale != 1.0f
	float newScale = (this->GetScale() / obj->GetScale()).x;
	this->SetScale(newScale);

	//Assign parent to this object
	this->m_Parent = obj;
	this->b_DirtyMatrix = true;

}

bool Object::IsModelMatrixDirty() const
{
	return this->b_DirtyMatrix;
}

void Object::SetDirtyModelMatrix()
{
	this->b_DirtyMatrix = true;
	if (!this->m_Children.empty())
	{
		for (unsigned int i = 0; i < this->m_Children.size(); i++)
		{
			this->m_Children[i]->SetDirtyModelMatrix();
		}
	}
}

void Object::UpdateModelMatrix()
{
	if (!this->b_DirtyMatrix)
	{
		return;
	}

	this->b_DirtyMatrix = false;

	glm::mat4 scale = glm::scale(glm::mat4(1), this->m_Scale);

	glm::mat4 translation = glm::translate(glm::mat4(1), this->m_Position);

	glm::mat4 rotation = glm::toMat4(this->m_Rotation);


	this->m_LocalModelMatrix = (translation * rotation * scale);
	this->m_ModelMatrix = this->m_LocalModelMatrix;

	if(this->m_Parent != NULL)
	{
		this->m_ModelMatrix = this->m_Parent->GetModelMatrix() * this->m_LocalModelMatrix;
	}
}

void Object::SetModelMatrix(glm::mat4 mat)
{
	this->m_ModelMatrix = mat;
	this->m_Position = glm::vec3(mat[3]);
	this->m_Rotation = glm::quat(mat * glm::inverse(glm::translate(glm::mat4(1), this->m_Position)));
	SetDirtyModelMatrix();
}

glm::mat4 Object::GetModelMatrix()
{
	this->UpdateModelMatrix();

	return this->m_ModelMatrix;
}

void Object::SetPosition(const glm::vec3& pos)
{
	this->m_Position = pos;
	SetDirtyModelMatrix();
}

void Object::Translate(const glm::vec3 &translation, bool local /* = true*/)
{
	if (local)
	{
		this->m_Position += this->m_Rotation * translation;
	}
	else
	{
		this->m_Position += translation;
	}
	SetDirtyModelMatrix();
}

glm::vec3 Object::GetPosition(bool local) const
{
	if (local)
	{
		return this->m_Position;
	}
	else
	{
		if (this->m_Parent != NULL)
		{
			return this->m_Position + this->m_Parent->GetPosition(false);
		}
		else
		{
			return this->m_Position;
		}
	}
}

void Object::SetRotation(const glm::quat& rot)
{
	this->m_Rotation = rot;
	SetDirtyModelMatrix();
}

glm::quat Object::GetRotation( bool local) const
{
	if (!local && this->m_Parent != NULL)
	{
		return this->m_Parent->GetRotation(false) * this->m_Rotation;
	}
	
	return this->m_Rotation;
}

void Object::SetScale(float scale)
{
	this->SetScale(glm::vec3(scale));
}

void Object::SetScale(glm::vec3 scale)
{
	this->m_Scale = scale;

	//if (this->m_Collider != NULL)
	//{
	//	this->m_Collider->RescaleCollider(m_Scale.x);
	//}

	SetDirtyModelMatrix();
}

void Object::SetRotationEuler(const glm::vec3& anglesInDeg)
{
	this->m_Rotation = glm::quat(glm::radians(anglesInDeg));
	SetDirtyModelMatrix();
}

void Object::RotateEuler(const float angle, const glm::vec3& rotAxis, const bool local /* = true*/)
{
	glm::quat rot = glm::angleAxis(glm::radians(angle), glm::normalize(rotAxis));

	if (local)
	{
		this->m_Rotation = this->m_Rotation * rot;
	}
	else
	{
		this->m_Rotation = rot * this->m_Rotation;
	}
	SetDirtyModelMatrix();
}

glm::vec3 Object::GetRotationEuler() const
{
	return glm::degrees(glm::eulerAngles(this->m_Rotation));
}

glm::vec3 Object::ForwardVec() const
{
	return glm::normalize(this->GetRotation(false) * glm::vec3(0, 0, -1));
}

glm::vec3 Object::UpVec() const
{
	return glm::normalize(this->GetRotation(false) * glm::vec3(0, 1, 0));
}

glm::vec3 Object::RightVec() const
{
	return glm::normalize(this->GetRotation(false) * glm::vec3(1, 0, 0));
}