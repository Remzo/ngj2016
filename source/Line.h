#pragma once

#include "Model.h"

class Line : public Model
{
public:
	Line::Line(VertexComplete p1, VertexComplete p2);
	Line(glm::vec3 p1, glm::vec3 p2, glm::vec3 normal = glm::vec3(0.0f, 1.0f, 0.0f));
	~Line();

	void SetLineWidth(float width);

private:
	void Init();

protected:
	std::vector<VertexComplete> m_Vertices;
	std::vector<unsigned int> m_Indices;

	float m_Thickness = 1.0f;
};
