#include "pch.h"

#include "texture.h"
#include "skybox.h"

Skybox::Skybox(GLuint shaderID, const char* texLeft, const char* texRight, const char* texUp, const char* texDown, const char* texForward, const char* texBack)
{
	this->m_CubemapID = SOIL_load_OGL_cubemap(texRight, texLeft, texUp, texDown, texForward, texBack, SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	glBindTexture(GL_TEXTURE_CUBE_MAP, this->m_CubemapID);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	this->m_ModelMatrix = glm::mat4(1);
	this->m_shaderID = shaderID;
	this->Init();
}

Skybox::~Skybox()
{
	delete[] this->m_Indices;
	delete[] this->m_Vertices;
}

void Skybox::Init()
{
	//ModelMatrix
	this->m_ModelMatrix = glm::mat4(1);

	//Vertices/Indices
	this->m_Vertices = new VertexUV[24] 
	{
		//front face
		{ glm::vec3(-1, 1, 1),	glm::vec2(0, 0) }, // 0
		{ glm::vec3(1, 1, 1),	glm::vec2(1, 0) }, // 1
		{ glm::vec3(1, -1, 1), glm::vec2(1, 1) }, // 2
		{ glm::vec3(-1, -1, 1),	glm::vec2(0, 1) }, // 3
		
		//back face
		{ glm::vec3(1, 1, -1), glm::vec2(0, 0) }, // 4
		{ glm::vec3(-1, 1, -1), glm::vec2(1, 0) }, // 5
		{ glm::vec3(-1, -1, -1), glm::vec2(1, 1) }, // 6
		{ glm::vec3(1, -1, -1),	glm::vec2(0, 1) }, // 7

		//left face
		{ glm::vec3(-1, 1, -1),	glm::vec2(0, 0) }, // 8
		{ glm::vec3(-1, 1, 1), glm::vec2(1, 0) }, // 9
		{ glm::vec3(-1, -1, 1), glm::vec2(1, 1) }, // 10
		{ glm::vec3(-1, -1, -1), glm::vec2(0, 1) }, // 11

		//right face
		{ glm::vec3(1, 1, 1), glm::vec2(0, 0) }, // 12
		{ glm::vec3(1, 1, -1), glm::vec2(1, 0) }, // 13
		{ glm::vec3(1, -1, -1), glm::vec2(1, 1) }, // 14
		{ glm::vec3(1, -1, 1), glm::vec2(0, 1) }, // 15

		//top face
		{ glm::vec3(-1, 1, -1), glm::vec2(0, 0) }, // 16
		{ glm::vec3(1, 1, -1), glm::vec2(1, 0) }, // 17
		{ glm::vec3(1, 1, 1), glm::vec2(1, 1) }, // 18
		{ glm::vec3(-1, 1, 1), glm::vec2(0, 1) }, // 19

		//bottom face
		{ glm::vec3(-1, -1, 1), glm::vec2(0, 0) }, // 20
		{ glm::vec3(1, -1, 1), glm::vec2(1, 0) }, // 21
		{ glm::vec3(1, -1, -1), glm::vec2(1, 1) }, // 22
		{ glm::vec3(-1, -1, -1), glm::vec2(0, 1) }, // 23
	};
	this->m_Indices = new GLuint[36] 
	{
			0, 2, 1, 3, 2, 0,			   // Front face
			4, 6, 5, 7, 6, 4,		       // Back face
			8, 10, 9, 11, 10, 8,           // Left face
			12, 14, 13, 15, 14, 12,        // Right face
			16, 18, 17, 19, 18, 16,        // Top face
			20, 22, 21, 23, 22, 20            // Bottom face
	};

	//Buffers
	glGenVertexArrays(1, &this->m_VAO);
	glBindVertexArray(this->m_VAO);

	GLuint vertBuffer, indexBuffer;
	glGenBuffersARB(1, &vertBuffer);
	glGenBuffersARB(1, &indexBuffer);

	// Copy the vertex data to the VBO
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertBuffer);
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(VertexXYZColor)*24, this->m_Vertices, GL_STATIC_DRAW_ARB);

	// Copy the index data to the VBO
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, indexBuffer);
	glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, sizeof(GLuint)*36, this->m_Indices, GL_STATIC_DRAW_ARB);

	//Get attribute locations from shader
	GLint positionAtribID = glGetAttribLocation(this->m_shaderID, "in_position");
	GLint uvAtribID = glGetAttribLocation(this->m_shaderID, "in_uv");

	//Map attribute locations to VBO
	glVertexAttribPointer(positionAtribID, 3, GL_FLOAT, GL_FALSE, sizeof(VertexUV), MEMBER_OFFSET(VertexUV, m_Pos));
	glEnableVertexAttribArray(positionAtribID);

	glVertexAttribPointer(uvAtribID, 2, GL_FLOAT, GL_FALSE, sizeof(VertexUV), MEMBER_OFFSET(VertexUV, m_UV));
	glEnableVertexAttribArray(uvAtribID);

	//Unbind
	glBindVertexArray(0);
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
	glDisableVertexAttribArray(positionAtribID);
	glDisableVertexAttribArray(uvAtribID);
}

void Skybox::Draw(Camera* cam /* = NULL*/)
{
	glDepthMask(GL_FALSE);
	glDisable(GL_CULL_FACE);
	if (cam == NULL)
	{
		//Use MainCamera if no other camera is specified.
		cam = &MainCamera;
	}

	glBindVertexArray(this->m_VAO);
	glUseProgram(this->m_shaderID);

	//make the skybox's position equal to the camera
	this->m_ModelMatrix = glm::translate(glm::mat4(1), cam->GetPosition());
	glm::mat4 mvp = cam->GetProjectionMatrix() * cam->GetViewMatrix() * this->m_ModelMatrix;
	
	//Set MVP
	GLint uniformMVP = glGetUniformLocation(this->m_shaderID, "MVP");
	glUniformMatrix4fv(uniformMVP, 1, GL_FALSE, glm::value_ptr(mvp));

	// Get the locations for the uniform sampler variables defined in the fragment shader.
	GLint uniformTex0 = glGetUniformLocation(this->m_shaderID, "tex0");

	// Bind the first texture to the first texture unit.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, this->m_CubemapID);

	// Now bind the uniform samplers to the corresponding texture units.
	// Note that we must bind the uniform samplers to the texture unit,
	// not the texture object ID!
	glUniform1i(uniformTex0, 0); // Bind the first sampler to texture unit 0.

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, BUFFER_OFFSET(0));

	glUseProgram(0);
	glBindVertexArray(0);
	glDepthMask(GL_TRUE);
	glEnable(GL_CULL_FACE);
}