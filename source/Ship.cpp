#include "pch.h"
#include "Ship.h"
#include "Sphere.h"
#include "texture.h"
#include "HoleModel.h"

#define LEAKRATE 0.001f
Hole::Hole(glm::vec3 _startpos = glm::vec3(0))
{
	m_HoleModel = new HoleModel();
	m_HoleModel->SetParent(this);
	SetPosition(_startpos);
	isOpen = true;
}

Hole::~Hole()
{
	if (m_HoleModel) delete m_HoleModel; m_HoleModel = 0;

}
void Hole::Render()
{
	m_HoleModel->Render();
}

Ship::Ship()
{
	health = 1.f;
	holes.clear();

	m_Model = new Model(g_ModelLoader.GetSubMeshOfModel(ModelLoader::SHIP, "Matrix_1.001"), ShaderType::TEXTURED_UNLIT);
	m_Model->m_Texture = new Texture("textures/ship.png", 512, 256);
	m_Model->SetParent(this);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Model->m_Texture->GetTextureID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);
}

Ship::~Ship()
{
	delete m_Model;
}

void Ship::Render()
{
	m_Model->Render();
	for (int i = 0; i < holes.size(); i++)
		holes[i]->Render();
}
void Ship::UpdateDamage(float _dt)
{
	for (int i = 0; i < holes.size(); i++)
		if (holes[i]->isOpen)
			health -= LEAKRATE*_dt;
}
void Ship::Tick(float _dt)
{
	UpdateDamage(_dt);
	if (GetAsyncKeyState('K')) printf(" %f", health);

	if (health < 0)
		printf("ship sunk octopus looses");
}