#include "pch.h"

#include "camera.h"

Camera& MainCamera = Camera();

Camera::Camera()
{
	this->SetPosition(glm::vec3(0, 0, 0));
}

Camera::~Camera()
{
}

void Camera::SetPerspective(float fovDeg, int scrWidth, int scrHeight, float zNear, float zFar)
{
	this->m_ProjectionMatrix = glm::perspective(glm::radians(fovDeg), scrWidth / (float)scrHeight, zNear, zFar);
	this->m_ProjType = ProjectionType::PERSPECTIVE;
}

void Camera::SetOrthographic(const float left, const float right, const float bottom, const float top, const float zNear, const float zFar)
{
	this->m_ProjectionMatrix = glm::ortho(left, right, bottom, top, zNear, zFar);
	this->m_ProjType = ProjectionType::OTHROGRAPHIC;
}

glm::vec3 Camera::GetPosition() const
{
	return this->m_Position;
}

glm::mat4 Camera::GetProjectionMatrix()
{
	return this->m_ProjectionMatrix;
}

glm::mat4 Camera::GetViewMatrix()
{
	UpdateViewMatrix();
	return this->m_ViewMatrix;
}

void Camera::UpdateViewMatrix()
{
	if (!this->m_DirtyViewMatrix)
		return;

	this->m_DirtyViewMatrix = false;

	glm::mat4 translation = glm::translate(glm::mat4(1), -this->m_Position);

	//Transpose because the rotation matrix is orthonormalised == inverse but cheaper
	glm::mat4 rotation = glm::transpose(glm::toMat4(this->m_Rotation));

	this->m_ViewMatrix = rotation * translation;

}

void Camera::SetPosition(const glm::vec3 &pos)
{
	this->m_Position = pos;
	this->m_DirtyViewMatrix = true;
}

void Camera::Translate(const glm::vec3 &translation, bool local /* = true*/)
{
	if (local)
	{
		this->m_Position += this->m_Rotation * translation;
	}
	else
	{
		this->m_Position += translation;
	}
	this->m_DirtyViewMatrix = true;
}

void Camera::SetRotation(const glm::quat& rot)
{
	this->m_Rotation = rot;
	this->m_DirtyViewMatrix = true;
}

glm::quat Camera::GetRotation() const
{
	return this->m_Rotation;
}

glm::vec3 Camera::ForwardVec() const
{
	return glm::normalize(this->GetRotation() * glm::vec3(0, 0, -1));
}

void Camera::SetRotationEuler(const glm::vec3& anglesInDeg)
{
	this->m_Rotation = glm::quat(glm::radians(anglesInDeg));
	this->m_DirtyViewMatrix = true;
}

glm::vec3 Camera::GetRotationEuler() const
{
	return glm::degrees(glm::eulerAngles(this->m_Rotation));
}

void Camera::Tick(const float& a_deltaTime)
{
	if (this == &MainCamera)
	{
		this->HandleInput(a_deltaTime);
	}
}

void Camera::Reset()
{
	this->SetPosition(glm::vec3(0, 0, 0));
	this->SetRotation(glm::quat());
}

void Camera::HandleInput(const float& a_deltaTime)
{
	if (b_FreezeCamera)
	{
		return;
	}

	//ResetPosition
	if(Input.GetKeyDown(SDL_SCANCODE_X))
	{
		Reset();
		return;
	}

	float speedFactor = (Input.GetKey(SDL_SCANCODE_LSHIFT)) ? this->m_SpeedIncreaseFactor : 1.0f;

	//if (this->m_ProjType == ProjectionType::OTHROGRAPHIC)
	//{
	//	return;
	//}

	//Translation
	//Forward
	if (Input.GetKey(SDL_SCANCODE_W))
	{
		this->Translate(glm::vec3(0, 0, -this->m_MovementSpeed * speedFactor)*a_deltaTime);
	}

	//Left
	if (Input.GetKey(SDL_SCANCODE_A))
	{
		this->Translate(glm::vec3(-this->m_MovementSpeed * speedFactor, 0, 0)*a_deltaTime);
	}

	//Right
	if (Input.GetKey(SDL_SCANCODE_D))
	{
		this->Translate(glm::vec3(this->m_MovementSpeed * speedFactor, 0, 0)*a_deltaTime);
	}

	//Back
	if (Input.GetKey(SDL_SCANCODE_S))
	{
		this->Translate(glm::vec3(0, 0, this->m_MovementSpeed * speedFactor)*a_deltaTime);
	}

	//Down
	if (Input.GetKey(SDL_SCANCODE_F))
	{
		this->Translate(glm::vec3(0, -this->m_MovementSpeed * speedFactor, 0)*a_deltaTime);
	}

	//Up
	if (Input.GetKey(SDL_SCANCODE_R))
	{
		Translate(glm::vec3(0, this->m_MovementSpeed * speedFactor, 0)*a_deltaTime);
	}

	//Rotation
	//Yaw Left
	if (Input.GetKey(SDL_SCANCODE_LEFT))
	{
		this->RotateEuler(this->m_RotationSpeed * speedFactor * a_deltaTime, glm::vec3(0, 1, 0), false);
	}

	//Yaw Right
	if (Input.GetKey(SDL_SCANCODE_RIGHT))
	{
		this->RotateEuler(-this->m_RotationSpeed * speedFactor * a_deltaTime, glm::vec3(0, 1, 0), false);
	}

	//clamp when directly down or up
	glm::vec3 curEuler = this->GetRotationEuler();

	
	//Pitch correction because roll gets set to 180deg when yaw is >90deg causing pitch sign to flip for unknown reasons. Needed for pitch clamping
	float curPitch = glm::degrees(glm::pitch(this->m_Rotation));
	float curRoll = glm::degrees(glm::roll(this->m_Rotation));

	if (abs(curRoll) >= 90.0f)
	{
		if (curPitch > 0.0f)
		{
			curPitch -= 180.0f;
		}
		else
		{
			curPitch += 180.0f;
		}
	}

	//Pitch Down
	if (Input.GetKey(SDL_SCANCODE_UP))
	{
		if (curPitch > -90.0f)
		{
			this->RotateEuler(-this->m_RotationSpeed * speedFactor * a_deltaTime, glm::vec3(1, 0, 0));
		}
	}

	//Pitch Up
	if (Input.GetKey(SDL_SCANCODE_DOWN))
	{
		if (curPitch < 90.0f)
		{
			this->RotateEuler(this->m_RotationSpeed * speedFactor * a_deltaTime, glm::vec3(1, 0, 0));
		}
	}

	//Roll clockwise
	if (Input.GetKey(SDL_SCANCODE_Q))
	{
		this->RotateEuler(this->m_RotationSpeed * speedFactor * a_deltaTime, glm::vec3(0, 0, 1));
	}

	//Roll counterclockwise
	if (Input.GetKey(SDL_SCANCODE_E))
	{
		this->RotateEuler(-this->m_RotationSpeed * speedFactor * a_deltaTime, glm::vec3(0, 0, 1));
	}

}

void Camera::FreezeCamera(bool freeze)
{
	b_FreezeCamera = freeze;
}

void Camera::SetMovementSpeed(const float speed)
{
	this->m_MovementSpeed = speed;
}

float Camera::GetMovementSpeed() const
{
	return this->m_MovementSpeed;
}

void Camera::RotateEuler(const float angle, const glm::vec3& rotAxis, const bool local /* = true*/)
{
	glm::quat rot = glm::angleAxis(glm::radians(angle), rotAxis);

	if (local)
	{
		this->m_Rotation = this->m_Rotation * rot;
	}
	else
	{
		this->m_Rotation = rot * this->m_Rotation;
	}

	this->m_DirtyViewMatrix = true;
}