#include "pch.h"
#include "GLerrorHelper.h"

#include "stdio.h"


void CheckOGLerror()
{
	return;
	char errorString[0xffff];
	errorString[0] = 0;

	GLenum curError;
	do 
	{
		curError = glGetError();
		if( curError != GL_NO_ERROR )
		{
			char tmps[256];
			sprintf_s(tmps, 256, "'%s' ", glewGetErrorString(curError));
			strcat_s( errorString, 0xffff-1, tmps );
		}
	} while ( curError != GL_NO_ERROR );

	if( strlen( errorString) != 0 )
	{
		DebugBreak();
		MessageBox( NULL, errorString, "OpenGL errors", MB_ICONSTOP );
		exit(1);
	}
}
