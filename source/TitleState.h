#pragma once
#include "State.h"

class TitleState: public State
{
public:

	TitleState(float halfHeight, float halfWidth, StateManager* stateManager);
	~TitleState();

	void Tick(float a_DT);
	void Render();

private:
	void UpdateAccumulators(float a_DT);
	void UpdateTitle(float a_DT);
	void HandleInput();
};
