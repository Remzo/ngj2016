#pragma once

#include "sphere.h"
#include "PhysicsObject.h"

class Boat;
class Cannonball : public Sphere, public PhysicsObject
{
public:
	Cannonball(Boat* owner, glm::vec3 pos, glm::vec3 vel);
	~Cannonball();

	Boat* GetOwner() const { return m_Owner; }

private:
	Boat* m_Owner;
};
