#pragma once

#include "State.h"

class StateManager;
class Quad;
class Skybox;
class Octopus;
class Ship;
class LaviniaState : public State
{
public:
	LaviniaState(float halfHeight, float halfWidth, StateManager* stateManager);
	~LaviniaState();

private:
	void Tick(float a_DT);
	void PhysicsTick(float a_DT);
	void Render();

	void HandleInput(float a_DT);
	

	Quad* m_Water;
	Skybox* m_Skybox;
	Octopus* octopus;
	Ship* ship;
};
