#include "pch.h"
#include "Mesh.h"

Mesh::Mesh()
{
}

Mesh::Mesh(const Mesh& mesh)// mesh mesh mesh mesh
{
	this->m_NumIndices = mesh.m_NumIndices;
	this->m_NumVerts = mesh.m_NumVerts;
	this->m_Indices = mesh.m_Indices;
	this->m_VertexData = mesh.m_VertexData;
}

//if indices.size() == 0, indices are generated from vertexData: incrementing count until vertexData.size()
Mesh::Mesh(std::vector<VertexComplete> vertexData, std::vector<unsigned int> indices)
{
	if (indices.size() == 0)
	{
		indices = GenerateIndicesFromVertexDataSize(vertexData.size());
	}

	this->m_NumIndices = indices.size();
	this->m_NumVerts = vertexData.size();
	this->m_Indices = indices;
	this->m_VertexData = vertexData;
}

Mesh::~Mesh()
{
	this->m_Indices.clear();
	this->m_VertexData.clear();
}

std::vector<unsigned int> Mesh::GenerateIndicesFromVertexDataSize(unsigned int vertexDataSize)
{
	std::vector<unsigned int> outIndices;

	for (unsigned int i = 0; i < vertexDataSize; ++i)
	{
		outIndices.push_back(i);
		outIndices.push_back((i + 1) % vertexDataSize);

	}

	return outIndices;
}