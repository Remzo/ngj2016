#pragma once

class StateManager;
class State
{
public:
	State(StateManager* stateManager);
	virtual ~State() = 0;

	virtual void Tick(float a_DT) = 0;
	virtual void Render() = 0;
	virtual void PhysicsTick(float a_DT);
protected:
	StateManager* const m_StateManager; // const pointer to statemanager
};
