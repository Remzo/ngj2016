#pragma once

#define WIN32_LEAN_AND_MEAN		// http://stackoverflow.com/questions/11040133/what-does-defining-win32-lean-and-mean-exclude-exactly
#include <windows.h>

#define GLEW_STATIC
#include <GLEW/glew.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\quaternion.hpp>

#include <stddef.h>

#include <SOIL\SOIL.h>

#define SDL_MAIN_HANDLED
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>

#include <string>
#include <map>
#include <vector>
#include <list>
#include <queue>

#include "structs.h"


#include "Globals.h"
#include "shaderLoader.h"
#include "inputManager.h"
#include "logger.h"
#include "camera.h"
#include "FontRenderer.h"
#include "ModelLoader.h"
//#include "AudioSystem.h"
#include "CollisionHandler.h"
#include "DebugManager.h"

/*
This macro uses the offsetof macro defined in the �stddef.h� header file.
The offsetof macro will return the offset in bytes to the member (m) in 
the struct (s). The MEMBER_OFFSET macro will convert the size_t value 
returned by this macro to a pointer type that the glVertexPointer method accepts.
*/
#define MEMBER_OFFSET(s,m) ((char *)NULL + (offsetof(s,m)))

/*
This macro just casts the integer buffer offset value to a pointer type that is accepted by the glDrawElements method.
*/
#define BUFFER_OFFSET(i) ((char *)NULL + (i))



/////////// GAME
#include "CannonballManager.h"