#pragma once

struct SDL_Window;
struct SDL_Surface;
class Window {
public:
	Window( const unsigned int& a_windowWidth, const unsigned int& a_windowHeight );
	~Window();

	SDL_Window* m_pSDLwindow;
	SDL_GLContext m_glContext;
	unsigned int m_windowWidth;
	unsigned int m_windowHeight;
};