#include "pch.h"

#include "skybox.h"
#include "sphere.h"
#include "game.h"
#include "window.h"
#include "StateManager.h"

Game::Game( bool& a_quit, Window* const& a_pWindow ) 
: m_quit( a_quit ), m_pWindow( a_pWindow ),m_pWidth( &a_pWindow->m_windowWidth ),m_pHeight( &a_pWindow->m_windowHeight ) 
{
}

void Game::Init() 
{
	GlobalInfo.m_HalfGameHeight = 100.0f;
	GlobalInfo.m_HalfGameWidth = 100.0f;

	this->LoadShaders();

	//init skybox
	this->m_Skybox = new Skybox(g_ShaderLoader.GetShader(ShaderType::SKYBOX), "textures/cubemap/greenNeb_left.png", "textures/cubemap/greenNeb_right.png", "textures/cubemap/greenNeb_top.png", "textures/cubemap/greenNeb_bottom.png", "textures/cubemap/greenNeb_front.png", "textures/cubemap/greenNeb_back.png");

	//init MainCamera
	MainCamera.SetPerspective(60.0f, *m_pWidth, *m_pHeight, 0.01f, 1000.0f);
	MainCamera.SetPosition(glm::vec3(0, 0, 10));

	//init stateManager
	m_StateManager = new StateManager();
	g_ModelLoader.LoadAllModels();
}

void Game::LoadShaders()
{
	g_ShaderLoader.LoadShaders();
}

void Game::Tick( const float& a_deltaTime ) 
{
	this->HandleInput();

	MainCamera.Tick(a_deltaTime);

	m_StateManager->Tick(a_deltaTime);

}

void Game::PhysicsTick( const float& a_deltaTime ) 
{
	if (!g_DebugManager.b_PausePhysics)
	{
		m_StateManager->PhysicsTick(a_deltaTime);
		g_CollisionManager.Tick(a_deltaTime);
	}
}

void Game::Render() const 
{
	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK);

	this->m_Skybox->Draw();

	m_StateManager->Render();
}

void Game::OnWindowResize() 
{
	Log.WriteDebug( "Window resized to %dx%d", *m_pWidth, *m_pHeight );

	MainCamera.SetPerspective(60.0f, *m_pWidth, *m_pHeight, 0.01f, 1000.0f);

	glViewport(0, 0, *m_pWidth, *m_pHeight);
}

void Game::Clean() 
{
	delete this->m_Skybox;
	delete this->m_StateManager;
}

void Game::HandleInput()
{
	if (Input.GetKeyDown(SDL_SCANCODE_1))
	{
		this->m_StateManager->SetState(StateManager::States::MENU);
	}

	if (Input.GetKeyDown(SDL_SCANCODE_2))
	{
		this->m_StateManager->SetState(StateManager::States::PLAY);
	}

	if (Input.GetKeyDown(SDL_SCANCODE_3))
	{
		this->m_StateManager->SetState(StateManager::States::DEBUG);
	}

  if (Input.GetKeyDown(SDL_SCANCODE_4)) {
    this->m_StateManager->SetState(StateManager::States::KEY_PRESS_PROTOTYPE);
  }

  if (Input.GetKeyDown(SDL_SCANCODE_F12)) {
    g_ShaderLoader.LoadShaders();
  }
	if (Input.GetKeyDown(SDL_SCANCODE_5))
	{
		this->m_StateManager->SetState(StateManager::States::LAVINIASTATE);
	}

	if (Input.GetKeyDown(SDL_SCANCODE_9))
	{
		this->m_StateManager->SetState(StateManager::States::CHARACTER_MOVE);
	}
}