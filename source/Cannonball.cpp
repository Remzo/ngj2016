#include "pch.h"

#include  "Cannonball.h"

Cannonball::Cannonball(Boat* owner, glm::vec3 pos, glm::vec3 vel) : Sphere(0.3f)
{
	this->m_Owner = owner;
	this->SetColor(glm::vec4(glm::vec3(0.1f), 1.0f));
	this->SetPosition(pos);
	this->SetVelocity(vel);
}

Cannonball::~Cannonball()
{
}