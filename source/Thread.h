#pragma once

class Thread
{
public:

	Thread();
	~Thread();

	HANDLE GetHandle();
	void WaitForFinish();
	void Kill();

protected:
	void BackgroundTask();
	virtual void ThreadFunction() = 0;

private:
	static DWORD WINAPI ThreadProc(__in LPVOID lpParam);

	HANDLE m_Handle;
	bool shutdown;
};