#include "pch.h"
#include "TentacleTip.h"
#include "Tentacle.h"
#include <glm/ext.hpp>
#include "Player.h"
#include "SoundSystem.h"

extern SoundSystem & g_SoundSystem;
TentacleTip::TentacleTip(int index)
{
  glm::vec4 dir4 = glm::vec4(1, 0, 0, 0) * glm::rotate<float>( glm::radians(10.0f + 160.0f / 8.0f * (float)index), glm::vec3(0, 1, 0));
  
  glm::vec3 startPos = glm::vec3(15 -float(index)/8.0 * 30.0f, -2.5, 25);
  glm::vec3 dir = { dir4.x, dir4.y, dir4.z };
  
  modeltest = new Sphere();
	modeltest->SetColor(glm::vec4(1, 0, 0, 1));
	m_StartPos = dir * 9.0f * 1.5f + startPos;
	SetPosition(m_StartPos);
	velocity = glm::vec3(0);
	modeltest->SetParent(this);

  std::vector<glm::vec3> points;
  for (int i = 0; i < 10; i++) {
    points.push_back(dir * 1.5f * (float)i + startPos);
  }

  tentacle = std::make_unique<Tentacle>(points);
  holeIndex = -1;
}

void TentacleTip::SetPlayer(Player* player) {
  tentacle->player = player;
}

void TentacleTip::FreeHole() 
{
	holeIndex = -1;
  tentacle->SetInHole(false);
}
void TentacleTip::SetHole(int _holeindex)
{
	holeIndex = _holeindex;
  tentacle->SetInHole(true);
}
int TentacleTip::GetHoleIndex()
{
	return holeIndex;
}

TentacleTip::~TentacleTip()
{
	if (modeltest) delete modeltest; modeltest = 0;
}
void TentacleTip::Render()
{
  tentacle->Render();
}

float TentacleTip::GetDistance(glm::vec3 pos) {
  return tentacle->GetDistance(pos);
}

void TentacleTip::SetColor(glm::vec4 _color)
{
	modeltest->SetColor(_color);
}

void TentacleTip::PhysicsTick(float _dt) {
  tentacle->Update(_dt);
}

void TentacleTip::Freeze() {
  velocity = { 0, 0, 0 };
  this->tentacle->MoveEndpoint(velocity, 0);
}

void TentacleTip::Stab()
{
	if (b_Stabbing || b_StabCooldown)
		return;
	b_Stabbing = true;
	b_StabCooldown = false;
	m_StabTimer = 0.0f;

  prevState = tentacle->tipState;
  tentacle->tipState = Tentacle::TipState::POKE;
  g_SoundSystem.PlaySound(g_SoundSystem.soundOctoAttack);
}

void TentacleTip::UpdateVelocity(glm::vec3 _inputdir, float _dt)
{
	float accel = c_Acceleration;
	float friction = c_Friction;

	if (b_Stabbing)
	{
		m_StabTimer += _dt;
		accel = c_StabAcceleration;
		friction = c_StabFriction;
		m_StabTimer = c_StabCooldown;

		if (m_StabTimer >= c_StabTime) 
		{
			b_Stabbing = false;
			b_StabCooldown = true; 
		}
	}
	else if (b_StabCooldown && m_StabTimer >= 0.0f)
	{
		m_StabTimer -= _dt;
		if (m_StabTimer <= 0.0f)
		{
			//Log.WriteDebug("Stab stopped");
			b_Stabbing = false;
			b_StabCooldown = false; 
      tentacle->tipState = prevState;
			// velocity = glm::vec3(0.0f); 
		}
	}

	//reduce vel
	velocity -= friction * velocity * _dt;

	//add vel
	if (_inputdir != glm::vec3(0.0f))
	{
		velocity += accel * _inputdir * _dt;
	}

	//add pos
	this->m_Position += velocity * _dt;
  this->tentacle->MoveEndpoint(velocity, _dt);
	this->SetPosition(m_Position);
}


