#include "pch.h"

#include "CollisionDebugState.h"
#include "Collider.h"

CollisionDebugState::CollisionDebugState(float halfHeight, float halfWidth, StateManager* stateManager) : State(stateManager)
{
	m_Collider1 = new Collider(glm::vec3(0.5), CollisionHandler::CollisionTypes::DYNAMIC);
	m_Collider2 = new Collider(glm::vec3(0.5), CollisionHandler::CollisionTypes::DYNAMIC);

	m_Collider1->Translate(glm::vec3(0.6f, 0.0f, 0.0f));
	m_Collider2->Translate(glm::vec3(-0.6f, 0.0f, 0.0f));

	g_CollisionManager.RegisterCollider(m_Collider1);
	g_CollisionManager.RegisterCollider(m_Collider2);
}

CollisionDebugState::~CollisionDebugState()
{
	delete m_Collider1;
	delete m_Collider2;
}

void CollisionDebugState::Tick(float a_DT)
{
	HandleInput(a_DT);
}

void CollisionDebugState::Render()
{
	m_Collider1->Render();
	m_Collider2->Render();
}

void CollisionDebugState::HandleInput(float a_DT)
{
	const float s = 1.0f;
	if (Input.GetKey(SDL_SCANCODE_I))
	{
		m_Collider1->Translate(glm::vec3(0.0f, 0.0f, -s * a_DT), false);
	}

	if (Input.GetKey(SDL_SCANCODE_K))
	{
		m_Collider1->Translate(glm::vec3(0.0f, 0.0f, s * a_DT), false);
	}

	if (Input.GetKey(SDL_SCANCODE_J))
	{
		m_Collider1->Translate(glm::vec3(-s * a_DT, 0.0f, 0.0f), false);
	}

	if (Input.GetKey(SDL_SCANCODE_L))
	{
		m_Collider1->Translate(glm::vec3(s * a_DT, 0.0f, 0.0f), false);
	}

	if (Input.GetKey(SDL_SCANCODE_U))
	{
		m_Collider1->RotateEuler(45.0f * a_DT, glm::vec3(0.0f, 1.0f, 0.0f), false);
	}

	if (Input.GetKey(SDL_SCANCODE_O))
	{
		m_Collider1->RotateEuler(-45.0f * a_DT, glm::vec3(0.0f, 1.0f, 0.0f), false);
	}
}