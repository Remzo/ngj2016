#pragma once

class Skybox;
class Window;
class Camera;
class StateManager;
class Game {
public:	
	Game(bool& a_quit, Window* const& a_pWindow);
	~Game() {}

	void Quit() {
		m_quit = true;
	}

	static glm::vec4 s_PlayerColors[8];

private:
	friend int main();
	friend void CheckEvent( const SDL_Event& a_event, bool& a_quit, Window& a_window, Game& a_game );
	
	void Init();
	void Tick( const float& a_deltaTime );
	void PhysicsTick( const float& a_deltaTime ); 
	void Render() const;
	void HandleInput();

	void OnWindowResize();
	void Clean();

	void LoadShaders();

	void DrawTriangle() const;
	void DrawCube() const;

	float m_fps;

	bool& m_quit;

	unsigned int* const m_pWidth;
	unsigned int* const m_pHeight;

	Window* const m_pWindow;

	Skybox* m_Skybox;

	StateManager* m_StateManager;
};