#pragma once

#include "Model.h"
#include "GameObject.h"

class Quad : public Model
{
public:
	Quad();
	~Quad();

private:

protected:
	std::vector<VertexComplete> m_Vertices;
	std::vector<unsigned int> m_Indices;

};
