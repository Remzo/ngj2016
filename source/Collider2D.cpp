#include "pch.h"

#include "Collider2D.h"
#include "Mesh.h"

Collider2D::Collider2D()
{
	this->SetGLRenderMode(GL_LINES);
	this->SetColor(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
}

Collider2D::~Collider2D()
{
	m_Points.clear();
}

void Collider2D::SetPoints(std::vector<glm::vec3> points, float scale)
{
	std::vector<VertexComplete> vertData;

	m_Points = points;

	for (auto i : points)
	{	
		vertData.push_back(VertexComplete(i, glm::vec3(0, 0, 1), glm::vec2(0, 0), glm::vec4(1)));
	}

	this->SetMesh(new Mesh(vertData));

	this->SetScale(scale);
}

void Collider2D::RescaleCollider(float scale)
{
	SetPoints(m_Points, scale);
}

std::vector<glm::vec3> Collider2D::GetCollisionPoints()
{
	std::vector<glm::vec3> outPoints;

	glm::quat myRot = this->GetRotation(false);
	glm::vec3 myPos = this->GetPosition(false);

	for (auto i = m_Points.begin(); i != m_Points.end(); ++i)
	{
		outPoints.push_back(( ((*i) * m_Scale) * myRot) + myPos);
	}

	return outPoints;
}
