#pragma once
//#include <string>
//#include <iostream>
#include <fstream>

namespace LogType {
	enum Type {
		INFO = 0,
		DEBUG,
		WARNING,
		FATAL
	};
	static const char* TypeStrings[] = { "Info", "Debug", "Warning", "Fatal" };
};

class Logger {
public:
	Logger();
	~Logger();

	void Write( const LogType::Type& a_type, const char* a_pMessage, ... );
	void WriteInfo( const char* a_pMessage, ... );
	void WriteDebug( const char* a_pMessage, ... );
	void WriteWarning( const char* a_pMessage, ... );
	void WriteFatal( const char* a_pMessage, ... );
private:
	void WriteInternal( const LogType::Type& a_type, const char a_message[512] );
	static const std::string ComputeTimeString();
	std::ofstream m_file;
	const char* m_filename;
};

extern Logger& Log;

