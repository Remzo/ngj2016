#pragma once

class XInputController;
class InputManager {
public:
	InputManager();
	~InputManager();

	enum GamepadButtons{
		BUTTON_A = 0,
		BUTTON_B,
		BUTTON_X,
		BUTTON_Y,
		BUTTON_LEFT,
		BUTTON_RIGHT,
		BUTTON_DOWN,
		BUTTON_UP,
		RIGHT_SHOULDER,
		LEFT_SHOULDER,
		RIGHT_THUMB,
		LEFT_THUMB,
		NUM_BUTTONS
	};

	void ResetKeys();

	bool GetKeyDown(const SDL_Scancode& a_key) const;
	bool GetKeyUp(const SDL_Scancode& a_key) const;
	bool GetKey(const SDL_Scancode& a_key) const;
	bool GetKeyChange(const SDL_Scancode& a_key) const;

	bool GetMouseDown(const int& a_mouseButton) const;
	bool GetMouseUp(const int& a_mouseButton) const;
	bool GetMouse(const int& a_mouseButton) const;


	void SetMouseXY(const int& a_mouseX, const int& a_mouseY, const int& a_mouseDX = 0, const int& a_mouseDY = 0);
	int GetMouseX() const { return m_mouseX; }
	int GetMouseY() const { return m_mouseY; }
	glm::vec2 GetMousePos() const { return glm::vec2(m_mouseX, m_mouseY); }

	int GetMouseXMove() const { return m_mouseDeltaX; }
	int GetMouseYMove() const { return m_mouseDeltaY; }
	glm::vec2 GetMouseMove() const;

	int GetMouseXClick() const { return m_mouseClickX; }
	int GetMouseYClick() const { return m_mouseClickY; }
	glm::vec2 GetClickPos() const { return glm::vec2(m_mouseClickX, m_mouseClickY); }

	const SDL_Scancode GetLastKeyPress() const { return m_LastKeyPress; }

	void SetMouseHold(const Uint8& a_mouseButton, const bool& a_held) { if (m_mouse[a_mouseButton] != a_held) { m_click[a_mouseButton] = true; m_mouseClickX = m_mouseX; m_mouseClickY = m_mouseY; } m_mouse[a_mouseButton] = a_held; }
	void SetHold(const SDL_Scancode& a_key, const bool& a_held);

	void ResetHoldChange();
	void ResetXInputs();

	void Tick(float a_DT);
	void Initialize();

	float GetTriggerValue(int controllerID, bool right = false) const;
	bool GetButtonDown(int playerID, GamepadButtons button) const;
	bool GetButtonUp(int playerID, GamepadButtons button) const;
	bool GetButton(int playerID, GamepadButtons button) const;
	glm::vec2 GetStickState(int playerID, bool rightStick = true) const;
	bool* ConnectedControllers();	//returns bool[4]
	void VibrateController(int playerID, int left, int right);
	float GetStickClockwiseDelta(int controllerID, bool rightStick = true) const;
	bool IsStickNeutral(int controllerID, bool rightStick = true) const;

private:
	void ResetLastXInputStates();
	void CheckControllersConnected();
	DWORD GetXInputMaskFromButton(GamepadButtons button) const;
	void UpdateStickDeltas();
	float CalcStickClockwiseDelta(int controllerID, bool rightStick = true) const;

	bool m_ButtonLastState[4][NUM_BUTTONS];
	bool m_ButtonHeldChange[4][NUM_BUTTONS];

	bool m_held[SDL_NUM_SCANCODES];
	bool m_heldChange[SDL_NUM_SCANCODES];

	bool m_mouse[5];
	bool m_click[5];

	int m_mouseClickX;
	int m_mouseClickY;
	int m_oldMouseX;
	int m_oldMouseY;
	int m_mouseX;
	int m_mouseY;
	int m_mouseDeltaX;
	int m_mouseDeltaY;

	int m_PixelsPerXUnit;
	int m_PixelsPerYUnit;

	bool m_Initialised = false;
	bool b_ControllerConnected[4];

	SDL_Scancode m_LastKeyPress = SDL_SCANCODE_UNKNOWN;

	XInputController* m_XInputControllers[4];

	glm::vec2 m_LeftStickLastState[4];
	glm::vec2 m_RightStickLastState[4];
	float m_LeftStickClockwiseDelta[4];
	float m_RightStickClockwiseDelta[4];
};

extern InputManager& Input;