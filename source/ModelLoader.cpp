#include "pch.h"
#include "Mesh.h"
#include "Model.h"
#include "ModelLoader.h"

ModelLoader& g_ModelLoader = ModelLoader();

ModelLoader::ModelLoader()
{
}

ModelLoader::~ModelLoader()
{
	for (auto it = this->m_LoadedMeshes.begin(); it != this->m_LoadedMeshes.end(); it++)
	{
		delete it->second;
	}
	this->m_LoadedMeshes.clear();
	delete this->m_LastLoadedMesh;
}

std::vector<Mesh*> ModelLoader::LoadObj(const char* filePath, bool loadSeperateMeshes)
{
	std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
	std::vector< glm::vec3 > temp_vertices;
	std::vector< glm::vec2 > temp_uvs;
	std::vector< glm::vec3 > temp_normals;

	std::vector<VertexComplete> out_Verts;

	std::vector<Mesh*> outMeshes;

	bool validName = false;

	char curName[128] = { 0 };
	auto a = sizeof(curName);
	char prevName[128] = { 0 };
	auto b = sizeof(prevName);

	//Parse File
	FILE * file;
	fopen_s(&file, filePath, "r");
	if (file == NULL){
		Log.WriteFatal("Impossible to open the file !\n");
	}

	Log.WriteInfo("Loading Model: %s", filePath);

	while (1)
	{
		char lineHeader[128];
		// read the first word of the line
		int res = fscanf_s(file, "%s", lineHeader, sizeof(lineHeader));
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader
		//vertices
		if (strcmp(lineHeader, "o") == 0)
		{
			if (loadSeperateMeshes)
			{
				memcpy(prevName, curName, sizeof(curName));
				fscanf_s(file, "%s", curName, sizeof(curName)); //get mesh name

				m_SubMeshNames[filePath].push_back(curName);

				//process the mesh data that we have so far for the prevName mesh
				if (validName) // skip first 'o'
				{
					std::vector<unsigned int> indexOut;
					//Process Data
					for (unsigned int i = 0; i < vertexIndices.size(); i++)
					{
						unsigned int vertexIndex = vertexIndices[i] - 1 ; // -1 because obj indexing starts at 1
						unsigned int uvIndex = uvIndices[i] - 1 ;
						unsigned int normalIndex = normalIndices[i] - 1 ;

						indexOut.push_back(i);

						glm::vec3 vertex = temp_vertices[vertexIndex];
						glm::vec3 vertexNormal = temp_normals[normalIndex];
						glm::vec2 vertexUV = temp_uvs[uvIndex];

						float tempU = vertexUV.x;
						vertexUV.x = vertexUV.y;
						vertexUV.y = tempU;
						out_Verts.push_back(VertexComplete(vertex, vertexNormal, vertexUV, glm::vec4(1)));
					}

					Mesh* newMesh = new Mesh(out_Verts, indexOut);

					outMeshes.push_back(newMesh);
					this->m_LoadedMeshes[prevName] = newMesh;

					out_Verts.clear();
					indexOut.clear();
					vertexIndices.clear();
					uvIndices.clear();
					normalIndices.clear();
				}
				validName = true;
			}

		}
		else if (strcmp(lineHeader, "v") == 0){
			glm::vec3 vertex;
			fscanf_s(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);

		}
		//uvs
		else if (strcmp(lineHeader, "vt") == 0){
			glm::vec2 uv;
			fscanf_s(file, "%f %f\n", &uv.x, &uv.y);
			temp_uvs.push_back(uv);
		}
		//normals
		else if (strcmp(lineHeader, "vn") == 0){
			glm::vec3 normal;
			fscanf_s(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		//faces
		else if (strcmp(lineHeader, "f") == 0){
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf_s(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9){
				Log.WriteFatal("OBJ file must contain vertex positions, normals, and uvs. Triangles only!");
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
	}

	std::vector<unsigned int> indexOut;
	//Process Data
	for (unsigned int i = 0; i < vertexIndices.size(); i++)
	{
		unsigned int vertexIndex = vertexIndices[i] - 1; // -1 because obj indexing starts at 1
		unsigned int uvIndex = uvIndices[i] - 1;
		unsigned int normalIndex = normalIndices[i] - 1;

		indexOut.push_back(i);

		glm::vec3 vertex = temp_vertices[vertexIndex];
		glm::vec3 vertexNormal = temp_normals[normalIndex];
		glm::vec2 vertexUV = temp_uvs[uvIndex];

		//float tempU = vertexUV.x;
		//vertexUV.x = vertexUV.y;
		vertexUV.y = 1.0f-vertexUV.y;

		out_Verts.push_back(VertexComplete(vertex, vertexNormal, vertexUV, glm::vec4(1)));
	}

	Mesh* newMesh = new Mesh(out_Verts, indexOut);
	outMeshes.push_back(newMesh);

	if (!loadSeperateMeshes)
	{
		this->m_LoadedMeshes[filePath] = newMesh;
	}
	else
	{
		this->m_LoadedMeshes[curName] = newMesh;
	}

	return outMeshes;
}

Model* ModelLoader::CreateModel(ModelName name)
{
	return new Model(this->CreateMesh(name));
}

const char* ModelLoader::GetModelPath(ModelName model)
{
	switch (model)
	{
	case NUM_MODELS:
		Log.WriteFatal("No models loaded!");
		break;
	case BOAT:
		return "models/boat.obj";
		break;
	case OCTOPUS:
		return "models/Octopus.obj";
		break;
	case SHIP:
		return "models/ship.obj";
		break;
	case AXE:
		return "models/axe.obj";
		break;
	case EYE:
		return "models/eye.obj";
		break;
	case HOLE:
		return "models/brokenhole.obj";
		break;
  case WATER:
    return "models/water.obj";
    break;
	default:
		Log.WriteFatal("Bad ModelName enum: %i", model);
		assert(0);
		break;
	}
	return "";
}


Mesh* ModelLoader::GetMesh(ModelName name)
{
	return this->m_LoadedMeshes[this->GetModelPath(name)];
}

const Mesh* ModelLoader::GetMesh(std::string name)
{
	return this->m_LoadedMeshes[name];
}

Mesh* ModelLoader::CreateMesh(ModelName model)
{
	Mesh* outMesh = this->m_LoadedMeshes[this->GetModelPath(model)]; //check map for loaded mesh data

	if (outMesh == NULL) //hasn't been loaded before
	{
		outMesh = LoadObj(this->GetModelPath(model))[0]; // load from file
	}

	if (m_LastLoadedMesh != 0)
	{
		delete m_LastLoadedMesh;
	}

	this->m_LastLoadedMesh = new Model(new Mesh(*outMesh));

	return new Mesh(*outMesh);
}

void ModelLoader::LoadAllModels()
{
	Mesh* mesh;
	for (int i = 0; i < ModelLoader::ModelName::NUM_MODELS; ++i)
	{
		mesh = LoadObj(this->GetModelPath((ModelLoader::ModelName)i))[0];
		this->m_LoadedMeshes[this->GetModelPath((ModelLoader::ModelName)i)] = mesh;
	}
}

void ModelLoader::DEBUGRenderLastLoadedModel()
{
	if (this->m_LastLoadedMesh == 0)
		return;

	this->m_Accumulator += 0.01f;
	glm::mat4 mod;
	mod = glm::rotate(glm::mat4(1), this->m_Accumulator, glm::vec3(0, 1, 0));
	this->m_LastLoadedMesh->SetModelMatrix(mod);
	m_LastLoadedMesh->SetColor(glm::vec4(0.7f));
	this->m_LastLoadedMesh->Render();
}

void ModelLoader::DebugTrigger()
{
	this->LoadObj(this->GetModelPath(ModelName::BOAT), true);
}

std::vector<std::pair<std::string, const Mesh*>> ModelLoader::GetModelAsSeperateMeshes(ModelName name)
{
	std::vector<std::pair<std::string, const Mesh*>> outVec;

	LoadSubMeshes(name);

	for (auto i : m_SubMeshNames[this->GetModelPath(name)])
	{
		outVec.push_back(std::pair<std::string, const Mesh*>(i, this->GetMesh(i)));
	}

	return outVec;
}

Mesh* ModelLoader::GetSubMeshOfModel(ModelName name, std::string subMeshName)
{
	auto subMeshes = this->GetModelAsSeperateMeshes(name);

	// LAMBDAS: [captures](params)->returnType {}
	auto strCompareLambda = [](const std::pair<std::string, const Mesh*>& pair, std::string sMeshName)->bool { return (pair.first.compare(sMeshName) == 0); };

	//nested lambda, performs the above strCmp lambda for every element in subMeshes vector
	auto pair = std::find_if(subMeshes.begin(), subMeshes.end(), [=](const std::pair<std::string, const Mesh*>& pair)->bool { return strCompareLambda(pair, subMeshName); });

	if (pair != subMeshes.end())
	{
		return new Mesh(*pair[0].second);
	}
	
	printf("Sub mesh not found.");
	assert(0);
	return 0;
}

inline void ModelLoader::LoadSubMeshes(ModelName name)
{
	//Load if not already loaded
	if (m_SubMeshNames[this->GetModelPath(name)].size() == 0)
	{
		this->LoadObj(this->GetModelPath(name), true);
	}
}