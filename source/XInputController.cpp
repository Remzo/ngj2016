#include "pch.h"

#include "XInputController.h"
//reference: http://www.codeproject.com/Articles/26949/Xbox-Controller-Input-in-C-with-XInput

XInputController::XInputController(int playerID)
{
	// Set the Controller Number
	m_ControllerID = playerID;
}

XInputController::~XInputController()
{
}

XINPUT_STATE XInputController::GetState()
{
	// Zeroise the state
	ZeroMemory(&m_ControllerState, sizeof(XINPUT_STATE));

	// Get the state
	XInputGetState(m_ControllerID, &m_ControllerState);

	return m_ControllerState;
}

bool XInputController::IsConnected()
{
	// Zeroise the state
	ZeroMemory(&m_ControllerState, sizeof(XINPUT_STATE));

	// Get the state
	DWORD Result = XInputGetState(m_ControllerID, &m_ControllerState);

	if (Result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void XInputController::Vibrate(int leftVal, int rightVal)
{
	// Create a Vibraton State
	XINPUT_VIBRATION Vibration;

	// Zeroise the Vibration
	ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));

	// Set the Vibration Values
	Vibration.wLeftMotorSpeed = leftVal;
	Vibration.wRightMotorSpeed = rightVal;

	// Vibrate the controller
	XInputSetState(m_ControllerID, &Vibration);
}