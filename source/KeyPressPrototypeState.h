#pragma once
#include "State.h"
#include <memory>

class Tentacle;

class KeyPressPrototypeState: public State
{
public:

	KeyPressPrototypeState(float halfHeight, float halfWidth, StateManager* stateManager);
	~KeyPressPrototypeState();

	void Tick(float a_DT);
	void Render();

  void PhysicsTick(float a_DT);

private:
  std::unique_ptr<Tentacle> testTentacle;

	void HandleInput();
};
