#pragma once

#include "sphere.h"

class Collider;
class Octopus;
class Axe : public Sphere
{
public:
	Axe(float radius);
	~Axe();

	void Render();
	void Tick(float a_DT);

	void SetAttacking(bool attacking);
	void SetCharging(bool charging);
	void SetOcto(Octopus* oct);

	Octopus* m_Octopus;

private:
	Collider* m_Collider;
	Model* m_Model;

	bool b_TentacleHit = false;
	bool b_Attacking = false;
	bool b_Charging = false;

	void InitCollider();

};