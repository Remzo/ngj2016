#include "pch.h"

#include "texture.h"
#include "BaseParticle.h"
#include "ParticleSystem.h"

ParticleSystem::ParticleSystem(Texture* tex, int numParticles, float lifetime)
{
	m_Texture = tex;

	m_NumParticles = numParticles;

	this->m_ParticlePool = new BaseParticle[m_NumParticles];

	for (int i = 0; i < m_NumParticles; i++)
	{
		m_ParticlePool[i].SetTexture(tex);
		m_ParticlePool[i].SetLifetime(lifetime);
		m_ParticlePool[i].SetVelocity(glm::vec3((rand()/INT_MAX)*2.0f - 1.0f, 1.0f, 0.0f));
		//m_ParticlePool[i].SetParent(this);
	}
}

ParticleSystem::~ParticleSystem()
{
	delete[] m_ParticlePool;
	delete m_Texture;
}

void ParticleSystem::Tick(float a_DT)
{
	for (int i = 0; i < m_NumParticles; i++)
	{
		m_ParticlePool[i].Update(a_DT);
	}
}

void ParticleSystem::Render()
{
	for (int i = 0; i < m_NumParticles; i++)
	{
		m_ParticlePool[i].Render();
	}
}


void ParticleSystem::Emit(int numToEmit)
{
	for (int i = 0; i < numToEmit; i++)
	{
		m_ParticlePool[m_CurrentIndex].SetActive(true);
		m_ParticlePool[m_CurrentIndex].SetPosition(m_Position);
		m_ParticlePool[m_CurrentIndex].SetScale(this->GetScale());
		m_CurrentIndex++;
		m_CurrentIndex %= m_NumParticles;
	}
}