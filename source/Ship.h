#pragma once
#include "GameObject.h"
class HoleModel;
class Model;
class Hole : public GameObject
{
public:
	Hole(glm::vec3 _startpos);
	~Hole();
	HoleModel* m_HoleModel;
	bool isOpen;
	void Render();

};

class Ship: public GameObject
{
public:
	Ship();
	~Ship();

	std::vector <Hole*> holes;

	void UpdateDamage(float _dt);
	void Tick(float _dt);
	//waterlevel
	float health;


	void Render();

private :
	Model* m_Model;

};
