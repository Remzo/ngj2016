#include "pch.h"

#include "Collider.h"
#include "Mesh.h"

Collider::Collider(glm::vec3 halfScales, CollisionHandler::CollisionTypes colType) : c_Dimensions(halfScales*2.0f)
{
	this->m_ColliderType = colType;

	std::vector<glm::vec3> verts;
	std::vector<unsigned int> indices;
	std::vector<glm::vec3> normals;
	int faceCount = 0;

	//top
	verts.push_back(glm::vec3(-halfScales.x, halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(-halfScales.x, halfScales.y, halfScales.z));

	//normals
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(glm::vec3(0.0f, 1.0, 0.0f));
	}

	//indices
	indices.push_back(0 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	indices.push_back(1 + faceCount * 4);

	indices.push_back(0 + faceCount * 4);
	indices.push_back(3 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	faceCount++;

	//bottom
	verts.push_back(glm::vec3(-halfScales.x, -halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, -halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, -halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(-halfScales.x, -halfScales.y, -halfScales.z));

	//normals
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(glm::vec3(0.0f, -1.0, 0.0f));
	}

	//indices
	indices.push_back(0 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	indices.push_back(1 + faceCount * 4);

	indices.push_back(0 + faceCount * 4);
	indices.push_back(3 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	faceCount++;


	//right
	verts.push_back(glm::vec3(halfScales.x, halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, -halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, -halfScales.y, halfScales.z));

	//normals
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(glm::vec3(1.0f, 0.0, 0.0f));
	}

	//indices
	indices.push_back(0 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	indices.push_back(1 + faceCount * 4);

	indices.push_back(0 + faceCount * 4);
	indices.push_back(3 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	faceCount++;

	//left
	verts.push_back(glm::vec3(-halfScales.x, halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(-halfScales.x, halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(-halfScales.x, -halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(-halfScales.x, -halfScales.y, -halfScales.z));

	//normals
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(glm::vec3(-1.0f, 0.0, 0.0f));
	}

	//indices
	indices.push_back(0 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	indices.push_back(1 + faceCount * 4);

	indices.push_back(0 + faceCount * 4);
	indices.push_back(3 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	faceCount++;

	//front
	verts.push_back(glm::vec3(-halfScales.x, halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, -halfScales.y, halfScales.z));
	verts.push_back(glm::vec3(-halfScales.x, -halfScales.y, halfScales.z));

	//normals
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(glm::vec3(0.0f, 0.0, 1.0f));
	}

	//indices
	indices.push_back(0 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	indices.push_back(1 + faceCount * 4);

	indices.push_back(0 + faceCount * 4);
	indices.push_back(3 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	faceCount++;

	//back
	verts.push_back(glm::vec3(halfScales.x, halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(-halfScales.x, halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(-halfScales.x, -halfScales.y, -halfScales.z));
	verts.push_back(glm::vec3(halfScales.x, -halfScales.y, -halfScales.z));

	//normals
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(glm::vec3(0.0f, 0.0, -1.0f));
	}

	//indices
	indices.push_back(0 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	indices.push_back(1 + faceCount * 4);

	indices.push_back(0 + faceCount * 4);
	indices.push_back(3 + faceCount * 4);
	indices.push_back(2 + faceCount * 4);
	faceCount++;

	this->m_FaceNormals.push_back(this->RightVec());
	this->m_FaceNormals.push_back(this->UpVec());
	this->m_FaceNormals.push_back(this->ForwardVec());

	//this->m_FaceNormals.push_back(glm::vec3(0.0f, 1.0f, 0.0f)); //top
	//this->m_FaceNormals.push_back(glm::vec3(0.0f, -1.0f, 0.0f)); //bottom
	//this->m_FaceNormals.push_back(glm::vec3(1.0f, 0.0f, 0.0f)); //right
	//this->m_FaceNormals.push_back(glm::vec3(-1.0f, 0.0f, 0.0f)); //left
	//this->m_FaceNormals.push_back(glm::vec3(0.0f, 0.0f, 1.0f)); //forward
	//this->m_FaceNormals.push_back(glm::vec3(0.0f, 0.0f, -1.0f)); //back

	this->SetGLRenderMode(GL_TRIANGLES);
	this->SetPoints(verts, normals, indices);
	this->SetColor(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));

	//register with collisionHandler
	//g_CollisionManager.RegisterCollider(this);
}

Collider::~Collider()
{
	m_Points.clear();
}

glm::mat3 Collider::GetInertialTensor() const
{
	//reference https://en.wikipedia.org/wiki/List_of_moments_of_inertia //tensor of cuboid
	float Iheight = (1.0f / 12.0f) * (powf(c_Dimensions.y, 2.0f) + powf(c_Dimensions.z, 2.0f));
	float Iwidth = (1.0f / 12.0f)* (powf(c_Dimensions.x, 2.0f) + powf(c_Dimensions.z, 2.0f));
	float Idepth = (1.0f / 12.0f)* (powf(c_Dimensions.z, 2.0f) + powf(c_Dimensions.x, 2.0f));

	glm::mat3 outTensor = glm::mat3(1);
	outTensor[0][0] = Iwidth;
	outTensor[1][1] = Iheight;
	outTensor[2][2] = Idepth;

	outTensor *= this->GetMass();

	return outTensor;
}

void Collider::SetPoints(std::vector<glm::vec3> points, float scale)
{
	std::vector<VertexComplete> vertData;

	m_Points = points;

	for (auto i : points)
	{
		vertData.push_back(VertexComplete(i, glm::vec3(0, 0, 1), glm::vec2(0, 0), glm::vec4(1)));
	}

	this->SetMesh(new Mesh(vertData));

	this->SetScale(scale);
}

void Collider::SetPoints(std::vector<glm::vec3> points, std::vector<glm::vec3> normals, std::vector<unsigned int> indices, float scale)
{
	std::vector<VertexComplete> vertData;

	m_Points = points;

	for (unsigned int i = 0; i < points.size(); i++)
	{
		vertData.push_back(VertexComplete(points[i], normals[i], glm::vec2(0, 0), glm::vec4(1)));
	}

	this->SetMesh(new Mesh(vertData, indices));

	this->SetScale(scale);
}

void Collider::RescaleCollider(float scale)
{
	Log.WriteWarning("Rescale collider needs refactoring to include normals and indices");
	SetPoints(m_Points, scale);
}

std::vector<glm::vec3> Collider::GetCollisionPoints() const
{
	std::vector<glm::vec3> outPoints;

	glm::quat myRot = this->GetRotation(false);
	glm::vec3 myPos = this->GetPosition(false);

	for (auto i = m_Points.begin(); i != m_Points.end(); ++i)
	{
		outPoints.push_back((((*i) * m_Scale) * myRot) + myPos);
	}

	return outPoints;
}


std::vector<glm::vec3> Collider::GetRelativeCollisionPoints() const
{
	std::vector<glm::vec3> outPoints;

	glm::quat myRot = this->GetRotation(false);
	glm::vec3 myPos = this->GetPosition(false);

	for (auto i = m_Points.begin(); i != m_Points.end(); ++i)
	{
		outPoints.push_back((((*i) * m_Scale) * myRot));
	}

	return outPoints;
}
