#pragma once

struct Vertex
{
	Vertex(glm::vec3 pos) : m_Pos(pos){};
	glm::vec3 m_Pos;
};

struct VertexComplete
{
	VertexComplete(glm::vec3 pos, glm::vec3 normal, glm::vec2 uv, glm::vec4 color) : m_Pos(pos), m_Normal(normal), m_UV(uv), m_Color(color){};
	glm::vec3 m_Normal, m_Pos;
	glm::vec2 m_UV;
	glm::vec4 m_Color;
};

struct VertexNormal: public Vertex
{
	VertexNormal(glm::vec3 pos, glm::vec3 normal) : Vertex(pos), m_Normal(normal){};
	glm::vec3 m_Normal;
};

struct VertexXYZColor : public Vertex
{
	VertexXYZColor(glm::vec3 pos, glm::vec3 color) : Vertex(pos), m_Color(color){};
	glm::vec3 m_Color;
};

struct VertexUV : public Vertex
{
	VertexUV(glm::vec3 pos, glm::vec2 uv) : Vertex(pos), m_UV(uv){};
	glm::vec2 m_UV;
};

struct VertexUVBarycentric : public VertexUV
{
	VertexUVBarycentric(glm::vec3 pos, glm::vec2 uv, glm::vec3 bary) : VertexUV(pos, uv), m_Bary(bary){};
	glm::vec3 m_Bary; //save me barry!
};