#pragma once

#include "GameObject.h"

class Line;
class Model;
class Boat : public GameObject
{
public:
	struct Inputs{
		bool b_Forward;
		bool b_Left;
		bool b_Right;
		bool b_AimLeft;
		bool b_AimRight;
		bool b_Shoot;
		float m_HelmDelta;
		float m_AimDelta;
	};

	Boat(unsigned int colorID, int controllerID = -1);
	~Boat();

	void Update(float a_DT);
	void UpdatePhysics(float a_DT);
	void Render();
	void HandleInput(SDL_Scancode forward, SDL_Scancode right, SDL_Scancode left, SDL_Scancode shoot);

private:
	typedef GameObject super;
	const float c_RudderMaxAngle = 45.0f;
	const float c_ShootMaxAngle = 90.0f;

	void InitCollider();
	void CalculateNewForces(float a_DT);
	void CalculateBuoyancy(float a_DT);

	void InitDebugHelm();
	void DrawDebugRudder();
	void DrawDebugShootDir();
	
	void UpdateRudder(float a_DT);
	void UpdateAim(float a_DT);

	glm::vec3 GetAbsoluteVectorFromVal(float val, float maxVal, float angleOffset = 0) const;

	Inputs m_Inputs;
	//[-1,1]
	float m_RudderVal;

	Model* m_HullModel = 0;
	Model* m_SailModel = 0;

	//shooting
	//[-1,1]
	float m_ShootVal;

	//helm
	const float c_MaxHelmAngle;

	//player
	int m_ControllingPlayerID = -1;

	//thrust
	float m_ThrustValue = 0.0f;

	//buoyancy
	bool b_Submerged = false;

	//DEBUG
	Line* m_DebugVelVector;
	std::vector<float> m_DHelmAngles;

};
