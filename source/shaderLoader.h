#pragma once

enum ShaderType
{
	FLAT_UNLIT = 0,
	FLAT_CAMERA_LIT,
	SKYBOX,
	TILED,
	TEXTURED_LIT,
	TEXTURED_UNLIT,
	SCREENQUAD,
	VERTEX_COLOR,
	PP_BLUR_H,
	PP_BLUR_V,
  RAYMARCH_SDF,
  WATER
};

class ShaderLoader
{
public:
	ShaderLoader();
	~ShaderLoader();

	void LoadShaders();
	GLuint GetShader(ShaderType shader);

	GLuint LoadShader(const char * vertex_file_path, const char * fragment_file_path, const bool includeGeometryShader = false, const char* geometry_file_path = "");

private:
	std::map<ShaderType, GLuint> m_ShaderMap;
	
};

extern ShaderLoader& g_ShaderLoader;