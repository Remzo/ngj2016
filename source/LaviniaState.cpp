#include "pch.h"

#include "LaviniaState.h"
#include <algorithm>

#include "StateManager.h"
#include "Model.h"
#include "Quad.h"
#include "skybox.h"
#include "Line.h"
#include "octopus.h"
#include "Ship.h"


LaviniaState::LaviniaState(float halfHeight, float halfWidth, StateManager* stateManager) : State(stateManager)
{
	//init MainCamera
	MainCamera.Reset();
	//MainCamera.SetOrthographic(-GlobalInfo.m_HalfGameWidth, GlobalInfo.m_HalfGameWidth, -GlobalInfo.m_HalfGameHeight, GlobalInfo.m_HalfGameHeight, 0.01f, 1000.0f);
	MainCamera.SetPosition(glm::vec3(0, 0, 30));
	MainCamera.SetRotationEuler(glm::vec3(0.0f, 0.0f, 0.0f));

	m_Water = new Quad();
	m_Water->RotateEuler(0, glm::vec3(1.0f, 0.0f, 0.0f));
	m_Water->SetScale(100.0f);
	m_Water->SetColor(glm::vec4(0.0f, 0.0f, 1.0f, 0.3f));
	m_Water->SetShaderType(ShaderType::FLAT_CAMERA_LIT);

	



	//init skybox
	this->m_Skybox = new Skybox(g_ShaderLoader.GetShader(ShaderType::SKYBOX), "textures/cubemap/greenNeb_left.png", "textures/cubemap/greenNeb_right.png", "textures/cubemap/greenNeb_top.png", "textures/cubemap/greenNeb_bottom.png", "textures/cubemap/greenNeb_front.png", "textures/cubemap/greenNeb_back.png");
	
	Input.Initialize();
	//Octopus
	octopus = new Octopus();
	octopus->SetControllerID(0);
	//ship
	ship = new Ship();
	octopus->SetShipRef(ship);

	


}

LaviniaState::~LaviniaState()
{
	if (octopus) delete octopus; octopus = 0;
	if (ship) delete ship; ship = 0;
	delete m_Water;
	delete m_Skybox;
}

void LaviniaState::Render()
{
	//clear
	glm::vec3 clearColor = glm::vec3(1.0f, 0.0f, 1.0f);
	glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	//skybox
	m_Skybox->Draw();

	//water
	m_Water->Render();

	//ship
	ship->Render();
	//Tentacletip
	octopus->Render();
	
}

void LaviniaState::PhysicsTick(float a_DT)
{
  octopus->PhysicsTick(a_DT);
}

void LaviniaState::Tick(float a_DT)
{
	HandleInput(a_DT);
	octopus->Tick(a_DT);
	ship->Tick(a_DT);

}

void LaviniaState::HandleInput(float a_DT)
{
	
}
