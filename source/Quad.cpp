#include "pch.h"

#include "Quad.h"
#include "Mesh.h"

Quad::Quad() : Model()
{
	float x = 0.5f;

	//topLeft
	m_Vertices.push_back(VertexComplete(glm::vec3(-x, x, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(0, 0), glm::vec4(1.0f)));
	//top right
	m_Vertices.push_back(VertexComplete(glm::vec3(x, x, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(1, 0), glm::vec4(1.0f)));
	//bottom right
	m_Vertices.push_back(VertexComplete(glm::vec3(x, -x, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(1, 1), glm::vec4(1.0f)));
	//bottom left
	m_Vertices.push_back(VertexComplete(glm::vec3(-x, -x, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(0, 1), glm::vec4(1.0f)));

	//Indices
	m_Indices.push_back(0);
	m_Indices.push_back(2);
	m_Indices.push_back(1);

	m_Indices.push_back(0);
	m_Indices.push_back(3);
	m_Indices.push_back(2);

	SetMesh(new Mesh(m_Vertices, m_Indices));
}

Quad::~Quad()
{
}