#include "pch.h"

#include "sphere.h"
#include "Mesh.h"

Sphere::Sphere(float radius)
{
	m_Radius = radius;
	GenerateMesh();
}

Sphere::~Sphere()
{

}

void Sphere::GenerateMesh()
{
	//Vertices/Indices

	const int stacks = 32;
	const int slices = 32;
	const unsigned int numIndices = (slices * stacks + slices) * 6;

	std::vector<VertexComplete> verts;
	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> textureCoords;
	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;

	for (int i = 0; i <= stacks; ++i)
	{
		// V texture coordinate.
		float V = i / (float)stacks;
		float phi = V * glm::pi<float>();

		for (int j = 0; j <= slices; ++j)
		{
			// U texture coordinate.
			float U = j / (float)slices;
			float theta = U * glm::pi<float>()*2.0f;

			float X = cos(theta) * sin(phi);
			float Y = cos(phi);
			float Z = sin(theta) * sin(phi);

			glm::vec3 vertPos = glm::vec3(X, Y, Z) * m_Radius;
			positions.push_back(vertPos);

			glm::vec3 normal = glm::vec3(X, Y, Z);
			normals.push_back(normal);

			glm::vec2 UV = glm::vec2(1.0f - U, V);
			textureCoords.push_back(UV);

			verts.push_back(VertexComplete(vertPos, normal, UV, glm::vec4(1.0f)));

			//tangent
			glm::vec3 tangent = glm::cross(glm::vec3(0, 1, 0), normal);
			glm::vec3 bitangent = glm::cross(normal, tangent);

			tangents.push_back(tangent);
			bitangents.push_back(bitangent);

		}
	}

	// Now generate the index buffer
	std::vector<GLuint> indices;

	for (int i = 0; i < slices * stacks + slices; ++i)
	{
		indices.push_back(i);
		indices.push_back(i + slices + 1);
		indices.push_back(i + slices);

		indices.push_back(i + slices + 1);
		indices.push_back(i);
		indices.push_back(i + 1);
	}


	SetMesh(new Mesh(verts, indices));

}