#pragma once

#include "State.h"
#include <unordered_map>

class StateManager;
class Player;
class Skybox;
class BaseParticle;
class ParticleSystem;
class Octopus;
class Ship;
class BucketPrototypeState : public State
{
public:
	BucketPrototypeState(float halfHeight, float halfWidth, StateManager* stateManager);
	~BucketPrototypeState();

private:
	void Tick(float a_DT);
	void PhysicsTick(float a_DT);
	void Render();

	void HandleInput(float a_DT);

	Skybox* m_Skybox;

	Player* m_Player;
	ParticleSystem* m_PS;

  Model* m_Water;

	Octopus* m_Octopus;
	Ship* m_Ship;


};