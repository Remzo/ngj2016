#pragma once

class State;
class SoundSystem;
class Texture;
class StateManager
{
public:
	enum States
	{
		MENU = 0,
		PLAY,
		DEBUG,
    KEY_PRESS_PROTOTYPE,
		CHARACTER_MOVE,
		LAVINIASTATE =5
	};

	StateManager();
	~StateManager();
	void Tick(float a_DT);
	void PhysicsTick(float a_DT);
	void Render();

	void SetState(States state);
	States GetState() { return m_CurState; }

private:
	void HandleInput();

	State* m_State;
	States m_CurState;

	const float m_HalfGameHeight;
	const float m_HalfGameWidth;
};