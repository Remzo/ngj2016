#include "pch.h"

#include "Collider.h"
#include "Axe.h"
#include "texture.h"
#include "Octopus.h"

Axe::Axe(float radius) : Sphere(radius)
{
	InitCollider();
	m_Model = new Model(g_ModelLoader.GetSubMeshOfModel(ModelLoader::AXE, "Cylinder_20"));
	m_Model->SetParent(this);

	this->m_Model->m_Texture = new Texture("textures/axe.png", 64, 32);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->m_Model->m_Texture->GetTextureID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	m_Model->SetShaderType(ShaderType::TEXTURED_UNLIT);

}

Axe::~Axe()
{
	delete m_Collider;
}

void Axe::Render()
{
	//Model::Render();
	//m_Collider->Render();
	m_Model->Render();
}

void Axe::SetAttacking(bool attacking)
{
	b_Attacking = attacking;
	if (b_Attacking)
	{
		this->SetCharging(false);
		this->SetColor(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
	}
	else
	{
		b_TentacleHit = false;
		this->SetColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	}
}

void Axe::SetCharging(bool charging)
{
	b_Charging = charging;
	if (b_Charging)
	{
		this->SetColor(glm::vec4(0.4f, 0.4f, 0.0f, 1.0f));
	}
	else
	{
		this->SetColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	}
}

void Axe::SetOcto(Octopus* octo)
{
	this->m_Octopus = octo;
}

void Axe::Tick(float a_DT)
{
	m_Collider->Update(a_DT); 

	if (b_Attacking && !b_TentacleHit)
	{
		TentacleTip* closestTentacle = NULL;
		float distanceToTentacle = m_Octopus->GetDistance(this->GetPosition(), &closestTentacle);
		Log.WriteDebug("AxeDist: %f", distanceToTentacle);
		if (distanceToTentacle <= 3.0f)
		{
			closestTentacle->tentacle->ResetPoints();
			b_TentacleHit = true;
		}
	}
}

void Axe::InitCollider()
{
	const float xScale = 1.0f;
	const float yScale = 1.0f;
	const float zScale = 1.0f;

	const float halfX = 0.5f;
	const float halfY = 0.5f;
	const float halfZ = 0.5f;

	this->m_Collider = new Collider(glm::vec3(halfX, halfY, halfZ), CollisionHandler::CollisionTypes::DYNAMIC);
	this->m_Collider->SetParent(this);

}