#pragma once

#include "Object.h"

class Mesh;
class Model : public Object
{
public:
	Model();
	Model(Mesh* mesh, const ShaderType shaderType = ShaderType::FLAT_UNLIT);
	~Model();

	virtual void Update(float a_DT);
	virtual void Render();
	void SetMesh(Mesh* mesh);
	virtual void SetColor(glm::vec4 color) { m_Color = color; }
	glm::vec4 GetColor() { return m_Color; };
	void SetShaderType(ShaderType shader);
	void SetGLRenderMode(GLenum renderMode);
	Mesh* GetMesh() { return m_Mesh; }
	Texture* m_Texture;

	void SetColorInterpolation(bool on, float transitionTime = 1.0f);
	glm::vec4 GetDefaultColor() { return m_DefaultColor; }
	void SetDefaultColor(glm::vec4);
	void SetTargetColor(glm::vec4 color);

protected:
  float m_TotalTime = 0;
	ShaderType m_CurShaderType;
	GLuint m_shaderID;
	GLuint m_VAO;
	GLenum m_RenderMode;


	glm::vec4 m_Color;

	Mesh* m_Mesh;

	bool b_InterpolateColor = false;
	glm::vec4 m_DefaultColor;
	bool b_ColorChanging = false;
	float m_ColorTimer = 0.0f;
	float m_ColorTransitionTime = 1.0f;
	glm::vec4 m_TargetColor;
	glm::vec4 m_ColorDelta;

private:
	bool b_Initialised = false;
	void Init();
	void CleanUp();
};