#include "pch.h"
#include "FMOD/Windows/inc/fmod.hpp"
#include "FMOD/Windows/inc/fmod_errors.h"
#include <stdio.h>
#include "SoundSystem.h"

SoundSystem& g_SoundSystem = SoundSystem();

void ERRCHECK(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		//  exit(-1);
	}
}

SoundSystem::SoundSystem()
{
	system = 0;
	soundbg = soundOctoAttack = soundVikingMakeHole = soundVikingCharge = soundOctoCoverHole = 0;
	channelbg = channeleffects = channelGameover = 0;
	channelbg->setVolume(0.4f);
	result = FMOD_RESULT::FMOD_ERR_CDDA_INIT;
	maxchannels = 0;
	triggeredMainBgLoop = false;
	startedbg = false;
}

void SoundSystem::Init()
{
	unsigned int version = 0;
	result = FMOD::System_Create(&system);
	ERRCHECK(result);

	result = system->getVersion(&version);
	ERRCHECK(result);

	if (version < FMOD_VERSION)
	{
		printf("Error!  You are using an old version of FMOD %08x.  This program requires %08x\n", version, FMOD_VERSION);

	}

	result = system->init(32, FMOD_INIT_NORMAL, 0);
	ERRCHECK(result);
}
void SoundSystem::LoadSounds()
{
	result = system->createSound("sounds/viking_Loop.wav", FMOD_LOOP_NORMAL, 0, &soundbg);
	ERRCHECK(result);

	result = system->createSound("sounds/viking_Start.wav", FMOD_HARDWARE, 0, &soundbgIntro);
	ERRCHECK(result);

	result = system->createSound("sounds/stab.wav", FMOD_HARDWARE, 0, &soundOctoAttack);
	ERRCHECK(result);

	result = system->createSound("sounds/plug_Hole.wav", FMOD_HARDWARE, 0, &soundOctoCoverHole);
	ERRCHECK(result);

	result = system->createSound("sounds/wood_Short.wav", FMOD_HARDWARE, 0, &soundVikingMakeHole);
	ERRCHECK(result);

	result = system->createSound("sounds/attack_Heavy.wav", FMOD_HARDWARE, 0, &soundVikingCharge);
	ERRCHECK(result);
	result = system->createSound("sounds/attack_Light.wav", FMOD_HARDWARE, 0, &soundvikingAttack);
	ERRCHECK(result);
}
bool SoundSystem::IsPlaying(FMOD::Channel* _c)
{
	bool bIsPlaying = false;
	FMOD_RESULT fRes = _c->isPlaying(&bIsPlaying);
	if (fRes == FMOD_ERR_INVALID_HANDLE || fRes == FMOD_ERR_CHANNEL_STOLEN)
	{
		ERRCHECK(fRes);
		return false; // Expected error

	}
	return bIsPlaying;
}
void SoundSystem::PlayBgSound()
{
	if (!startedbg)
	{
		result = system->playSound(FMOD_CHANNEL_FREE, soundbgIntro, false, &channelbg);
		ERRCHECK(result);
		startedbg = true;
	}
	if (startedbg && !IsPlaying(channelbg) && !triggeredMainBgLoop)
	{
		result = system->playSound(FMOD_CHANNEL_FREE, soundbg, false, &channelbg);
		ERRCHECK(result);
		triggeredMainBgLoop = true;
	}

}

void SoundSystem::StopBgSound()
{
	channelbg->stop();
	triggeredMainBgLoop = false;
	startedbg = false;
}

void SoundSystem::PlaySound(FMOD::Sound* s)
{
	result = system->playSound(FMOD_CHANNEL_FREE, s, false, &channeleffects);
	ERRCHECK(result);
}
void SoundSystem::Update()
{
	system->update();
}

void SoundSystem::Terminate()
{
	result = soundbg->release();
	ERRCHECK(result);
	result = soundbgIntro->release();
	ERRCHECK(result);
	result = soundOctoAttack->release();
	ERRCHECK(result);
	result = soundOctoCoverHole->release();
	ERRCHECK(result);
	result = soundVikingMakeHole->release();
	ERRCHECK(result);
	result = soundVikingCharge->release();
	ERRCHECK(result);
	result = soundvikingAttack->release();
	ERRCHECK(result);

	result = system->close();
	ERRCHECK(result);
	result = system->release();
	ERRCHECK(result);

}