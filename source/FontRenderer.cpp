#include "pch.h"

#include "Model.h"
#include "Mesh.h"
#include "texture.h"
#include "FrameBufferObject.h"
#include "FontRenderer.h"

FontRenderer &g_FontRenderer = FontRenderer();

#define A_UV_POS glm::vec2(0.03125f, 0.5f)
#define a_UV_POS glm::vec2(0.03125f, 0.75f)
#define ZERO_UV_POS glm::vec2(0.5f, 0.25f)
#define CHAR_UV_DIMENSIONS glm::vec2(0.03123f, 0.25f)

FontRenderer::FontRenderer()
{
}

void FontRenderer::Init()
{
	if (initialised)
	{
		return;
	}
	initialised = true;

	this->m_FontTexture = new Texture("textures/font.png", 512, 96);

	const float letterHalfWidth = 0.5f;
	const float letterHalfHeight = 0.5f;

	glm::vec2 charXOffset = CHAR_UV_DIMENSIONS;
	charXOffset.y = 0.0f;
	glm::vec2 charYOffset = CHAR_UV_DIMENSIONS;
	charYOffset.x = 0.0f;
	
	float charNum = (float)0;

	glm::vec2 charPos = A_UV_POS + (charNum*charXOffset);

	m_Indices.push_back(0);
	m_Indices.push_back(2);
	m_Indices.push_back(1);

	m_Indices.push_back(0);
	m_Indices.push_back(3);
	m_Indices.push_back(2);

	//letter quad
	this->UpdateUVs(this->GetUVFromChar('A')); //initialise m_Vertices with UVs for 'A'

	this->m_LetterQuad = new Model(new Mesh(m_Vertices, m_Indices), ShaderType::TEXTURED_UNLIT);

}

void FontRenderer::RenderCharacter()
{
	this->m_LetterQuad->Render();
	this->m_LetterQuad->Translate(glm::vec3(1.0f, 0.0f, 0.0f)*m_LetterQuad->GetScale().x);
}

void FontRenderer::RenderCharacter(const char character, glm::vec3 pos, glm::vec3 color, float scale)
{
	this->m_LetterQuad->SetColor(glm::vec4(color, 1.0f));
	this->m_LetterQuad->SetScale(scale);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->m_FontTexture->GetTextureID());

	this->m_LetterQuad->SetPosition(pos);

	const char curChar = character;

	this->UpdateUVs(this->GetUVFromChar(curChar));
	this->m_LetterQuad->SetMesh(new Mesh(m_Vertices, m_Indices));
	RenderCharacter();

	glBindTexture(GL_TEXTURE_2D, 0);

}

//Punctuation unsupported!
void FontRenderer::RenderString(const char* string, glm::vec3 pos, glm::vec3 color, float scale)
{
	if (strcmp(string, "") == 0)
	{
		return;
	}
	this->m_LetterQuad->SetColor(glm::vec4(color, 1.0f));
	this->m_LetterQuad->SetScale(scale);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->m_FontTexture->GetTextureID());

	this->m_LetterQuad->SetPosition(pos);

	const char* curChar = string;

	int charCount = strlen(string);

	for (int i = 0; i < charCount; i++)
	{
		this->UpdateUVs(this->GetUVFromChar(*curChar));
		this->m_LetterQuad->SetMesh(new Mesh(m_Vertices, m_Indices));
		RenderCharacter();
		curChar++;
	}
	glBindTexture(GL_TEXTURE_2D, 0);
}

glm::vec2 FontRenderer::GetUVFromChar(char a)
{
	glm::vec2 outUV;
	/* A = 65, a = 97, 0 = 48 */

	float charNum;

	glm::vec2 charXOffset = CHAR_UV_DIMENSIONS;
	charXOffset.y = 0.0f;

	if (a == 32 || (a <= 90 && a >= 65)) // uppercase and space
	{
		charNum = (float)((int)a % 65);
		outUV = A_UV_POS + (charNum*charXOffset);
	}
	else if (a <= 132 && a >= 97) //lowercase
	{
		a -= 32;
		charNum = (float)((int)a % 65);
		outUV = a_UV_POS + (charNum*charXOffset);
	}
	else //number
	{
		charNum = (float)(a-48);
		outUV = ZERO_UV_POS + (charNum*charXOffset);
	}

	return outUV;
}

void FontRenderer::UpdateUVs(glm::vec2 charPos)
{
	const float letterHalfWidth = 0.5f;
	const float letterHalfHeight = 0.5f;

	glm::vec2 charXOffset = CHAR_UV_DIMENSIONS;
	charXOffset.y = 0.0f;
	glm::vec2 charYOffset = CHAR_UV_DIMENSIONS;
	charYOffset.x = 0.0f;

	m_Vertices.clear();

	//topLeft
	m_Vertices.push_back(VertexComplete(glm::vec3(-letterHalfHeight, letterHalfWidth, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), charPos, glm::vec4(1.0f)));
	//top right
	m_Vertices.push_back(VertexComplete(glm::vec3(letterHalfHeight, letterHalfWidth, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), charPos + charXOffset, glm::vec4(1.0f)));
	//bottom right
	m_Vertices.push_back(VertexComplete(glm::vec3(letterHalfHeight, -letterHalfWidth, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), charPos + CHAR_UV_DIMENSIONS, glm::vec4(1.0f)));
	//bottom left
	m_Vertices.push_back(VertexComplete(glm::vec3(-letterHalfHeight, -letterHalfWidth, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), charPos + charYOffset, glm::vec4(1.0f)));
}

FontRenderer::~FontRenderer()
{
	delete this->m_FontTexture;
	delete this->m_LetterQuad;
}