#include "pch.h"

#include "shaderLoader.h"

ShaderLoader& g_ShaderLoader = ShaderLoader();

ShaderLoader::ShaderLoader()
{
}

ShaderLoader::~ShaderLoader()
{
}

void ShaderLoader::LoadShaders()
{
	Log.WriteInfo("Loading shaders");
  this->m_ShaderMap.clear();
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::FLAT_UNLIT, this->LoadShader("shaders/flatColorVert.glsl", "shaders/flatColorFrag.glsl")));
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::FLAT_CAMERA_LIT, this->LoadShader("shaders/cameraLitColorVert.glsl", "shaders/cameraLitColorFrag.glsl")));
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::SKYBOX, this->LoadShader("shaders/skyboxVert.glsl", "shaders/skyboxFrag.glsl")));
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::TEXTURED_LIT, this->LoadShader("shaders/textureLitVert.glsl", "shaders/textureLitFrag.glsl")));
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::TEXTURED_UNLIT, this->LoadShader("shaders/textureUnlitVert.glsl", "shaders/textureUnlitFrag.glsl")));
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::SCREENQUAD, this->LoadShader("shaders/screenQuadVert.glsl", "shaders/screenQuadFrag.glsl" , true, "shaders/screenQuadGeom.glsl")));
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::VERTEX_COLOR, this->LoadShader("shaders/vertexColorVert.glsl", "shaders/vertexColorFrag.glsl")));
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::PP_BLUR_H, this->LoadShader("shaders/screenQuadVert.glsl", "shaders/blurFrag.glsl", true, "shaders/screenQuadGeom.glsl")));
	this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::PP_BLUR_V, this->LoadShader("shaders/screenQuadVert.glsl", "shaders/blurFrag.glsl", true, "shaders/screenQuadGeom.glsl")));
  this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::RAYMARCH_SDF, this->LoadShader("shaders/raymarchSdfVert.glsl", "shaders/raymarchSdfFrag.glsl")));
  this->m_ShaderMap.insert(std::pair<ShaderType, GLuint>(ShaderType::WATER, this->LoadShader("shaders/waterVert.glsl", "shaders/waterFrag.glsl")));
}

GLuint ShaderLoader::GetShader(ShaderType shader)
{
	return this->m_ShaderMap[shader];
}

GLuint ShaderLoader::LoadShader(const char * vertex_file_path, const char * fragment_file_path, const bool includeGeometryShader /* = false*/, const char* geometry_file_path /* = ""*/)
{

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint GeometryShaderID = glCreateShader(GL_GEOMETRY_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open())
	{
		std::string Line = "";
		while (getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}
	else
	{
		Log.WriteWarning("Invalid Vertex Shader Path: %s", vertex_file_path);
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open()){
		std::string Line = "";
		while (getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}
	else
	{
		Log.WriteWarning("Invalid Fragment Shader Path: %s", fragment_file_path);
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	Log.WriteInfo("Compiling shader : %s", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> VertexShaderErrorMessage(InfoLogLength);
	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	if (!Result && VertexShaderErrorMessage.size() != 1)
	{
		Log.WriteWarning("%s", &VertexShaderErrorMessage[0]);
	}

	// Compile Fragment Shader
	Log.WriteInfo("Compiling shader : %s", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
	if (!Result && FragmentShaderErrorMessage.size() != 1)
	{
		Log.WriteWarning("%s", &FragmentShaderErrorMessage[0]);
	}
	
	if (includeGeometryShader)
	{
		// Read the Geometry Shader code from the file
		std::string GeometryShaderCode;
		std::ifstream GeometryShaderStream(geometry_file_path, std::ios::in);
		if (GeometryShaderStream.is_open()){
			std::string Line = "";
			while (getline(GeometryShaderStream, Line))
				GeometryShaderCode += "\n" + Line;
			GeometryShaderStream.close();
		}
		else
		{
			Log.WriteWarning("Invalid Geometry Shader Path: %s", geometry_file_path);
		}

		// Compile Geometry Shader
		Log.WriteInfo("Compiling shader : %s", geometry_file_path);
		char const * GeometrySourcePointer = GeometryShaderCode.c_str();
		glShaderSource(GeometryShaderID, 1, &GeometrySourcePointer, NULL);
		glCompileShader(GeometryShaderID);

		// Check Geometry Shader
		glGetShaderiv(GeometryShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(GeometryShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		std::vector<char> GeometryShaderErrorMessage(InfoLogLength);
		glGetShaderInfoLog(GeometryShaderID, InfoLogLength, NULL, &GeometryShaderErrorMessage[0]);
		if (!Result && GeometryShaderErrorMessage.size() != 1)
		{
			Log.WriteWarning("%s", &GeometryShaderErrorMessage[0]);
		}
	}



	// Link the program
	Log.WriteDebug("Linking program");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	if (includeGeometryShader)
	{
		glAttachShader(ProgramID, GeometryShaderID);
	}
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage(InfoLogLength, int(1));
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	if (!Result && ProgramErrorMessage.size() != 1)
	{
		Log.WriteWarning("%s", &ProgramErrorMessage[0]);
	}
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);
	glDeleteShader(GeometryShaderID);

	return ProgramID;
}