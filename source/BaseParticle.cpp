#include "pch.h"

#include "texture.h"
#include "BaseParticle.h"

BaseParticle::BaseParticle(Texture* tex) : Quad()
{
	m_Texture = tex;
	this->SetShaderType(ShaderType::TEXTURED_UNLIT);
	m_Velocity = glm::vec3(0);
}

BaseParticle::BaseParticle() : Quad()
{
	this->SetShaderType(ShaderType::TEXTURED_UNLIT);
	//this->RotateEuler(90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
}

void BaseParticle::SetTexture(Texture* tex)
{
	m_Texture = tex;
}

BaseParticle::~BaseParticle()
{
	Quad::~Quad();
}

void BaseParticle::Update(float a_DT)
{
	if (!b_Active)
	{
		return;
	}

	//front facing ish
	//this->SetRotation(glm::quat(glm::lookAt(this->GetPosition() , this->GetPosition() + glm::normalize(MainCamera.GetPosition()- this->GetPosition()), glm::vec3(0.0f, 0.0f, 1.0f))));
	//this->SetRotation(glm::inverse(MainCamera.GetRotation()));

	//position
	this->SetPosition(m_Position + m_Velocity * a_DT);

	m_Timer += a_DT;
	if (m_Timer >= m_Lifetime)
	{
		b_Active = false;
	}
}

void BaseParticle::SetVelocity(glm::vec3 vel)
{
	this->m_Velocity = vel;
}

void BaseParticle::Render()
{
	if (!b_Active)
		return;

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Texture->GetTextureID());
	Quad::Render();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void BaseParticle::SetLifetime(float lifetime)
{
	this->m_Lifetime = lifetime;
}

void BaseParticle::SetActive(bool active)
{
	b_Active = active;
	m_Timer = 0.0f;
}