#pragma once

#include "Model.h"

class Texture;
class Sphere : public virtual Model
{
public:
	Sphere(float radius = 1.0f);
	~Sphere();

protected:
	std::vector<VertexComplete> m_Vertices;
	std::vector<unsigned int> m_Indices;

	float m_Radius;
private:
	void GenerateMesh();

};