#include "pch.h"

#include "XInputController.h"

InputManager& Input = InputManager();

#define CONTROLLER_STICK_DEADZONE_LENGTH 0.25f

InputManager::InputManager() :m_mouseDeltaX(0), m_mouseDeltaY(0) 
{
	ResetKeys();

	for (int i = 0; i < 4; i++)
	{
		b_ControllerConnected[i] = false;
		m_XInputControllers[i] = new XInputController(i);
		m_LeftStickLastState[i] = glm::vec2(0.0f);
		m_RightStickLastState[i] = glm::vec2(0.0f);
	}
}

InputManager::~InputManager()
{
	for (int i = 0; i < 4; i++)
	{
		if (m_XInputControllers[i] != 0)
		{
			delete m_XInputControllers[i];
		}
	}
}

void InputManager::ResetKeys()
{
	for (int i = 0; i < SDL_NUM_SCANCODES; ++i)
	{
		m_held[i] = false;
	}
}

void InputManager::Initialize()
{
	if (this->m_Initialised)
		return;

	this->m_PixelsPerXUnit = GlobalInfo.ScreenWidth / 100;
	this->m_PixelsPerYUnit = GlobalInfo.ScreenHeight / 100;
	this->m_Initialised = true;
	CheckControllersConnected();
}

bool InputManager::GetKeyDown( const SDL_Scancode& a_key ) const {
	const bool retVal = m_held[a_key] && m_heldChange[a_key];
	//m_heldChange[a_key] = false;
	return retVal;
}

bool InputManager::GetKeyUp( const SDL_Scancode& a_key ) const {
	const bool retVal = !m_held[a_key] && m_heldChange[a_key];
	//m_heldChange[a_key] = false;
	return retVal;
}

void InputManager::ResetHoldChange() {
	memset( m_heldChange, false, sizeof(m_heldChange) );
	memset( m_click, false, sizeof(m_click) );

	m_oldMouseX = m_mouseX;
	m_oldMouseY = m_mouseY;
}

void InputManager::ResetXInputs()
{
	memset(this->m_ButtonHeldChange, 0, sizeof(this->m_ButtonHeldChange[0][0]) * 4 * NUM_BUTTONS); //reference:http://stackoverflow.com/questions/2516096/fastest-way-to-zero-out-a-2d-array-in-c/2516111#2516111
	ResetLastXInputStates();
}

bool InputManager::GetKey( const SDL_Scancode& a_key ) const {
	return m_held[a_key];
}

bool InputManager::GetKeyChange(const SDL_Scancode& a_key) const
{
	return m_heldChange[a_key];
}

bool InputManager::GetMouseDown( const int& a_mouseButton ) const {
	if ( m_click[a_mouseButton] && m_mouse[a_mouseButton] ) {
		//m_click[a_mouseButton] = false;
		return true;
	}
	else {
		return false;
	}
}

bool InputManager::GetMouseUp( const int& a_mouseButton ) const {
	if ( m_click[a_mouseButton] && !m_mouse[a_mouseButton] ) {
		//m_click[a_mouseButton] = false;
		return true;
	}
	else {
		return false;
	}
}

void InputManager::SetHold(const SDL_Scancode& a_key, const bool& a_held)
{
	if (a_held)
	{
		m_LastKeyPress = a_key;
	}

	if (m_held[a_key] != a_held) 
	{ 
		m_held[a_key] = a_held; 
		m_heldChange[a_key] = true; 
	} 
}

bool InputManager::GetMouse( const int& a_mouseButton ) const {
	return ((SDL_GetMouseState(NULL,NULL)&SDL_BUTTON(a_mouseButton)) == SDL_BUTTON(a_mouseButton) );
}

glm::vec2 InputManager::GetMouseMove() const
{
	return glm::vec2(this->m_mouseDeltaX, this->m_mouseDeltaY);
}

void InputManager::SetMouseXY(const int& a_mouseX, const int& a_mouseY, const int& a_mouseDX, const int& a_mouseDY) {
	m_oldMouseX = m_mouseX;
	m_mouseX = a_mouseX;

	this->m_mouseDeltaX = a_mouseDX;
	this->m_mouseDeltaY = a_mouseDY;

	m_oldMouseY = m_mouseY;
	m_mouseY = a_mouseY;
}

void InputManager::Tick(float a_DT)
{
	this->m_mouseDeltaX = 0;
	this->m_mouseDeltaY = 0;

	UpdateStickDeltas();

}

void InputManager::UpdateStickDeltas()
{
	for (int i = 0; i < 4; i++)
	{
		//update deltas
		m_LeftStickClockwiseDelta[i] = this->CalcStickClockwiseDelta(i, false);
		m_RightStickClockwiseDelta[i] = this->CalcStickClockwiseDelta(i, true);

		//update state
		m_LeftStickLastState[i] = this->GetStickState(i, false);
		m_RightStickLastState[i] = this->GetStickState(i, true);
	}
}

void InputManager::CheckControllersConnected()
{
	XINPUT_STATE state;

	for (DWORD i = 0; i < XUSER_MAX_COUNT; i++)
	{
		ZeroMemory(&state, sizeof(XINPUT_STATE));

		if (XInputGetState(i, &state) != ERROR_DEVICE_NOT_CONNECTED)
		{
			//controller found
			if (this->b_ControllerConnected[i] == false)
			{
				Log.WriteInfo("Controller found on port: %i", i);
				this->b_ControllerConnected[i] = true;
			}
		}
		else
		{
			if (this->b_ControllerConnected[i] == true)
			{
				Log.WriteInfo("Controller disconnected on port: %i", i);
				this->b_ControllerConnected[i] = false;
			}
		}
	}
}

bool* InputManager::ConnectedControllers()
{
	//TODO: return bool[4] of connected controllers
	return b_ControllerConnected;
}

bool InputManager::GetButton(int playerID, GamepadButtons button) const
{
	//example: (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A
	return (m_XInputControllers[playerID]->GetState().Gamepad.wButtons & GetXInputMaskFromButton(button)) != 0;
}

glm::vec2 InputManager::GetStickState(int playerID, bool rightStick) const
{
	const float maximumVal = 32767;
	glm::vec2 outVec = glm::vec2(0.0f);

	if (rightStick)
	{
		outVec = glm::vec2(m_XInputControllers[playerID]->GetState().Gamepad.sThumbRX, m_XInputControllers[playerID]->GetState().Gamepad.sThumbRY);
	}
	else
	{
		outVec = glm::vec2(m_XInputControllers[playerID]->GetState().Gamepad.sThumbLX, m_XInputControllers[playerID]->GetState().Gamepad.sThumbLY);
	}

	//normalise
	if (outVec != glm::vec2(0.0f))
	{
		outVec /= maximumVal;
		if (glm::length(outVec) > CONTROLLER_STICK_DEADZONE_LENGTH)
		{
			//restore range to 0,1, instead of DEADZONE,1
			outVec -= glm::normalize(outVec)*CONTROLLER_STICK_DEADZONE_LENGTH;
			outVec /= 1.0f - CONTROLLER_STICK_DEADZONE_LENGTH;
			float outVecLength = glm::length(outVec);

			if (outVecLength > 1.0f)
			{
				outVec = glm::normalize(outVec);
			}

		}
		else
		{
			outVec = glm::vec2(0.0f);
		}
	}

	return outVec;
}

float InputManager::GetStickClockwiseDelta(int controllerID, bool rightStick) const
{
	return (rightStick) ? m_RightStickClockwiseDelta[controllerID] : m_LeftStickClockwiseDelta[controllerID];
}

float InputManager::CalcStickClockwiseDelta(int controllerID, bool rightStick) const
{
	float outDelta = 0.0f;
	const float c_MaxRateCosAngle = 0.7f;

	glm::vec2 stickState = this->GetStickState(controllerID, rightStick);
	glm::vec2 lastStickState = (rightStick) ? m_RightStickLastState[controllerID] : m_LeftStickLastState[controllerID];

	if (stickState != glm::vec2(0.0f))
	{
		glm::vec3 crossResult = glm::cross(glm::vec3(lastStickState, 0.0f), glm::vec3(stickState, 0.0f));

		outDelta = glm::clamp(-crossResult.z / c_MaxRateCosAngle, -1.0f, 1.0f);
	}

	return outDelta;
}

bool InputManager::IsStickNeutral(int controllerId, bool rightStick) const
{
	return (this->GetStickState(controllerId, rightStick) == glm::vec2(0.0f));
}

float InputManager::GetTriggerValue(int controllerID, bool right) const
{
	auto state = this->m_XInputControllers[controllerID]->GetState();
	if (right)
	{
		return state.Gamepad.bRightTrigger/255.0f;
	}
	else
	{
		return state.Gamepad.bLeftTrigger / 255.0f;
	}
}

bool InputManager::GetButtonDown(int playerID, GamepadButtons button) const
{
	if (m_ButtonLastState[playerID][button] == false)
	{
		if (GetButton(playerID, button))
		{
			//button down
			return true;
		}
	}
	return false;
}

bool InputManager::GetButtonUp(int playerID, GamepadButtons button) const
{
	if (m_ButtonLastState[playerID][button] == true)
	{
		if (!GetButton(playerID, button))
		{
			//button up
			return true;
		}
	}
	return false;
}


void InputManager::ResetLastXInputStates()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < NUM_BUTTONS; j++)
		{
			this->m_ButtonLastState[i][j] = GetButton(i, (GamepadButtons)j);
		}
	}
}

DWORD InputManager::GetXInputMaskFromButton(GamepadButtons button) const
{
	DWORD XMask = 0;
	switch (button)
	{
	case GamepadButtons::BUTTON_A:
		XMask = XINPUT_GAMEPAD_A;
		break;
	case GamepadButtons::BUTTON_B:
		XMask = XINPUT_GAMEPAD_B;
		break;

	case GamepadButtons::BUTTON_X:
		XMask = XINPUT_GAMEPAD_X;
		break;

	case GamepadButtons::BUTTON_Y:
		XMask = XINPUT_GAMEPAD_Y;
		break;

	case GamepadButtons::BUTTON_LEFT:
		XMask = XINPUT_GAMEPAD_DPAD_LEFT;
		break;

	case GamepadButtons::BUTTON_RIGHT:
		XMask = XINPUT_GAMEPAD_DPAD_RIGHT;
		break;

	case GamepadButtons::BUTTON_DOWN:
		XMask = XINPUT_GAMEPAD_DPAD_DOWN;
		break;

	case GamepadButtons::BUTTON_UP:
		XMask = XINPUT_GAMEPAD_DPAD_UP;
		break;

	case GamepadButtons::RIGHT_SHOULDER:
		XMask = XINPUT_GAMEPAD_RIGHT_SHOULDER;
		break;

	case GamepadButtons::LEFT_SHOULDER:
		XMask = XINPUT_GAMEPAD_LEFT_SHOULDER;
		break;

	case GamepadButtons::RIGHT_THUMB:
		XMask = XINPUT_GAMEPAD_RIGHT_THUMB;
		break;

	case GamepadButtons::LEFT_THUMB:
		XMask = XINPUT_GAMEPAD_LEFT_THUMB;
		break;

	default:
		Log.WriteWarning("Gamepad button not handled: %i", button);
		break;
	}

	return XMask;
}

void InputManager::VibrateController(int playerID, int left, int right)
{
	this->m_XInputControllers[playerID]->Vibrate(left, right);
}