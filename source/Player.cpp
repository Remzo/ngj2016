#include "pch.h"

#include "sphere.h"
#include "Axe.h"
#include "Player.h"
#include "Sprite.h"
#include "Ship.h"
#include "Octopus.h"
#include "soundsystem.h"

extern SoundSystem & g_SoundSystem;
Player::Player(float radius) : Sphere(radius)
{
	m_InputDir = glm::vec3(0.0f);
	m_Bucket = new Axe(0.5f);
	m_Bucket->SetColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));

	m_AxeOffset = glm::vec3(0.0f, 0.0f, -c_AxeOffsetMagnitude);
	
	unsigned int* framesPerAnim = new unsigned int[1];
	framesPerAnim[0] = 6;

	this->m_Sprite = new Sprite("sprites/viking_walk.png", 156, 25, 1U, framesPerAnim);
	m_Sprite->SetRotation(glm::quat(glm::lookAt(this->GetPosition(), MainCamera.GetPosition(), glm::vec3(0.0f, 0.0f, 1.0f))));
	m_Sprite->SetParent(this);
	m_Sprite->SetScale(c_SpriteScale);
	m_Sprite->SetAnim(0, 0.1f);

	m_StunSprite = new Sprite("sprites/viking_stunned.png", 156, 25, 1U, framesPerAnim);
	m_StunSprite->SetRotation(glm::quat(glm::lookAt(this->GetPosition(), MainCamera.GetPosition(), glm::vec3(0.0f, 0.0f, 1.0f))));
	m_StunSprite->SetParent(this);
	m_StunSprite->SetScale(c_SpriteScale);
	m_StunSprite->SetAnim(0, 0.25f);

	this->SetRotationEuler(glm::vec3(90.0f, 0.0f, 0.0));
}

Player::~Player()
{
	delete m_Bucket;
	delete m_Sprite;
}

void Player::SetControllerID(int controllerID)
{
	m_ControllerID = controllerID;

	b_ValidControllerConnected = Input.ConnectedControllers()[m_ControllerID];
	if (b_ValidControllerConnected)
	{
		Log.WriteDebug("ValidController for player: %i", m_ControllerID);
	}
	else
	{
		Log.WriteDebug("InvalidController for player: %i", m_ControllerID);
	}
}

void Player::Tick(float a_DT)
{
	if (m_Bucket->m_Octopus == NULL)
	{
		m_Bucket->m_Octopus = m_Octopus;
	}

	UpdateTimers(a_DT);

	HandleInput();

	m_Sprite->Tick(a_DT);
	m_StunSprite->Tick(a_DT);

	//reduce vel
	m_Velocity -= ((b_Charging) ? c_ChargingFriction : c_Friction) * m_Velocity * a_DT;

	//add vel
	if (m_InputDir != glm::vec3(0.0f))
	{
		m_Velocity += c_Acceleration * m_InputDir * a_DT;
	}

	//add pos
	//bounds
	glm::vec3 futurePos = this->m_Position + m_Velocity * a_DT;
  float distToOcto = m_Octopus->GetDistance(futurePos);
  glm::vec3 octoDir = m_Octopus->GetDir(futurePos);
  octoDir.y = 0;

  if (distToOcto < 10) {
    m_Velocity +=  octoDir * (10.0f - distToOcto);
  }

	if (futurePos.z < 25.0f || futurePos.z > 75.0f || futurePos.x < -70.0f || futurePos.x > 74 || distToOcto < 5.0) {
		m_Velocity = glm::vec3(0);
	} 

	this->m_Position += m_Velocity * a_DT;
	this->SetPosition(m_Position);

	UpdateTargetAxePosition();

	m_Bucket->Tick(a_DT);
}

void Player::UpdateTargetAxePosition()
{
	m_Bucket->SetPosition(this->m_Position + m_AxeOffset);
	m_Bucket->SetRotation(glm::quat(glm::lookAt(m_Bucket->GetPosition(), this->m_Position, glm::vec3(0.0f, -1.0f, 0.0f))));
	m_Bucket->RotateEuler(90.0f, glm::vec3(0.0f, 1.0f, 0.0f), false);
	m_Bucket->RotateEuler(180.0f, glm::vec3(1.0f, 0.0f, 0.0f), true);
	m_Bucket->RotateEuler(180.0f, glm::vec3(0.0f, 1.0f, 0.0f), true);
}

void Player::SetShip(Ship* ship)
{
	m_Ship = ship;
}

void Player::UpdateTimers(float a_DT)
{
	if (b_Stunned)
	{
		m_StunTimer -= a_DT;

		if (m_StunTimer <= 0.0f)
		{
			b_Stunned = false;
			this->SetState(ALIVE);
		}
		return;
	}

	if (b_Attacking)
	{
		m_AttackTimer += a_DT;
		//pulse axe
		m_AxeOffset += glm::normalize(m_AxeOffset) * glm::sin(m_AttackTimer * glm::pi<float>() * 4);

		if (m_AttackTimer >= c_AttackDuration)
		{
			m_AttackTimer = 0.0f;
			b_Attacking = false;
			m_Bucket->SetAttacking(false);
			this->SetState(ALIVE);
			m_AxeOffset = glm::normalize(m_AxeOffset) * c_AxeOffsetMagnitude;
		}
	}

	if (b_Charging)
	{
		m_ChargeTimer += a_DT;
		if (m_ChargeTimer >= c_ChargeDuration)
		{
			ChargingComplete();
		}
	}
}

void Player::SetState(PlayerStates newState)
{
	switch (newState)
	{
	case ALIVE:
		this->SetColor(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
		b_Attacking = false;
		b_Charging = false;
		b_Stunned = false;
		m_StunSprite->StopAnim();
		break;
	case STUNNED:
		this->SetColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
		b_Attacking = false;
		b_Charging = false;
		
		m_StunSprite->StartAnim();

		m_StunSprite->b_ReverseV = m_Sprite->b_ReverseV;
		m_StunSprite->SetDirtyUVs();

		m_Ship->holes.push_back(new Hole(this->m_Position + glm::vec3(0.0f, -3.5f, 0.0f)));

		break;
	case CHARGING:
		this->SetColor(glm::vec4(0.4f, 0.0f, 0.4f, 1.0f));
		b_Attacking = false;
		b_Stunned = false;
		m_StunSprite->StopAnim();
		g_SoundSystem.PlaySound(g_SoundSystem.soundVikingCharge);
		break;
	default:
		break;
	}
	this->m_CurrentState = newState;
}

void Player::HandleInput()
{
	m_InputDir = glm::vec3(0.0f);

	switch (m_CurrentState)
	{
	case PlayerStates::DEAD:
	case PlayerStates::STUNNED:
		return; // no input in dead or stunned states
	default:
		break;
	};

	if (!b_ValidControllerConnected) //keyboard fallback
	{
		//Movement
		//keyboard
		if (Input.GetKey(SDL_SCANCODE_A))
		{
			m_InputDir += glm::vec3(-1.0f, 0.0f, 0.0f);
		}

		if (Input.GetKey(SDL_SCANCODE_D))
		{
			m_InputDir += glm::vec3(1.0f, 0.0f, 0.0f);
		}

		if (Input.GetKey(SDL_SCANCODE_W))
		{
			m_InputDir += glm::vec3(0.0f, 0.0f, -1.0f);
		}

		if (Input.GetKey(SDL_SCANCODE_S))
		{
			m_InputDir += glm::vec3(0.0f, 0.0f, 1.0f);
		}

    if (glm::length(m_InputDir) != 0) {
      m_InputDir = glm::normalize(m_InputDir);
    }

		//Aiming
	}else
	{
		//Movement
		glm::vec2 leftStick = Input.GetStickState(m_ControllerID, false);

		m_InputDir = glm::vec3(leftStick.x, 0.0f, -leftStick.y);

		if (m_InputDir != glm::vec3(0.0f))
		{
			b_Moving = true;
			m_Sprite->StartAnim();
			if (m_InputDir.x > 0)
			{
				if (!m_Sprite->b_ReverseV)
				{
					m_Sprite->b_ReverseV = true;
					m_Sprite->SetDirtyUVs();
				}
			}
			else
			{
				if (m_Sprite->b_ReverseV)
				{
					m_Sprite->b_ReverseV = false;
					m_Sprite->SetDirtyUVs();
				}
			}
		}
		else
		{
			b_Moving = false;
			m_Sprite->StopAnim();
			m_Sprite->SetFrame(0);
		}


		if (!b_Attacking && !b_Charging)
		{
			//Aiming
			glm::vec2 rightStick = Input.GetStickState(m_ControllerID, true);
			if (rightStick != glm::vec2(0.0f))
			{
				rightStick = glm::normalize(rightStick);

				m_AxeOffset = glm::vec3(rightStick.x, 0.0f, -rightStick.y) * c_AxeOffsetMagnitude;
			}
			else
			{
				if (leftStick != glm::vec2(0.0f))
				{
					m_AxeOffset = glm::normalize(glm::vec3(leftStick.x, 0.0f, -leftStick.y)) * c_AxeOffsetMagnitude;
				}
			}
			
			if (Input.GetButtonDown(m_ControllerID, InputManager::RIGHT_SHOULDER))
			{
				StartCharging();
			}else
			if (Input.GetButtonDown(m_ControllerID, InputManager::LEFT_SHOULDER))
			{
				Attack();
			}
		}

		//cancel charge if button released
		if (b_Charging && Input.GetButtonUp(m_ControllerID, InputManager::RIGHT_SHOULDER))
		{
			CancelCharging();
		}
	}
}

void Player::Stun(float stunDuration)
{
	this->m_StunTimer = stunDuration;
	b_Stunned = true;
	this->SetState(STUNNED);
}

void Player::Attack()
{
	b_Attacking = true;
	m_Bucket->SetAttacking(true);
	this->SetState(PlayerStates::ATTACK);
	g_SoundSystem.PlaySound(g_SoundSystem.soundvikingAttack);
}

void Player::StartCharging()
{
	b_Charging = true;
	m_ChargeTimer = 0.0f;
	Log.WriteDebug("StartedCharging");
	m_Bucket->SetCharging(true);
	this->SetState(PlayerStates::CHARGING);
}

void Player::CancelCharging()
{
	b_Charging = false;
	Log.WriteDebug("ChargeCancelled.");
	m_Bucket->SetCharging(false);
	//alive state
	this->SetState(PlayerStates::ALIVE);
}

void Player::ChargingComplete()
{
	b_Charging = false;
	Log.WriteDebug("ChargeComplete");
	m_Bucket->SetCharging(false);
	g_SoundSystem.PlaySound(g_SoundSystem.soundVikingMakeHole);
	//stun state
	this->Stun();
}

void Player::Render()
{
	if (!b_Stunned)
	{
		m_Sprite->Render();
	}
	else
	{
		m_StunSprite->Render();
	}

	m_Bucket->Render();
}