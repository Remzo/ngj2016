#include "pch.h"

#include "GameObject.h"

GameObject::GameObject()
{
	
}

GameObject::~GameObject()
{

}

void GameObject::Tick(float a_DT)
{
	this->m_Velocity += this->m_Acceleration;

	m_Position += m_Velocity * a_DT;

	this->SetPosition(m_Position);
}
