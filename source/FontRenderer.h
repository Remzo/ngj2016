#pragma once

class Model;
class Texture;
class FontRenderer
{
public:
	FontRenderer();
	~FontRenderer();

	void Init();

	void RenderCharacter(const char character, glm::vec3 pos, glm::vec3 color, float scale);
	void RenderString(const char* string, glm::vec3 pos, glm::vec3 color = glm::vec3(1.0f), float scale = 1.0f);
private:
	bool initialised = false;
	void RenderCharacter();
	void UpdateUVs(glm::vec2 uvTopLeft);
	glm::vec2 GetUVFromChar(char a);

	Texture* m_FontTexture;
	Model* m_LetterQuad;

	std::vector<VertexComplete> m_Vertices;
	std::vector<unsigned int> m_Indices;


};
extern FontRenderer &g_FontRenderer;