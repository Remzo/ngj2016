#include "pch.h"

#include "CannonballManager.h"
#include "Cannonball.h"
#include "Collider.h"

#define CANNONBALL_LIFETIME_IN_SECONDS 2.0f
#define CANNONBALL_MASS 20.0f

CannonballManager& g_CannonballManager = CannonballManager();

CannonballManager::CannonballManager()
{
}

CannonballManager::~CannonballManager()
{
	for (auto i : m_Cannonballs)
	{
		delete i.m_Ball;
	}
	m_Cannonballs.clear();
}

Cannonball* CannonballManager::CreateCannonball(unsigned int ownerID, glm::vec3 pos, glm::vec3 vel)
{
	Cannonball* newBall = new Cannonball(NULL, pos, vel);
	newBall->SetMass(CANNONBALL_MASS);
	newBall->SetVelocity(glm::vec3(0.0f));
	newBall->AddAcceleration(vel);
	
	CannonballInfo ballInfo;
	ballInfo.m_Ball = newBall;
	ballInfo.m_Lifetime = 0.0f;
	
	m_Cannonballs.push_back(ballInfo);

	return newBall;
}

void CannonballManager::UpdateCannonballs(float a_DT)
{
	for (auto i = m_Cannonballs.begin(); i != m_Cannonballs.end(); /*i is incremented in below if statement*/)
	{
		i->m_Ball->Tick(a_DT);
		i->m_Lifetime += a_DT;

		//lifetime check
		if (i->m_Lifetime >= CANNONBALL_LIFETIME_IN_SECONDS)
		{
			//delete
			delete i->m_Ball;
			i = m_Cannonballs.erase(i);
		}
		else
		{
			++i;
		}
	}
}

void CannonballManager::Render()
{
	for (auto i : m_Cannonballs)
	{
		i.m_Ball->Render();
	}
}