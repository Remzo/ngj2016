#pragma once

#include "Quad.h"

class Texture;
class BaseParticle : public Quad
{
public:
	BaseParticle();
	BaseParticle(Texture* tex);
	~BaseParticle();

	void SetTexture(Texture* tex);
	void SetLifetime(float lifetime);
	void SetVelocity(glm::vec3 vel);

	void Update(float a_DT);
	void Render();
	void SetActive(bool active);
private:
	Texture* m_Texture;
	bool b_Active = false;
	float m_Lifetime = 0.0f;
	float m_Timer = 0.0f;

	glm::vec3 m_Velocity;
};