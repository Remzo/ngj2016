#pragma once

#include "Object.h"

class Texture;
class BaseParticle;
class ParticleSystem : public Object
{
public:
	ParticleSystem(Texture* tex, int numParticles, float lifetime = 1.0f);
	~ParticleSystem();

	void Emit(int numParticlesToEmit);
	void Tick(float a_DT);
	void Render();
private:
	Texture* m_Texture;
	int m_NumParticles = 0;
	BaseParticle* m_ParticlePool;

	int m_CurrentIndex = 0;
};