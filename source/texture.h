#pragma once

class Texture
{
public:
	Texture(GLuint textureID, int width = -1, int height = -1);
	Texture(const char* texPath, int width = -1, int height = -1);
	~Texture();

	GLuint GetTextureID() const {	return this->m_ID; }
	const int GetTextureWidth() const { return m_Width; }
	const int GetTextureHeight() const { return m_Height; }

private:
	GLuint m_ID;
	int m_Width;
	int m_Height;
};