#pragma once

class Texture;
class Skybox
{
public:
	Skybox(GLuint shaderID, const char* texLeft, const char* texRight, const char* texUp, const char* texDown, const char* texForward, const char* texBack);
	~Skybox();

	void Draw(Camera* cam = NULL);

private:
	void Init();

	GLuint m_CubemapID;

	GLuint m_shaderID;

	glm::mat4 m_ModelMatrix;
	//GLuint m_VertexBuffer;
	//GLuint m_IndexBuffer;
	GLuint m_VAO;

	VertexUV* m_Vertices;
	GLuint* m_Indices;

};