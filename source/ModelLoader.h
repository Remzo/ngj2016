#pragma once

class Mesh;
class Model;
class ModelLoader
{
public:
	enum ModelName
	{
		BOAT,
		OCTOPUS,
		SHIP,
		AXE,
		EYE,
		HOLE,
    WATER,
    NUM_MODELS,
	};

	ModelLoader();
	~ModelLoader();

	void DEBUGRenderLastLoadedModel();

	Mesh* CreateMesh(ModelName model);
	Model* CreateModel(ModelName name);
	std::vector<std::pair<std::string, const Mesh*>> GetModelAsSeperateMeshes(ModelName name);
	Mesh* GetMesh(ModelName name);
	const Mesh* GetMesh(std::string name);
	Mesh* GetSubMeshOfModel(ModelName name, std::string subMeshName);

	void LoadAllModels();
	void DebugTrigger();
private:
	inline void LoadSubMeshes(ModelName model);

	const char* GetModelPath(ModelName model);
	std::vector<Mesh*> LoadObj(const char* filePath, bool loadSeperateMeshes = false);

	std::map<std::string, Mesh*> m_LoadedMeshes;
	float m_Accumulator = 0.0f;

	Model* m_LastLoadedMesh = 0;
	std::map<std::string, std::vector<std::string>> m_SubMeshNames;
};

extern ModelLoader& g_ModelLoader;