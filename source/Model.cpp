#include "pch.h"

#include "Mesh.h"
#include "Model.h"
#include "Collider.h"
#include "texture.h"

#define RENDER_BOUNDING_BOXES

Model::Model()
{
	this->b_Initialised = false;
	this->m_Mesh = NULL;
	this->m_Color = glm::vec4(1);
	this->m_CurShaderType = ShaderType::FLAT_UNLIT;
	this->m_RenderMode = GL_TRIANGLES;
	this->m_Texture = 0;
}

Model::Model(Mesh* mesh, const ShaderType shaderType)
{
	this->m_Mesh = mesh;
	this->m_Color = glm::vec4(1);
	this->m_CurShaderType = shaderType;
	this->m_RenderMode = GL_TRIANGLES;
	Init();
}

Model::~Model()
{
	CleanUp();
}

void Model::SetMesh(Mesh* mesh)
{
	if (this->m_Mesh != 0)
	{
		CleanUp();
	}
	this->m_Mesh = mesh;
	Init();
}

void Model::SetGLRenderMode(GLenum renderMode)
{
	this->m_RenderMode = renderMode;
}

void Model::SetShaderType(ShaderType shaderType)
{
	m_CurShaderType = shaderType;
	if (b_Initialised)
	{
		glDeleteBuffers(1, &this->m_VAO);
		Init();
	}
}

void Model::CleanUp()
{
	if (this->m_Mesh != 0)
	{
		delete this->m_Mesh;
		this->m_Mesh = 0;
	}
	glDeleteVertexArrays(1, &this->m_VAO);
}

void Model::Init()
{
  m_TotalTime = 0;
	this->b_Initialised = true;
	this->m_shaderID = g_ShaderLoader.GetShader(this->m_CurShaderType);
	//Buffers
	glGenVertexArrays(1, &this->m_VAO);
	glBindVertexArray(this->m_VAO);

	GLuint vertBuffer, indexBuffer;
	glGenBuffersARB(1, &vertBuffer);
	glGenBuffersARB(1, &indexBuffer);

	// Copy the vertex data to the VBO
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertBuffer);
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(VertexComplete)* this->m_Mesh->m_NumVerts, &this->m_Mesh->m_VertexData[0], GL_STATIC_DRAW_ARB);

	//Copy the index data to the VBO
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, indexBuffer);
	glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, sizeof(unsigned int)* this->m_Mesh->m_NumIndices, &this->m_Mesh->m_Indices[0], GL_STATIC_DRAW_ARB);

	//Get attribute locations from shader
	GLint positionAtribID = glGetAttribLocation(this->m_shaderID, "in_position");
	GLint normalAtribID = glGetAttribLocation(this->m_shaderID, "in_normal");
	GLint uvAtribID = glGetAttribLocation(this->m_shaderID, "in_uv");
	GLint colorAtribID = glGetAttribLocation(this->m_shaderID, "in_color");

	// Bind the first texture to the first texture unit.
	glActiveTexture(GL_TEXTURE0);

	//Map attribute locations to VBO
	glVertexAttribPointer(positionAtribID, 3, GL_FLOAT, GL_FALSE, sizeof(VertexComplete), MEMBER_OFFSET(VertexComplete, m_Pos));
	glEnableVertexAttribArray(positionAtribID);

	glVertexAttribPointer(normalAtribID, 3, GL_FLOAT, GL_FALSE, sizeof(VertexComplete), MEMBER_OFFSET(VertexComplete, m_Normal));
	glEnableVertexAttribArray(normalAtribID);

	glVertexAttribPointer(uvAtribID, 2, GL_FLOAT, GL_FALSE, sizeof(VertexComplete), MEMBER_OFFSET(VertexComplete, m_UV));
	glEnableVertexAttribArray(uvAtribID);

	glVertexAttribPointer(colorAtribID, 4, GL_FLOAT, GL_FALSE, sizeof(VertexComplete), MEMBER_OFFSET(VertexComplete, m_Color));
	glEnableVertexAttribArray(colorAtribID);

	//Unbind
	glBindVertexArray(0);
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
	glDisableVertexAttribArray(positionAtribID);
	glDisableVertexAttribArray(normalAtribID);
	glDisableVertexAttribArray(uvAtribID);
	glDisableVertexAttribArray(colorAtribID);
	glDeleteBuffers(1, &vertBuffer);
	glDeleteBuffers(1, &indexBuffer);
}

void Model::SetColorInterpolation(bool on, float transitionTime)
{
	b_InterpolateColor = on;
	if (b_InterpolateColor)
	{
		m_ColorTransitionTime = transitionTime;
	}
}

void Model::Update(float a_DT)
{
  m_TotalTime += a_DT;

	if (b_InterpolateColor)
	{
		if (b_ColorChanging)
		{
			m_ColorTimer += a_DT;
			m_Color += m_ColorDelta*a_DT;

			if (m_ColorTimer > m_ColorTransitionTime)
			{
				//finished transition
				b_ColorChanging = false;
				m_Color = m_TargetColor;
			}

		}
	}
}

void Model::Render()
{
	if (!b_Initialised)
	{
		Log.WriteWarning("Model not initialised! No mesh set");
		return;
	}

	//render bounding box
#if defined(_DEBUG) && defined(RENDER_BOUNDING_BOXES)
	if (this->m_Collider != NULL)
	{
		this->m_Collider->Render();
	}
#endif

	Camera* cam = &MainCamera;
	glBindVertexArray(this->m_VAO);
	glUseProgram(this->m_shaderID);

	//Set MVP
	glm::mat4 modelMatrix = this->GetModelMatrix();
	glm::mat4 mvp = cam->GetProjectionMatrix() * cam->GetViewMatrix() * modelMatrix;
	GLint uniformMVP = glGetUniformLocation(this->m_shaderID, "MVP");
	GLint uniformModel = glGetUniformLocation(this->m_shaderID, "Model");
	GLint uniformColor = glGetUniformLocation(this->m_shaderID, "color");
	GLint uniformCamDir = glGetUniformLocation(this->m_shaderID, "camDir");
  GLint uniformTime = glGetUniformLocation(this->m_shaderID, "uTime");

	if (this->m_Texture != 0)
	{
		// Get the locations for the uniform sampler variables defined in the fragment shader.
		GLint uniformTex0 = glGetUniformLocation(this->m_shaderID, "tex0");

		// Bind the first texture to the first texture unit.
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_Texture->GetTextureID());

		// Now bind the uniform samplers to the corresponding texture units.
		// Note that we must bind the uniform samplers to the texture unit,
		// not the texture object ID!
		glUniform1i(uniformTex0, 0); // Bind the first sampler to texture unit 0.

	}
  glUniform1f(uniformTime, m_TotalTime); 

	glUniformMatrix4fv(uniformMVP, 1, GL_FALSE, glm::value_ptr(mvp));
	glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(glm::toMat4(this->GetRotation(false)))); //rotation only for normals
	glUniform4fv(uniformColor, 1, glm::value_ptr(this->m_Color));
	glUniform3fv(uniformCamDir, 1, glm::value_ptr(cam->ForwardVec()));

	glDrawElements(this->m_RenderMode, this->m_Mesh->m_NumIndices, GL_UNSIGNED_INT, BUFFER_OFFSET(0));

	glUseProgram(0);
	glBindVertexArray(0);

}

void Model::SetDefaultColor(glm::vec4 color)
{
	m_DefaultColor = color;
	this->SetTargetColor(color);
}

void Model::SetTargetColor(glm::vec4 newColor)
{
	m_TargetColor = newColor;
	m_ColorDelta = newColor - m_Color;
	m_ColorDelta /= m_ColorTransitionTime;
	m_ColorTimer = 0.0f;
	b_ColorChanging = true;
}