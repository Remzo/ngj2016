#pragma once

#include "PhysicsObject.h"

class Collider :public PhysicsObject
{
public:
	Collider(glm::vec3 halfScales, CollisionHandler::CollisionTypes colType = CollisionHandler::CollisionTypes::STATIC);
	~Collider();

	void SetPoints(std::vector<glm::vec3> points, float scale = 1.0f);
	void SetPoints(std::vector<glm::vec3> points, std::vector<glm::vec3> normals, std::vector<unsigned int> indices = std::vector<unsigned int>(), float scale = 1.0f);

	CollisionHandler::CollisionTypes GetColliderType() const { return m_ColliderType; }

	void RescaleCollider(float scale);
	std::vector<glm::vec3> GetCollisionPoints() const;
	std::vector<glm::vec3> GetRelativeCollisionPoints() const;
	std::vector<glm::vec3> GetFaceNormals() const { return m_FaceNormals; }

	glm::mat3 GetInertialTensor() const;

	const glm::vec3 c_Dimensions;
private:
	std::vector<glm::vec3> m_Points;
	std::vector<glm::vec3> m_FaceNormals;

	CollisionHandler::CollisionTypes m_ColliderType;
};
